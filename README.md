
![Micromanager logo](https://bitbucket.org/AliasAPI/micromanager/raw/master/opt/image/micromanager-logo-600.png)

`MicroManager` helps you draft, develop, and deploy dependable code.  It's a _didactic_ development<br>
environment that calls dozens of linters and static analyzers. It _dogmatically_ enforces extremely<br>
strict code quality standards for bash, Composer, C, Javascript, JSON, Go, MySQL, PHP, HTML, and YAML.<br>  
It is designed to automate and **micro**manage most software development tasks<br>
and deploy high-performance backend and frontend **micro**services (and monoliths).

## Contents

- [Contents](#contents)
- [Vision](#vision)
- [Architecture](#architecture)
- [Installation](#installation)
- [Configuration](#configuration)
- [Functionality](#functionality)
- [Contribute](#contribute)


<a id="vision"></a>
## Vision 

+ ``Install & configure strict quality assurance systems with a single command.``
+ ``Run multiple instances of MicroManager on a shared network on your machine.``
+ ``Provide a complete web development environment to your developers for free.``
+ ``Give code reviews to developers who write the syntax of the code you draft.``
+ ``Train developers to write more secure, clean code and adopt best practices.``
+ ``Save time, effort & money managing lower cost, less experienced developers.``
+ ``Capture script crashes and perform automatic debugging and code correction.``
+ ``Dump, compress, encrypt, and transmit (database) backups to remote servers.``
+ ``Convert pseudocode you draft into the procedural web development languages.``
+ ``Check the security and quality of code composed by artificial intelligence.``
+ ``Dynamically fine tune and automatically manage local large language models.``
+ ``Push passing code to repositories and deploy servers with a single command.``


<a id="architecture"></a>
## Architecture

* The open source libraries are implemented in [dedicated folders using their respective names](https://bitbucket.org/AliasAPI/micromanager/src/master/opt/).
* The folders include the configuration files (that are preconfigured to the most strict settings).
* The libraries are installed, configured, and run using easy to comprehend top level functions.
* The procedural design makes it much easier to implement more functionality using copy & paste.
* Finally, the functions are called sequentially in an _extremely clean_ [mm script](https://bitbucket.org/AliasAPI/micromanager/src/master/mm)!


<a id="installation"></a>
## Installation
Run this command in a terminal (in <a href="https://mxlinux.org" target="_blank">MX Linux</a> or <a href="https://mxlinux.org" target="_blank">Debian</a>):

```bash
curl -sSL https://bitbucket.org/AliasAPI/micromanager/raw/master/opt/micromanager/install_micromanager.sh | bash
```

<a id="configuration"></a>
## Configuration

```bash
Update the .env file
```


<a id="functionality"></a>
## Functionality 

```bash
To see a list of commands (and help):
$ ./mm

To run a scan without changing code:
$ ./mm scan

To fix code while scanning the code:
$ ./mm fix
```


<a id="contribute"></a>
## Contribute

```bash
The todo list is conveniently located within the codebase itself.
It also includes links to documentation to support the developer.
Please complete some tasks & contact me @ viewdrew.com@gmail.com.

$ grep -r todo:: ./*
```