#!/bin/bash
source "$( dirname -- "$( readlink -f -- "$0"; )"; )/opt/micromanager/source_scripts.sh"

source_scripts

set_environment_variables

configure_micromanager "$@"

help_micromanager

sudo -v

create_mm_alias

run_jq

run_php

run_mariadb

run_git

configure_docker

install_docker

run_nvidia

save_train

permit_user

configure_mariadb_docker_image

configure_phpmyadmin_docker_image

configure_nginx_docker_image

configure_php_fpm_docker_image

create_custom_dockerfile

build_docker_image

configure_tools_docker_image

create_custom_dockerfile

build_docker_image

configure_gpu_docker_image

create_custom_dockerfile

build_docker_image

stop_docker_containers

delete_docker_containers

delete_docker_custom_images

delete_docker_base_images

delete_applications

create_directories

configure_opcache

start_docker_containers

delete_build_scripts

list_docker_containers

remove_docker

get_docker_addresses

run_nginx

synchronize_code

delete_docker_train_keys

save_train

check_docker_containers

docker exec -i "gpu${MM_ID}" /bin/bash <<EOF
    source /opt/micromanager/micromanager/source_scripts.sh 

    source_scripts

    load_train

    set_dockerized

    run_ollama

    run_litellm
EOF

docker exec -i "tools${MM_ID}" /bin/bash <<EOF
    source /opt/micromanager/micromanager/source_scripts.sh

    source_scripts

    load_train

    set_dockerized

    run_git

    # run_xdebug

    run_composer

    configure_file_orders

    run_mariadb

    create_and_select_mm_database

    create_mm_tables

    # run_code_chunker

    create_file_list

    insert_files_to_process

    reset_file_statuses

    sort_file_lists

    #run_tiktoken

    select_loops_remaining

    while [[ "\${TRAIN["loops_remaining"]:-}" -gt 0 ]]; do
        select_file

        delete_file_flaws

        delete_removed_file_path

        set_file_log_path

        set_manage_file

        create_file_port_path

        run_shfmt

        # run_prettier

        run_jsonlint
        
        run_jq

        run_composer_normalize

        run_composer_require_checker 

        run_security_checker 

        run_yaml_lint

        # run_html_purifier

        # run_htmlawed

        run_shellcheck

        # run_jslint

        # run_eslint 

        run_phplint

        run_php_parallel_lint 

        run_php_cs_fixer

        run_php_codesniffer

        # run_php_assumptions

        # run_phan

        run_phpmd 

        # run_phpstan 

        # run_phpmnd 

        # run_progpilot

        # run_psalm

        # run_rector

        # run_pestphp

        # run_phpunit

        # run_infection

        increment_file_mods

        increment_track_loops

        select_file_flaws

        create_prompt_file

        run_ollama

        set_ollama_command

        # use_ollama

        set_file_status

        update_file_status

        select_loops_remaining

        stop_loop_track

        display_elapsed_time
    done

    # run_phpmetrics

    # run_phpinsights 

    # run_phpdocumentor 

    # export_mariadb_databases 

    # backup_mm 

    # import_mariadb_databases 

    # git push - set the maximum Git commits per duration.

    # jira https://github.com/lesstif/php-jira-rest-client

    # ease_development

    # create_certificates 

    delete_log_files 

    display_elapsed_time

    display_file_flaws
EOF
