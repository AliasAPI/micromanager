#!/bin/bash

# todo:: Also add https://en.wikipedia.org/wiki/HTML_Tidy HTML_Tidy

# Run htmlLawed to lint and check HTML documents
# http://www.bioinformatics.org/phplabware/internal_utilities/htmLawed/ Original project
# https://github.com/kesar/HTMLawed Derivative project
function run_htmlawed {
    echo "${FUNCNAME[0]}():"

    # Install htmLawed
    # http://www.bioinformatics.org/phplabware/internal_utilities/htmLawed/composer_usage.htm
    # http://www.bioinformatics.org/phplabware/internal_utilities/htmLawed/
    # https://www.math.ucla.edu/sites/all/modules/htmLawed/htmLawed/htmLawed_README.htm
    if [[ "${TRAIN[task]:?}" == "install" ]] \
        && [ ! -f "${TRAIN[bin_dir]:?}/htmLawed.php" ]; then
        cd "${TRAIN[toolbox]:?}" || return 1

        composer require --dev htmlawed/htmlawed
    fi

    # Configure htmLawed
    # http://www.bioinformatics.org/phplabware/internal_utilities/htmLawed/htmLawed_README.htm#s2.2
    # todo:: Review /opt/micromanager/htmlawed/sanitize_html.php
    # todo:: Add the most strict configuration in sanitize_html.php

    # http://www.bioinformatics.org/phplabware/internal_utilities/htmLawed/example_settings.htm
    # The following increase security risks:
    # Allowing script, applet, embed, iframe or object elements, or certain of their attributes like allowscriptaccess
    # Allowing HTML comments (some Internet Explorer versions are vulnerable with, e.g., <!--[if gte IE 4]><script>alert("xss");</script><![endif]-->
    # Allowing dynamic CSS expressions (a feature of the IE browser)
    # Allowing the style attribute
    # https://www.math.ucla.edu/sites/all/modules/htmLawed/htmLawed/htmLawed_README.htm#s2.5

    # Usage examples:
    # https://www.math.ucla.edu/sites/all/modules/htmLawed/htmLawed/htmLawed_README.htm#s2.9
    # https://www.math.ucla.edu/sites/all/modules/htmLawed/htmLawed/htmLawed_README.htm#s3.3.5
    if [[ "${TRAIN[task]:?}" == "install" ||
        "${TRAIN[task]:?}" == "config" ]] \
        && [ ! -f "${TRAIN[bin_dir]:?}/htmLawed.php" ]; then
        cp "${TRAIN[toolbox]:?}/vendor/htmlawed/htmlawed/htmLawed.php" "${TRAIN[bin_dir]:?}"
    fi

    # Run htmLawed
    if [[ "${TRAIN[file_type]:?}" != "html" ]]; then
        echo "  Skipping [${TRAIN[file_type]:?}] file"
        return 0
    fi

    cd "${TRAIN[git_app_path]:?}" || return 1

    # todo:: Add scan Is there scan (dry run) functionality?
    if [[ "${TRAIN[task]:?}" == "scan" ]]; then
        # todo:: Run htmlawed to get the errors in file_log
        # todo:: insert_file_flaws
        return 0
    fi

    if [[ "${TRAIN[task]:?}" != "fix" &&
        "${TRAIN[task]:?}" != "htmlawed" ]]; then
        return 0
    fi

    # set_file_scanned

    # php "${TRAIN[bin_dir]:?}/sanitize_html.php" || true # Add parameters here
}
