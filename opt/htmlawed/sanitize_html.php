<?php

declare(strict_types = 1);

namespace HTMLawed;

/**
 * Lint and check HTML using htmLawed.
 *
 * https://packagist.org/packages/htmlawed/htmlawed
 *
 * @return array $train  Or, it returns a respond() on error
 */
function sanitize_html(array $train) : array
{
    if ( ! \function_exists('htmLawed')) {
        return $train; // error?
    }

    // https://github.com/kesar/HTMLawed/blob/c733124f21c0330be18d7b0f7f0944a19be6791f/htmLawed.php

    // $sanitized_html = htmLawed($t, $C=1, $S=array());

    // return $sanitized_html;
}
