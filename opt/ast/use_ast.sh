#!/bin/bash

# PHP-AST (Abstract Syntax Tree)
# https://pecl.php.net/package/ast
function use_ast {
    echo "${FUNCNAME[0]}():"

    # Ensure this script runs only when needed
    if [[ "${TRAIN[task]:?}" == "install" ]] && \
       [[ "${TRAIN[dockerized]:?}" == "no" ]] && \
       ! php -m | grep -q '^ast$'; then

        if ! command -v pecl &> /dev/null; then
            echo "  Error: Install PECL to install ast"
            return 1
        fi

        # Install required build dependencies
        if ! apt-get install -yq build-essential gcc make autoconf pkg-config; then
            echo "  Error: Failed to install build dependencies."
            return 1
        fi

        echo "  Installing PHP-AST . . ."

        php_config_path=$(command -v php-config)

        # Ensure php-config is available
        if [[ -z "$php_config_path" ]]; then
            echo "  Error: php-config not found. Ensure PHP development packages are installed."
            return 1
        fi

        # Install PHP-AST via PECL
        if ! PHP_CONFIG="$php_config_path" pecl install ast; then
            echo "  Error: Failed to install PHP-AST."
            return 1
        fi

        php_ini_dir=$(php --ini | grep 'Scan for additional .ini files' | awk -F': ' '{print $2}')

        # Ensure PHP ini directory exists
        if [[ -z "$php_ini_dir" ]]; then
            echo "  Error: Could not determine PHP ini directory."
            return 1
        fi

        ast_ini_file="$php_ini_dir/20-ast.ini"

        # Ensure extension=ast.so is not already present
        if grep -q 'extension=ast.so' "$ast_ini_file" 2>/dev/null; then
            echo "  PHP-AST is already enabled in $ast_ini_file."
            return 0
        fi

        echo "extension=ast.so" > "$ast_ini_file"

        echo "  PHP-AST has been enabled in $ast_ini_file."

        # Verify PHP-AST installation
        if ! php -m | grep -q '^ast$'; then
            echo "  Error: PHP-AST installation failed or is not enabled."
            return 1
        fi

        echo "  PHP-AST installed and enabled successfully."
    fi

}
