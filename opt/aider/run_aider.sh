#!/bin/bash

# Run Aider
# https://github.com/paul-gauthier/aider
# https://aider.chat
# https://www.linkedin.com/in/paulgauthier
function run_aider {
    echo "${FUNCNAME[0]}():"

    # Install Aider
    if [[ "${TRAIN[task]:?}" == "install" ]] \
        && command -v pipx &> /dev/null \
        && [[ "${TRAIN[dockerized]:?}" == "no" ]]; then

        # Install aider and indent output by 2 spaces
        pipx install aider-chat 2>&1 | while IFS= read -r line; do
            echo "  $line"
        done

        # Register Python argument completion for pipx
        eval "$(register-python-argcomplete pipx)"

        # Ensure pipx's bin directory is in the PATH
        pipx ensurepath
    fi

    # Only run Aider when the flaw fixes are going to be saved
    if [[ "${TRAIN[task]:?}" != "fix" &&
        "${TRAIN[task]:?}" != "run_aider" ]] \
        || [[ "${TRAIN[manage_file]:?}" != "yes" ]]; then
        echo "  Use 'mm fix' or 'mm run_aider' to run Aider"
        return 0
    fi

    # Configure Aider
    # https://aider.chat/docs/scripting.html
    # https://aider.chat/docs/config/options.html --commit, --commit-prompt PROMPT
    # --test ? Call Aider when the git commit should be done? code, tools, tests

    # todo:: Run Aider just to output the repo map

    # Change the file_path so that files can be ported
    local file_path="${TRAIN[file_path]}"

    # Add this export when copying and pasting the command in the console
    # command+="export OLLAMA_API_BASE=http://127.0.0.${TRAIN[mm_id]:?}:11434 &&\n"
    command="/opt/pipx/bin/aider "
    command+="${file_path} \n"
    command+="--file ${file_port_path} \n"
    command+="--model ${TRAIN[ollama_llm_model]:?} \n"
    # MicroManager will have thousands more I/O writes and reads if this is set
    command+="--message-file ${TRAIN[app_logs_dir]}/prompt_file.log \n"
    command+="--map-tokens 0 "
    # Only run Aider when the flaw fixes are going to be written
    command+="--dark-mode "
    # --cache-prompts requires --no-stream
    command+="--no-stream "
    command+="--cache-prompts "
    # MicroManager only commits files after they are flawless
    # https://aider.chat/docs/git.html
    command+="--no-git "
    command+="--no-dirty-commits \n"
    # Save costs and code faster https://aider.chat/docs/usage/caching.html
    # Reduce Aider responsibility
    command+="--no-auto-test "
    # MicroManager automatically updates the various tools
    command+="--no-check-update "
    # https://aider.chat/docs/usage/modes.html code|ask|help
    command+="--chat-mode code \n"
    # Always say yes to every confirmation since MicroManager is fully-automated
    command+="--yes "
    # MicroManager will test files after they are flawless
    # https://aider.chat/docs/faq.html
    command+="--edit-format whole "
    # See the raw data between Aider and the LLM https://aider.chat/docs/faq.html
    # todo:: Change this line to command+="--no-pretty " after testing MicroManager
    command+="--verbose --no-pretty "
    command+="--encoding=utf-8 \n"
    command+="--input-history-file ${TRAIN[app_logs_dir]}/aider_input_history.log \n"
    command+="--chat-history-file ${TRAIN[app_logs_dir]}/aider_chat_history.log \n"
    command+="--model-metadata-file /opt/aider/.aider.model.metadata.json "

    # Print the command with line breaks indented 2 spaces
    echo -e "$command" | while IFS= read -r line; do
        echo "  $line"
    done

    # todo:: Remove this from here and leave it in run_ollama
    export OLLAMA_API_BASE="http://gpu${TRAIN[mm_id]}:11434"

    # Remove the line breaks so it executes
    command=$(echo -e "$command" | tr -d '\n')

    # Run Aider
    # capture stdout and stderr separately
    error_output=$({ ${command} 1> "${TRAIN[app_logs_dir]}/aider_output.log"; } 2>&1)
    exit_code=$?

    # todo:: Mark .tt files "fixed", or "passed", or something to prevent the 3 processes
    set_file_scanned

    echo " exit_code $exit_code"
    echo "  error_output $error_output"

    # Check if the exit code indicates an error
    if [ $exit_code -gt 0 ]; then
        # Clean and format the error message
        flaw=$(clean_flaw "${error_output} Exit code $exit_code")
        file_flaws="[{\"file_path\":\"${TRAIN[file_path]:?}\",\"line\":0,\"column\":0,\"flaw\":\"${flaw}\"}]"

        # Save the error as JSON in the log file
        echo "${file_flaws}" > "${TRAIN[file_log_path]}"

        # todo:: Implement this
        # insert_file_flaws

        return 1
    fi

    # Display the aider_output here (with color)
    less -R "${TRAIN[app_logs_dir]}/aider_output.log"
}
