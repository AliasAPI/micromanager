#!/bin/bash

# Run Psalm to analyze PHP applications and finding errors
# https://github.com/vimeo/psalm
# Psalm plugin list: https://packagist.org/?type=psalm-plugin
# https://psalm.dev/articles/detect-security-vulnerabilities-with-psalm
# https://github.com/vimeo/psalm/issues/1135
# (because it must process the entire codebase)
# Run Psalm on specific files: https://psalm.dev/docs/running_psalm/command_line_usage/
function run_psalm {
    echo "${FUNCNAME[0]}():"

    # Install Psalm if necessary
    if [[ "${TRAIN[task]:?}" == "install" ]] \
        && [ ! -f "${TRAIN[bin_dir]:?}/psalm" ]; then
        cd "${TRAIN[toolbox]:?}" || return 1

        composer require --dev vimeo/psalm

        install_psalm_plugins
    fi

    # Configure Psalm
    # https://psalm.dev/docs/running_psalm/configuration/
    if [[ "${TRAIN[task]:?}" == "install" ||
        "${TRAIN[task]:?}" == "config" ]] \
        && [ ! -f "${TRAIN[toolbox]:?}/psalm.xml" ]; then

        if [ ! -f "${TRAIN[opt_dir]}/psalm/psalm.xml" ]; then
            echo "  The psalm.xml configuration file is missing"
            exit 1
        fi

        cp "${TRAIN[opt_dir]}/psalm/psalm.xml" "${TRAIN[toolbox]:?}"
    fi

    if [[ "${TRAIN[manage_file]:?}" != "yes" &&
        "${TRAIN[task]:?}" != "${FUNCNAME[0]}" ]]; then
        return 0
    fi

    # Configure the command (line) to run Psalm
    command="php ${TRAIN[bin_dir]:?}/psalm "

    # Add parameters to get valid JSON as output
    command+="--config=${TRAIN[toolbox]:?}/psalm.xml --output-format=json --no-progress "

    # Add a parameter to automatically fix files
    if [[ "${TRAIN[task]:?}" == "fix" ]]; then
        command+="--alter "
    fi

    # If there is no log file, just return
    if [[ ! -f "${TRAIN[file_log_path]:?}" ]]; then
        return 0
    fi

    # Add the specific file (or directory) to process
    command+="${TRAIN[file_path]:?}"

    # https://psalm.dev/docs/running_psalm/configuration/
    export XDG_CACHE_HOME="${TRAIN[app_data_files]:?}/.psalm"

    # Run Psalm
    if [[ "${TRAIN[file_type]:?}" != "php" ]]; then
        echo "  Skipping [${TRAIN[file_type]:?}] file"
        return 0
    fi

    set_file_scanned

    cd "${TRAIN[git_app_path]:?}" || return 1

    # Run the pre-configured Psalm command
    set +e
    error=$(${command} 2>&1 1> "${TRAIN[file_log_path]}")
    exit_code=$?
    set -e

    if [[ "${exit_code}" -ne 0 &&
        "${exit_code}" -ne 2 ]]; then
        echo -e "  Error: ${error}\n  Exit code: ${exit_code}" >> "${TRAIN[file_log_path]}"
    fi

    # Extract the locations of the flaws and the warning messages
    # file_flaws=$(jq -c '.[] |
    #     {file_path: .file_path, line: .line_from, column: .column_from, flaw: .message}' "${TRAIN[file_log_path]}")

    # insert_file_flaws "${file_flaws}" "psalm"
}
