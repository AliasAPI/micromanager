#!/bin/bash

# List of plugins that can fix code: https://github.com/orklah
function install_psalm_plugins {
    echo "${FUNCNAME[0]}():"

    if [[ "${TRAIN[task]:?}" != "install" ]]; then
        return 0
    fi

    cd "${TRAIN[toolbox]:?}" || return 1

    # todo:: Make sure each of the Psalm plugin below works

    # Optional. A list of <plugin filename="path_to_plugin.php" /> entries. See the Plugins section for more information.

    # https://github.com/php-standard-library/psalm-plugin
    # if [ ! -f "${TRAIN[bin_dir]:?}/psalm-plugin" ]; then
    #     composer require --dev php-standard-library/psalm-plugin
    #     php "${TRAIN[bin_dir]:?}/psalm-plugin enable php-standard-library/psalm-plugin"
    # fi

    # @why Analyze code while writing unit tests ?
    # https://github.com/psalm/psalm-plugin-phpunit
    # if [ ! -f "${TRAIN[toolbox]:?}/vendor/psalm/plugin-phpunit/src/Plugin.php" ]; then
    #     composer require --dev psalm/plugin-phpunit
    #     php "${TRAIN[bin_dir]:?}/psalm-plugin enable psalm/plugin-phpunit"
    # fi

    # @why Detect possible insane comparisons ("string" == 0) to help migrate to PHP8
    # https://github.com/orklah/psalm-insane-comparison
    # if [ ! -f "${TRAIN[toolbox]:?}/vendor/orklah/psalm-insane-comparison/Plugin.php" ]; then
    #     composer require --dev orklah/psalm-insane-comparison
    #     php "${TRAIN[bin_dir]:?}/psalm-plugin enable orklah/psalm-insane-comparison"
    # fi

    # @why Detect calling private or protected method via proxy
    # https://github.com/orklah/psalm-strict-visibility
    # if [ ! -f "${TRAIN[toolbox]:?}/vendor/orklah/psalm-strict-visibility/Plugin.php" ]; then
    #     composer require --dev orklah/psalm-strict-visibility
    #     php "${TRAIN[bin_dir]:?}/psalm-plugin enable orklah/psalm-strict-visibility"
    # fi

    # @why Add strict_types declaration when the file is provably safe
    # https://github.com/orklah/psalm-strict-types
    # if [ ! -f "${TRAIN[toolbox]:?}/vendor/orklah/psalm-strict-visibility/Plugin.php" ]; then
    #     composer require --dev orklah/psalm-strict-types --ignore-platform-reqs
    #     php "${TRAIN[bin_dir]:?}/psalm-plugin enable orklah/psalm-strict-types"
    # fi

    # @why Restrict the use of (int) and (float) to numeric-string only
    # https://github.com/orklah/psalm-strict-numeric-cast
    # if [ ! -f "${TRAIN[toolbox]:?}/vendor/orklah/psalm-strict-numeric-cast/Plugin.php" ]; then
    #     composer require --dev orklah/psalm-strict-numeric-cast
    #     php "${TRAIN[bin_dir]:?}/psalm-plugin enable orklah/psalm-strict-numeric-cast"
    # fi

    # @why Change == into === in PHP files when safe
    # https://github.com/orklah/psalm-strict-equality
    # if [ ! -f "${TRAIN[toolbox]:?}/vendor/orklah/psalm-strict-equality/src/Plugin.php" ]; then
    #     composer require --dev orklah/psalm-strict-equality
    #     php "${TRAIN[bin_dir]:?}/psalm-plugin enable orklah/psalm-strict-equality"
    # fi

    # @why Change empty() into a more explicit expression
    # https://github.com/orklah/psalm-not-empty
    # if [ ! -f "${TRAIN[toolbox]:?}/vendor/orklah/psalm-not-empty/src/Plugin.php" ]; then
    #     composer require --dev orklah/psalm-not-empty
    #     php "${TRAIN[bin_dir]:?}/psalm-plugin enable orklah/psalm-not-empty"
    # fi

    # @why Fix the code to use existing constants instead of literals when available
    # https://github.com/orklah/psalm-use-constants
    # if [ ! -f "${TRAIN[toolbox]:?}/vendor/orklah/psalm-use-constants/Plugin.php" ]; then
    #     composer require --dev --ignore-platform-reqs --with-all-dependencies orklah/psalm-use-constants:*
    #     php "${TRAIN[bin_dir]:?}/psalm-plugin enable orklah/psalm-use-constants"
    # fi

    # @why Use property type to fix param in the setter
    # https://github.com/orklah/psalm-type-setters-params
    # if [ ! -f "${TRAIN[toolbox]:?}/vendor/orklah/psalm-type-setters-params/Plugin.php" ]; then
    #     composer require --dev orklah/psalm-type-setters-params:* --ignore-platform-reqs --with-all-dependencies
    #     php "${TRAIN[bin_dir]:?}/psalm-plugin enable orklah/psalm-type-setters-params"
    # fi

    # @why FIX THIS: Let Psalm understand better psr11 containers
    # https://github.com/Lctrs/psalm-psr-container-plugin
    # if [ ! -f "${TRAIN[toolbox]:?}/vendor/lctrs/psalm-psr-container-plugin/src/Plugin.php" ]; then
    #     composer require --dev lctrs/psalm-psr-container-plugin
    #     php "${TRAIN[bin_dir]:?}/psalm-plugin enable lctrs/psalm-psr-container-plugin"
    # fi

    # @why Checks the sprintf, printf, sscanf and fscanf function usage
    # https://github.com/boesing/psalm-plugin-stringf
    # if [ ! -f "${TRAIN[toolbox]:?}/vendor/boesing/psalm-plugin-stringf/src/Plugin.php" ]; then
    #     composer require --dev boesing/psalm-plugin-stringf
    #     php "${TRAIN[bin_dir]:?}/psalm-plugin enable boesing/psalm-plugin-stringf"
    # fi

    # @why Enforce final on PHP classes to reduce inheritance
    # https://github.com/cspray/phinal
    # if [ ! -f "${TRAIN[toolbox]:?}/vendor/cspray/phinal/src/Plugin.php" ]; then
    #     composer require --dev --ignore-platform-reqs --with-all-dependencies cspray/phinal
    #     php "${TRAIN[bin_dir]:?}/psalm-plugin enable ${TRAIN[toolbox]:?}/vendor/cspray/phinal"
    # fi

    # @why Make sure there aren't too many arguments in functions and methods
    # https://github.com/kafkiansky/reduce-arguments
    # if [ ! -d "${TRAIN[toolbox]:?}/vendor/kafkiansky/reduce-arguments" ]; then
    #     composer require --dev --ignore-platform-reqs --with-all-dependencies kafkiansky/reduce-arguments:*
    #     php "${TRAIN[bin_dir]:?}/psalm-plugin enable kafkiansky/reduce-arguments"
    # fi

    # @why Stubs to let Psalm understand psr/log (PSR-3) strictly
    # https://packagist.org/packages/struggle-for-php/sfp-psalm-psr-log-plugin
    # if [ ! -f "${TRAIN[toolbox]:?}/vendor/struggle-for-php/sfp-psalm-psr-log-plugin/src/Plugin.php" ]; then
    #     composer require --dev struggle-for-php/sfp-psalm-psr-log-plugin
    #     php "${TRAIN[bin_dir]:?}/psalm-plugin enable struggle-for-php/sfp-psalm-psr-log-plugin"
    # fi

    # @why Find mismatch type assignment in function/method scope with psalm
    # https://github.com/struggle-for-php/sfp-psalm-typed-local-variable-plugin
    # if [ ! -d "${TRAIN[toolbox]:?}/vendor/struggle-for-php/sfp-psalm-typed-local-variable-plugin" ]; then
    #     composer require --dev --ignore-platform-reqs --with-all-dependencies struggle-for-php/sfp-psalm-typed-local-variable-plugin:*
    #     php "${TRAIN[bin_dir]:?}/psalm-plugin enable struggle-for-php/sfp-psalm-typed-local-variable-plugin"
    # fi

    # @why Add sinks for all file functions that accept streams
    # https://github.com/pizzeys/funserialize
    # if [ ! -d "${TRAIN[toolbox]:?}/vendor/pizzeys/funserialize" ]; then
    #     composer require --dev pizzeys/funserialize
    #     php "${TRAIN[bin_dir]:?}/psalm-plugin enable pizzeys/funserialize"
    # fi
}
