#!/bin/bash

# Run TikToken
# https://github.com/openai/tiktoken
function run_tiktoken {
    echo "${FUNCNAME[0]}():"

    # Install TikToken
    if [[ "${TRAIN[task]:?}" == "install" ]] \
        && [[ "${TRAIN[dockerized]:?}" == "no" ]]; then
        cd "/opt" || return 1

        # Create a virtual environment if it doesn't exist
        if [[ ! -d "tiktoken_env" ]]; then
            echo "  Creating virtual environment for Tiktoken . . ."
            python3 -m venv tiktoken_env
        fi

        # Activate the virtual environment
        echo "  Activating virtual environment . . ."
        source tiktoken_env/bin/activate

        echo "  Installing chardet . . ."
        pip install chardet 2>&1 | while IFS= read -r line; do
            echo "    $line"
        done

        # Install TikToken and indent output by 2 spaces
        echo "  Installing TikToken . . ."
        pip install tiktoken --upgrade 2>&1 | while IFS= read -r line; do
            echo "    $line"
        done

        deactivate

        echo "  TikToken installation complete."
    fi

    # Configure TikToken
    export MAX_PROMPT_SIZE="4000"

    export TIKTOKEN_MODEL="gpt-3.5-turbo"

    # Check the requirements to use TikToken 
    if [[ "${TRAIN[task]:?}" != "fix" &&
        "${TRAIN[task]:?}" != "install" &&
        "${TRAIN[task]:?}" != "run_tiktoken" ]] \
        || [[ "${TRAIN[dockerized]:?}" != "yes" ]] \
        || [[ -z "${TRAIN[file_path]:-}" ]] \
        || [[ ! -s "${TRAIN[file_path]}" ]]; then
        return 0
    fi
 
    # Load the file_path content into a variable
    # The max prompt size is low enough for this
    export PROMPT_TEXT=$(<"${TRAIN[file_path]}")
    
    # Run TikToken
    if [[ ! -f "/opt/tiktoken_env/bin/activate" ]]; then
        echo "  Call run_tiktoken when building the Docker container"
        return 1
    fi
   
    # If the virtual environment is not started, start it
    if [[ -z "${VIRTUAL_ENV:-}" ]]; then
        source "/opt/tiktoken_env/bin/activate"
    fi

    # Run Tiktoken to calculate the tokens in the file_path file
    file_tokens=$(python3 "${TRAIN[opt_dir]:?}/tiktoken/count_tokens.py")

    set_train "file_tokens" "$file_tokens"

    deactivate
}
