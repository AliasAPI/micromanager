#!/bin/bash

# create_prompt_file writes a LLM prompt in a file
# NOTE Aider uses the file_path to add the code regardless of the prompt file
function create_prompt_file {
    echo "${FUNCNAME[0]}():"

    local prompt=""
    export MAX_PROMPT_SIZE="4000"
    export TIKTOKEN_MODEL="gpt-3.5-turbo"

    if [[ "${TRAIN[manage_file]:?}" != "yes" ]]; then
        return 0
    fi

    if [[ -z "${TRAIN[file_tokens]:?}" ]]; then
        return 1
    fi

    if [[ -z "${TRAIN[file_tokens]}" || 
           ! "${TRAIN[file_tokens]}" =~ ^[0-9]+$ ]]; then
        echo "  Set and integer for file_tokens"
        return 1
    fi

    # Go to the Tiktoken virtual environment
    cd "/opt" || return 1

    # Set MAX_PROMPT_SIZE if not already set
    MAX_PROMPT_SIZE=${MAX_PROMPT_SIZE:-4000}
  
    # Add a newline
    prompt+=$'\n'

      # Specify the language
    prompt+="Write this pseudocode as ${TRAIN[file_type_port]:?}:"
  
    prompt+=$'\n\n'
    
    # Including the file_port_path for aider
    prompt+="${TRAIN[file_port_path]:?}"
  
    # Add ``` as a fence around the text in the prompt file
    prompt+=$'\n```\n'

    # Set the text in the file_path as primary_prompt
    prompt+=$(cat "${TRAIN[file_path]}")
    
    prompt+=$'\n```\n'

    # Extract the directory_path from the file_path
    directory_path=$(dirname "${TRAIN[file_path]}")

    # If the file is in the git_app_path, it is probably code, so add line numbers
    # if [[ "$directory_path" == "${TRAIN[git_app_path]}"* ]]; then
    #     primary_prompt=$(awk '{printf "%4d: %s\n", NR, $0}' "${TRAIN[file_path]}")
    # fi

    local secondary_prompt=""

    # # Add the file_flaws to the prompt file
    # if [[ -n "${TRAIN[file_flaws]:-}" ]]; then
    #     secondary_prompt+="Fix the following flaws: "
    #     secondary_prompt+=$'\n'

    #     # Calculate the remaining tokens for the prompt
    #     remaining_tokens=$((MAX_PROMPT_SIZE - "${TRAIN[file_tokens]}"))

    #     # Set file_flaws as the secondary_prompt, removing \n
    #     secondary_prompt+=$(echo -e "${TRAIN[file_flaws]}")

    #     export PROMPT_TEXT="$secondary_prompt"

    #     # Run count_tokens which expects PROMPT_TEXT
    #     # current_tokens=$(python3 "/opt/tiktoken/count_tokens.py")
    #     run_tiktoken

    #     echo "  current_tokens: ${TRAIN[file_tokens]:?}"

    #     while [[ "${TRAIN[file_tokens]}" -gt $remaining_tokens && -n "$PROMPT_TEXT" ]]; do
    #         # Remove the last line from the PROMPT_TEXT
    #         secondary_prompt=$(echo "$PROMPT_TEXT" | sed '$d')

    #         export PROMPT_TEXT="$secondary_prompt"

    #         # current_tokens=$(python3 "/opt/tiktoken/count_tokens.py")
    #         run_tiktoken
    #     done
    # fi

    if [[ -n "$secondary_prompt" ]]; then
        prompt+=$'\n\n'"$secondary_prompt"
    fi

    prompt_file="${TRAIN[app_logs_dir]}/prompt_file.log"

    # Write the prompt to the prompt_file
    echo "$prompt" > "$prompt_file"

    # Echo the file contents while indenting lines 2 spaces
    echo -e "$prompt" | while IFS= read -r line; do
        echo "  $line"
    done
}
