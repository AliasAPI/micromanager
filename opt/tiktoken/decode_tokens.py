import tiktoken
import sys
import os
import json

def decode_tokens():
    # Get tokens and model from environment variables
    tokens_str = os.getenv('OLLAMA_RESPONSE_CONTEXT')
    model = os.getenv('TIKTOKEN_MODEL')

    # If the tokens string is empty, return an empty string
    if not tokens_str:
        return ""

    if not model:
        raise ValueError("TIKTOKEN_MODEL environment variable is not set")

    try:
        # Initialize the tokenizer
        encoding = tiktoken.encoding_for_model(model)
    except KeyError:
        raise ValueError(f"Invalid or unsupported model: {model}")

    try:
        # Parse the tokens string into a list of integers
        tokens = json.loads(tokens_str)
        if not isinstance(tokens, list):
            raise ValueError("Tokens should be a list of integers")
    except json.JSONDecodeError:
        raise ValueError("Invalid JSON format for tokens")

    # Decode the tokens
    decoded_text = encoding.decode(tokens)
    return decoded_text

if __name__ == "__main__":
    try:
        decoded_text = decode_tokens()
        print(decoded_text)
    except Exception as e:
        print(f"Error: {str(e)}", file=sys.stderr)
        sys.exit(1)
