import tiktoken
import sys
import os

def count_tokens():
    # Get text and model from environment variables
    prompt_text = os.getenv('PROMPT_TEXT')

    model = os.getenv('TIKTOKEN_MODEL')

    # If the prompt_text is empty, return 0 tokens 
    if not prompt_text:
        return 0

    if not model:
        raise ValueError("TIKTOKEN_MODEL environment variable is not set")

    try:
        # Initialize the tokenizer
        encoding = tiktoken.encoding_for_model(model)
    except KeyError:
        raise ValueError(f"Invalid or unsupported model: {model}")

    # Calculate total tokens
    total_tokens = len(encoding.encode(prompt_text))
    return total_tokens

if __name__ == "__main__":
    try:
        token_count = count_tokens()
        print(token_count)
    except Exception as e:
        print(f"Error: {str(e)}", file=sys.stderr)
        sys.exit(1)
