#!/bin/bash

# Run Ollama to run large language models locally
# https://github.com/ollama/ollama
function run_ollama {
    echo "${FUNCNAME[0]}():"

	export GIN_MODE=release
    export OLLAMA_HOST=0.0.0.0 
    export OLLAMA_ORIGINS=* 

    # Install Ollama
    # The run_ollama function is run from a Dockerfile
    # The installation is located in the host computer
    # Checking the Ollama command stops reinstallation
    if [[ "${TRAIN[task]:?}" == "install" ]] \
        && [[ "${TRAIN[dockerized]:?}" == "no" ]] \
        && command -v poetry &> /dev/null \
        && ! command -v ollama &> /dev/null; then

        # Copy the Python project settings file (with the Ollama dependencies) to /app
        if [ ! -f "${TRAIN[git_app_path]:-/app}/pyproject.toml" ]; then
            echo "  Copying the pyproject.toml file that defines Ollama dependencies . . ."
            cp "/tmp/ollama/pyproject.toml" /app
        fi

        export DEBIAN_FRONTEND=noninteractive

        # Install nc to see if the IP and port are already in use
        apt-get install -y --no-install-recommends netcat-traditional | while IFS= read -r line; do
            echo "  $line"
        done

        if ! command -v nc &> /dev/null; then
            echo "  Try installing netcat-traditional again"
            exit 1
        fi

        # Install the Ollama dependencies
        poetry install --no-root --extras "ui vector-stores-qdrant llms-ollama embeddings-ollama" | while IFS= read -r line; do
            echo "  $line"
        done

        echo "  Installing Ollama . . ."

        if ! mkdir -p "${TRAIN[app_logs_dir]}"; then
            echo "Failed to create ${TRAIN[app_logs_dir]} directory"
            exit 1
        fi

        # Run the installation script in a subprocess (and suppress the annoying red output)
        curl -fsSL https://ollama.com/install.sh | sh > /tmp/ollama_install.log 2>&1

        # Process the captured output to remove ANSI color codes and indent each line
        sed 's/\x1b\[[0-9;]*m//g' /tmp/ollama_install.log | while IFS= read -r line; do
            echo "  $line"
        done

        # Clean up
        rm /tmp/ollama_install.log

        source /opt/poetry/venv/bin/activate

        pip install injector pydantic pyyaml fastapi llama-index transformers uvicorn gradio | while IFS= read -r line; do
            echo "  $line"
        done

        deactivate

        # Install netcat-traditional to get nc (which is used below to see if the Ollama host is in use)
        if ! command -v nc &> /dev/null; then
            apt-get install -y --no-install-recommends netcat-traditional | while IFS= read -r line; do
                echo "  $line"
            done
        fi

        unset DEBIAN_FRONTEND

        echo " "
    fi

    # Configure Ollama
    set_train "ollama_api_base" "http://gpu1:11434"

    export OLLAMA_MODELS="/data/ollama/models"
    export OLLAMA_HOST="0.0.0.0:11434"

    # Format the host value for netcat
    export OLLAMA_NETCAT_HOST="0.0.0.0 11434"
    export OLLAMA_RESPONSE_FORMAT=json
    export OLLAMA_MAX_ATTEMPTS=3

    if [[ "${TRAIN[task]:?}" == "install" ||
        "${TRAIN[task]:?}" == "restart" ||
        "${TRAIN[task]:?}" == "config" ]] \
        && [[ "${TRAIN[dockerized]:?}" == "yes" ]] \
        && command -v ollama &> /dev/null; then
        # Check if the netcat address is already used 
        if nc -z "${TRAIN[ollama_netcat_host]}"; then
            echo "  Ollama address ${TRAIN[ollama_netcat_host]} is already in use"
            return 0
        fi

        # Start Ollama in a separate process
        setsid ollama serve > ${TRAIN[app_logs_dir]:?}/ollama.log 2>&1 &
        ollama_pid=$!

        echo "  Ollama is being started in a separate process (PID: $ollama_pid)"
        echo "  The logs are being written to ${TRAIN[app_logs_dir]:?}/ollama.log"

        timer=0
        ollama_status=""

        echo "  Waiting for Ollama to start . . . "

        set +e

        while [ "$ollama_status" != "Ollama is running" ] && [ $timer -le 10 ]; do
            ollama_status=$(curl -s -f -o /dev/null -w "%{http_code}" "${TRAIN[ollama_host]}" 2> /dev/null)

            curl_exit_code=$?

            if [ $curl_exit_code -eq 0 ] && [ "$ollama_status" = "200" ]; then
                ollama_status="Ollama is running"
            fi

            sleep 1

            timer=$((timer + 1))
            echo "  [$timer]"
        done

        set -e

        echo "  $ollama_status"

        echo "  Pulling Ollama models . . ."
        # Define the models to download and use
        ollama_models="${TRAIN[ollama_llm_model]:?} ${TRAIN[ollama_llm_embed]:?}"

        # Get the list of installed models
        installed_models=$(ollama list)

        # Loop through each model in the list
        for model in $ollama_models; do
            # If the Ollama model does NOT exist
            if ! echo "$installed_models" | grep -q "$model"; then
                echo "  Downloading the [$model] model . . ."
                ollama pull "$model"

                if [ $? -ne 0 ]; then
                    echo "  Error [$?]: Failed to pull the ${model} model"
                    exit 1
                fi
            fi
        done

        echo "  All models downloaded successfully!"
    fi

    # Use Ollama (is faciliated by APE)

    # Process the output (is facilitated by APE)
}
