#!/bin/bash

function use_ollama {
    echo "${FUNCNAME[0]}():"

    export GIN_MODE=release
    export OLLAMA_HOST="0.0.0.0"
    export OLLAMA_ORIGINS="*"

    echo "  Executing the Ollama command . . ."

    if [[ -z "${TRAIN[ollama_command]:-}" ]]; then
        echo "  Error: TRAIN[ollama_command] is not set."
        return 1
    fi

    attempt=0
    max_attempts="${TRAIN[ollama_max_attempts]:-3}"

    while [[ $attempt -lt $max_attempts ]]; do
        echo "  Attempt $((attempt + 1))/$max_attempts"

        response_file=$(mktemp)

        # Execute Ollama command, capturing response and status code
        http_code=$(bash -c "${TRAIN[ollama_command]}" >"$response_file"; echo $?)

        echo "  HTTP Status Code: $http_code"

        if [[ $http_code -ne 0 ]]; then
            echo "  Error: HTTP request failed with status code $http_code"
            cat "$response_file"
            rm "$response_file"
            ((attempt++))
            sleep 1
            continue
        fi

        response=$(<"$response_file")
        rm "$response_file"

        # Print the raw response to debug its structure
        echo "  Raw Response: $response"

        # Ensure the response is valid JSON
        if jq . <<<"$response" >/dev/null 2>&1; then
            echo "  Valid JSON response received"
            break
        fi

        echo "  Warning: Ollama response is not valid JSON (Attempt $((attempt + 1))/$max_attempts)"
        ((attempt++))
        sleep 1
    done

    if [[ $attempt -eq $max_attempts ]]; then
        echo "  Error: Ollama failed to return valid JSON after $max_attempts attempts"
        return 1
    fi

    # Check if the response field is a valid string (i.e., JSON string)
    if echo "$response" | jq -e '.response' >/dev/null; then
        response_text=$(echo "$response" | jq -r '.response')
        # Check if the response is a JSON string that needs parsing
        if echo "$response_text" | jq . >/dev/null 2>&1; then
            response_text=$(echo "$response_text" | jq -c .)
        fi
    else
        response_text="No response text available"
    fi

    # Ensure fields exist and handle missing values with defaults
    context=$(echo "$response" | jq -r '.context // "null"')
    done_reason=$(echo "$response" | jq -r '.done_reason // "unknown"')
    response_model=$(echo "$response" | jq -r '.model // "unknown"')
    total_duration=$(echo "$response" | jq -r '.total_duration // 0')

    # Set the response variables properly
    set_train "ollama_response_text" "$response_text"
    set_train "ollama_response_context" "$context"
    set_train "ollama_done_reason" "$done_reason"
    set_train "ollama_response_model" "$response_model"
    set_train "ollama_total_duration" "$total_duration"

    return 0
}
