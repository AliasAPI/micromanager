#!/bin/bash

# https://github.com/ollama/ollama/blob/main/docs/api.md
function set_ollama_command {
    echo "${FUNCNAME[0]}():"

    # Only run Ollama when the flaw fixes are going to be saved
    if [[ "${TRAIN[task]:?}" != "fix" && 
          "${TRAIN[task]:?}" != "use_ollama" ]] || 
       [[ "${TRAIN[manage_file]:?}" != "yes" ]]; then
        echo "  Use 'mm fix' or 'mm use_ollama' to query Ollama"
        return 0
    fi

    # Check required parameters
    required_parameters=(
        "app_logs_dir"
        "ollama_api_base"
        "ollama_llm_model"
        "ollama_max_attempts"
        "ollama_response_format"
    )

    for parameter in "${required_parameters[@]}"; do
        if [[ -z "${TRAIN[$parameter]:-}" ]]; then
            echo "  Error: Missing required parameter TRAIN[$parameter]"
            return 1
        fi
    done

    # Set Ollama the options in an array and convert it to JSON
    declare -A options
    options[repeat_penalty]=1.1
    options[mirostat]=0
    options[mirostat_eta]=0.5
    options[mirostat_tau]=5.0
    options[num_ctx]=4000
    options[repeat_last_n]=64
    options[temperature]=0
    options[seed]=42
    options[stop]='["AI assistant"]'
    options[num_predict]=42
    options[top_k]=40
    options[top_p]=0.9
    options[min_p]=0.05

    # Convert the associative array 'options' to JSON using jq
    options_json=$(jq -n \
        --arg repeat_penalty "${options[repeat_penalty]}" \
        --arg mirostat "${options[mirostat]}" \
        --arg mirostat_eta "${options[mirostat_eta]}" \
        --arg mirostat_tau "${options[mirostat_tau]}" \
        --arg num_ctx "${options[num_ctx]}" \
        --arg repeat_last_n "${options[repeat_last_n]}" \
        --arg temperature "${options[temperature]}" \
        --arg seed "${options[seed]}" \
        --argjson stop "${options[stop]}" \
        --arg num_predict "${options[num_predict]}" \
        --arg top_k "${options[top_k]}" \
        --arg top_p "${options[top_p]}" \
        --arg min_p "${options[min_p]}" \
        '{
            repeat_penalty: $repeat_penalty | tonumber,
            mirostat: $mirostat | tonumber,
            mirostat_eta: $mirostat_eta | tonumber,
            mirostat_tau: $mirostat_tau | tonumber,
            num_ctx: $num_ctx | tonumber,
            repeat_last_n: $repeat_last_n | tonumber,
            temperature: $temperature | tonumber,
            seed: $seed | tonumber,
            stop: $stop,
            num_predict: $num_predict | tonumber,
            top_k: $top_k | tonumber,
            top_p: $top_p | tonumber,
            min_p: $min_p | tonumber
        }')

    prompt_file="${TRAIN[app_logs_dir]}/prompt_file.log"

    if [[ ! -f "$prompt_file" ]]; then
        echo "  Error: Prompt file not found at $prompt_file"
        return 1
    fi

    prompt_file_content=$(<"$prompt_file")

    "messages": [
     {
                "role": "system",
                "content": "You are an expert PHP developer. \nWrite this pseudocode as PHP:\n```\narea bank_accounts\n\nstation set_bank_account_city[train]\n    if train[wreck] is set\n        return train\n\n    train[log][] = [4, set_bank_account_city:]\n\n    if train[input][bank_account_city] is not set\n        return train\n\n    if train[input][bank_account_city] is not a string\n        train[wreck] = Please enter the text for bank_account_city\n\n    if train[input][bank_account_city] is shorter than 3 characters \n        train[wreck] = Please enter more text for bank_account_city \n\n    if train[input][bank_account_city] is longer than 50 characters \n        train[wreck] = Please enter less text for bank_account_city \n\n    train[bank_account_city] = train[input][bank_account_city]\n\n    train[log][] = [6, bank_account_city has been set to train[bank_account_city]]\n\n    return train\n```"
            }
    ]
    prompt_payload=$(jq -n \
        --arg format "${TRAIN[ollama_response_format]}" \
        --arg model "${TRAIN[ollama_llm_model]}" \
        --argjson options "$options_json" \
        --arg prompt "$prompt_file_content" \
        --argjson stream false \
        '{ 
            format: $format, 
            model: $model, 
            options: $options, 
            prompt: $prompt, 
            stream: $stream
        }')

    ollama_command="curl --request POST \"${TRAIN[ollama_api_base]}/v1/chat/completions\" \
        --header \"Content-Type: application/json\" \
        --data '$prompt_payload' \
        --write-out \"%{http_code}\" "

    set_train "ollama_command" "$ollama_command"

    echo "${TRAIN[ollama_command]}" | while IFS= read -r line; do
        echo "  $line"
    done
exit
    return 0
}
