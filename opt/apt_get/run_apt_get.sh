#!/bin/bash

# Check and beautify JSON files
function run_apt_get {
    echo "${FUNCNAME[0]}():"

    if [[ "${TRAIN[task]}" == "install"
        && "${TRAIN[dockerized]}" == "no" ]]; then
        # Refresh apt-get as a hack solution for the "E: Unable to locate package <package>" error
        echo "  Updating apt-get . . ."
    
        export DEBIAN_FRONTEND=noninteractive

        # apt-get clean

        # apt-get update

        # Do not run apt-get update if it was already run today
        if ! [[ "$(find /var/lib/apt/lists -maxdepth 0 -newermt 'yesterday' 2>/dev/null)" =~ "/var/lib/apt/lists"  ]]; then
            # The following commands prevent the "unable to locate <package>" malfunction
            sudo apt-get clean | while IFS= read -r line; do
                echo "  $line"
            done

            sudo apt-get update | while IFS= read -r line; do
                echo "  $line"
            done

            sudo apt-get install apt-utils | while IFS= read -r line; do
                echo "  $line"
            done

            sudo dpkg --configure -a | while IFS= read -r line; do
                echo "  $line"
            done

            sudo apt-get install -f | while IFS= read -r line; do
                echo "  $line"
            done

            sudo rm -rf /var/lib/apt/lists/*
        fi
    fi
} 