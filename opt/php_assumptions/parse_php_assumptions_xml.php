<?php

declare(strict_types = 1);

function parse_php_assumptions_xml($json_parameters) : void
{
    $file_flaws = [];

    try {
        $params = json_decode((string) $json_parameters, true, 512, \JSON_THROW_ON_ERROR);

        $output = file_get_contents($params['file_log_path']);

        // Remove the unnecessary output from php_assumptions
        // $output = str_replace('PHPAssumptions analyser v0.8.0 by @rskuipers', '', $output);

        $xml = simplexml_load_string($output, 'SimpleXMLElement', LIBXML_NOCDATA | LIBXML_NOERROR | LIBXML_NOWARNING);

#        $xml = simplexml_load_string($output, 'SimpleXMLElement', \LIBXML_NOCDATA | \LIBXML_ERR_FATAL | \LIBXML_ERR_WARNING);

        if ($xml === false) {
            echo "Failed loading XML:\n";
            foreach (libxml_get_errors() as $error) {
                echo "\t jlejl", $error->message;
            }
        }

        die(); 

echo "xml $xml";
        if (mb_strlen($xml) > (int) 5) {
            $array = json_decode(json_encode($xml), true);

            foreach ($array['files']['file'] as $file) {
                $file_path = (string) $file['name'];
                $line = (int) $file['line']['number'];
                $column = 0;
                $flaw = (string) $file['line']['message'];

                $file_flaws .= "[{\"file_path\":\"{$file_path}\",";
                $file_flaws .= "\"line\":{$line},\"column\":{$column},";
                $file_flaws .= "\"flaw\":\"Fix this assumption: {$flaw}\"}]";
            }
        }
    } catch (Exception $exception) {
        $error_message = $exception->getMessage();
        $file_flaws = '[{"file_path":"parse_php_assumptions_xml.php","line":0,"column":0,';
        $file_flaws .= "\"flaw\":\"{$error_message}\"}]";

        // Make PHP "print to the console" so that the Bash run_php_assumptions() function sees it
        echo $file_flaws;
    }


    echo json_encode($file_flaws, JSON_PRETTY_PRINT);
}

// Capture the argument passed from the run_php_assumptions() Bash function
$json_parameters = $argv[1];

// Pass the JSON-encoded string from the run_php_assumptions.sh bash script into parse_php_assumptions_xml()
parse_php_assumptions_xml($json_parameters);
