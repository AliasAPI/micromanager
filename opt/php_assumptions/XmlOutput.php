<?php

declare(strict_types = 1);

namespace PhpAssumptions\Output;

use DOMDocument;
use DOMXPath;
use League\CLImate\CLImate;
use PhpAssumptions\Cli;

final readonly class XmlOutput implements OutputInterface
{
    private DOMDocument $domDocument;

    private DOMXPath $domxPath;

    /**
     * @param CLImate $cli
     */
    public function __construct(private string $file)
    {
        $this->domDocument = new DOMDocument();

        $phpaNode = $this->domDocument->createElement('phpa');
        $phpaNode->setAttribute('version', Cli::VERSION);
        $this->domDocument->appendChild($phpaNode);

        $filesNode = $this->domDocument->createElement('files');
        $phpaNode->appendChild($filesNode);

        $this->domxPath = new DOMXPath($this->domDocument);
    }

    public function output(Result $result) : void
    {
        $assumptions = $result->getAssumptions();

        foreach ($assumptions as $assumption) {
            $fileElements = $this->domxPath->query('/phpa/files/file[@name="'.$assumption['file'].'"]');

            if ($fileElements->length === 0) {
                $files = $this->domxPath->query('/phpa/files')->item(0);
                $fileElement = $this->domDocument->createElement('file');
                $fileElement->setAttribute('name', $assumption['file']);
                $files->appendChild($fileElement);
            } else {
                $fileElement = $fileElements->item(0);
            }

            $lineElement = $this->domDocument->createElement('line');
            $lineElement->setAttribute('number', $assumption['line']);
            $lineElement->setAttribute('message', $assumption['message']);
            $fileElement->appendChild($lineElement);
        }

        $this->domDocument->documentElement->setAttribute('assumptions', $result->getAssumptionsCount());
        $this->domDocument->documentElement->setAttribute('bool-expressions', $result->getBoolExpressionsCount());
        $this->domDocument->documentElement->setAttribute('percentage', $result->getPercentage());

        $this->domDocument->preserveWhiteSpace = false;
        $this->domDocument->formatOutput = true;
        $this->domDocument->save($this->file);

        // $this->cli->out(sprintf('Written %d assumption(s) to file %s', $result->getAssumptionsCount(), $this->file));
    }
}
