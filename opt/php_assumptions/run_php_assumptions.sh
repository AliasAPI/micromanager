#!/bin/bash

# Run PHP Assumptions to automatically check for weak assumptions
# https://github.com/rskuipers/php-assumptions
# https://phpqa.io/projects/php-assumptions.html
function run_php_assumptions {
    echo "${FUNCNAME[0]}():"

    # Install PHP Assumptions
    if [[ "${TRAIN[task]:?}" == "install" ]] \
        && [ ! -f "${TRAIN[bin_dir]:?}/phpa" ]; then
        cd "${TRAIN[toolbox]:?}" || return 1
        
        composer require --dev rskuipers/php-assumptions
    fi

    # Configure PHP Assumptions
    # if [[ "${TRAIN[task]:?}" == "install" || "${TRAIN[task]:?}" == "config" ]]; then
    #     destination="${TRAIN[toolbox]:?}/vendor/rskuipers/php-assumptions/src/PhpAssumptions/Output/XmlOutput.php"
    #     cp -f "TRAIN[opt_dir]/php_assumptions/XmlOutput.php" "${destination}"
    # fi

    if [[ "${TRAIN[manage_file]:?}" != "yes" &&
        "${TRAIN[task]:?}" != "${FUNCNAME[0]}" ]]; then
        return 0
    fi

    command="php ${TRAIN[bin_dir]:?}/phpa ${TRAIN[file_path]:?} --format=xml --output=${TRAIN[file_log_path]:?}"

    if [[ "${TRAIN[file_type]:?}" != "php" ]]; then
        echo "  Skipping [${TRAIN[file_type]:?}] file"
        return 0
    fi

    set_file_scanned

    set +e
    # Run the command and capture stdout and stderr separately
    error_output=$({ ${command} 2>&1 1>&3; } 3>&1)
    exit_code=$?
    set -e

    # Check if the exit code indicates an error
    # if [ $exit_code -gt 2 ]; then
    #     echo "eo [$error_output] ec [$exit_code]"
    #     # Clean and format the error message
    #     flaw=$(clean_flaw "${error_output} Exit code $exit_code")
    #     file_flaws="[{\"file_path\":\"${TRAIN[file_path]:?}\",\"line\":0,\"column\":0,\"flaw\":\"${flaw}\"}]"

    #     # Save the error as JSON in the log file
    #     echo "${file_flaws}" > "${TRAIN[app_logs_dir]}/php_assumptions.log"
    #     return 1
    # fi

    json_parameters="{\"file_log_path\":\"${TRAIN[file_log_path]:?}\"}"

    # Parse the PHP Assumptions output
    set +e
    file_flaws=$(php "${TRAIN[opt_dir]:?}/php_assumptions/parse_php_assumptions_xml.php" "$json_parameters")
    set -e

    insert_file_flaws "${file_flaws}" "php_assumptions"
}
