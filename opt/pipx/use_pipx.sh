#!/bin/bash

# Run Pipx
function use_pipx {
    echo "${FUNCNAME[0]}():"

    # Install Pipx
    if [[ "${TRAIN[task]:?}" == "install" ]] \
        && [[ "${TRAIN[dockerized]:?}" == "no" ]]; then

        # Set environment variables for pipx
        export PIPX_HOME="/opt/pipx"
        export PIPX_BIN_DIR="/opt/pipx/bin"

        # Create directories for pipx and set ownership recursively
        mkdir -p "${PIPX_BIN_DIR}"
        chown -R "${TRAIN[mm_user]:?}" "${PIPX_HOME}"

        # Install aider and indent output by 2 spaces
        apt-get install pipx 2>&1 | while IFS= read -r line; do
            echo "  $line"
        done

        # Register Python argument completion for pipx
        eval "$(register-python-argcomplete pipx)"

        # Ensure pipx's bin directory is in the PATH
        pipx ensurepath
    fi

    # Configure Pipx

    # Run Pipx
    # Pipx is run when installing the libraries that it manages
}
