#!/bin/bash

# Run PHP-CS-Fixer to automatically check and beautify PHP files
# https://github.com/FriendsOfPHP/PHP-CS-Fixer
function run_php_cs_fixer {
    echo "${FUNCNAME[0]}():"

    # Install PHP-CS-Fixer
    if [[ "${TRAIN[task]:?}" == "install" ]] \
        && [ ! -f "${TRAIN[bin_dir]:?}/php-cs-fixer" ]; then
        cd "${TRAIN[toolbox]:?}" || return 1

        composer require --dev adamwojs/php-cs-fixer-phpdoc-force-fqcn

        composer require --dev friendsofphp/php-cs-fixer
    fi

    # Configure PHP-CS-Fixer
    # Exit code of the fix command is built using following bit flags:
    # https://github.com/PHP-CS-Fixer/PHP-CS-Fixer/blob/master/doc/usage.rst
    # https://cs.symfony.com/doc/rules/index.html
    if [[ "${TRAIN[task]:?}" == "install" ||
        "${TRAIN[task]:?}" == "config" ]]; then
        # See configurations here: https://mlocati.github.io/php-cs-fixer-configurator/#version:3.14
        cd "${TRAIN[opt_dir]}/php_cs_fixer" || return 1
        cp ".php-cs-fixer.dist.php" "${TRAIN[toolbox]:?}/.php-cs-fixer.dist.php"
    fi

    if [[ "${TRAIN[manage_file]:?}" != "yes" &&
        "${TRAIN[task]:?}" != "${FUNCNAME[0]}" ]]; then
        return 0
    fi

    if [[ "${TRAIN[file_type]:?}" != "php" ]]; then
        echo "  Skipping [${TRAIN[file_type]:?}] file"
        return 0
    fi

    export PHP_CS_FIXER_FUTURE_MODE=1

    export PHP_CS_FIXER_IGNORE_ENV=1

    command="php ${TRAIN[bin_dir]:?}/php-cs-fixer -vv --diff --format=json --config=${TRAIN[toolbox]:?}/.php-cs-fixer.dist.php "

    # In scan, --dry-run will run the fixer without making changes to the files
    if [[ "${TRAIN[task]:?}" == "scan" ||
        "${TRAIN[task]:?}" == "run_php_cs_fixer" ]]; then
        command+="--show-progress=none --dry-run fix ${TRAIN[file_path]:?}"
    fi

    if [[ "${TRAIN[task]:?}" == "fix" ]]; then
        command+="fix ${TRAIN[file_path]:?}"
    fi

    # Run PHP-CS-Fixer
    set_file_scanned

    set +e
    # { ${command} > "${TRAIN[file_log_path]:?}" 2>"$stardard_error_file"; } 3>&1 2>&1
    error=$(${command} 2>&1 > "${TRAIN[file_log_path]:?}")
    set -e

    exit_code=$?

    # 1 - General error (or PHP minimal requirement not matched).
    if [[ "${exit_code}" -eq 1 ]]; then
        file_flaws=$(jq -c '.files[] | 
            {file_path: .name, line: 0, column: 0, flaw: ("appliedFixers: " + (.appliedFixers | join(", ")))}' "${TRAIN[file_log_path]}")

        if [[ -z "${file_flaws}" ]]; then
            file_flaws='{"file_path":"'"${TRAIN[file_path]:?}"'","line":0,"column":0,"flaw":"Uncaught error; Probably invalid PHP syntax"}'
        fi
    fi

    # 4 - Some files have invalid syntax (only in dry-run mode).
    if [[ "${exit_code}" -eq 4 ]]; then
        file_flaws='{"file_path":"'"${TRAIN[file_path]:?}"'","line":0,"column":0,"flaw":"Error 4: Invalid PHP syntax "'"${error}"'""}'
    fi

    # Error #8: Some files need fixing (only in dry-run mode)
    if [[ "${exit_code}" -eq 8 ]]; then
        file_flaws='{"file_path":"'"${TRAIN[file_path]:?}"'","line":0,"column":0,"flaw":"Error 8: The file(s) need fixing"}'
    fi

    if [[ "${exit_code}" -eq 16 ]]; then
        file_flaws='{"file_path":"'"${TRAIN[file_path]:?}"'","line":0,"column":0,"flaw":"Error 16: Configuration error of the tool"}'
    fi

    if [[ "${exit_code}" -eq 32 ]]; then
        file_flaws='{"file_path":"'"${TRAIN[file_path]:?}"'","line":0,"column":0,"flaw":"Error 32: Configuration error of a Fixer"}'
    fi

    if [[ "${exit_code}" -eq 64 ]]; then
        file_flaws='{"file_path":"'"${TRAIN[file_path]:?}"'","line":0,"column":0,"flaw":"Error 64: Exception raised within the tool"}'
    fi

    if [[ -n "${file_flaws}" ]]; then
        insert_file_flaws "${file_flaws}" "php_cs_fixer"
    fi
}
