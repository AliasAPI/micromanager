#!/bin/bash

function synchronize_code {
    echo "${FUNCNAME[0]}():"

    if [[ "${TRAIN[task]:?}" == "init" ]]; then
        echo "  git init: Creating an empty Git repository or reinitialize an existing one"
        git init "$MM_DIR/$GIT_APP_PATH"
    fi

    # See; https://stackoverflow.com/questions/23391839/clone-private-git-repo-with-dockerfile

    if [[ "${TRAIN[task]:?}" == "checkout" ]]; then
        echo "  git checkout: Switching branches or restoring working tree files . . ."
        checkout_git
    fi

    if [[ "${TRAIN[task]:?}" == "pull" ]]; then
        # todo:: if file_status=approved, does the md5sum match?
        # todo:: git pull daily?
        echo "  git pull: Fetching code from remote repository and updating the local repository . . ."
        if git -C "$MM_DIR/$GIT_APP_PATH" status &> /dev/null; then
            git pull
        else
            git clone "$GIT_APP_REPOSITORY" "$MM_DIR/$GIT_APP_PATH"
            checkout_git &> /dev/null
        fi
    fi

    if [[ "${TRAIN[task]:?}" == "commit" ]]; then
        echo "  git commit: Record code changes to the local repository . . ."
        # git add -A
        # git commit
    fi

    if [[ "${TRAIN[task]:?}" == "push" ]]; then
        echo "  git push: Update remote references along with associated objects . . ."
        # git add -A
        # git commit
    fi
}
