#!/bin/bash

function checkout_git {
    echo "${FUNCNAME[0]}():"

    # Check if branch exists locally, check it out if so
    if git -C "$MM_DIR/$GIT_APP_PATH" ls-remote --heads . "$GIT_APP_BRANCH" &> /dev/null; then
        git -C "$MM_DIR/$GIT_APP_PATH" checkout "$GIT_APP_BRANCH"
    # If the branch exists on the remote create and track that branch locally
    elif git -C "$MM_DIR/$GIT_APP_PATH" ls-remote --exit-code --heads "$GIT_APP_REMOTE" "$GIT_APP_BRANCH" &> /dev/null; then
        git -C "$MM_DIR/$GIT_APP_PATH" checkout -b "$GIT_APP_BRANCH" --track "$GIT_APP_REMOTE/$GIT_APP_BRANCH"
    # If the branch doesn't exist locally or remote, create it
    else
        git -C "$MM_DIR/$GIT_APP_PATH" checkout -b "$GIT_APP_BRANCH"
    fi
}
