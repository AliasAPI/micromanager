#!/bin/bash

function run_git {
    echo "${FUNCNAME[0]}():"

    # Install Git
    if [[ "${TRAIN[task]:?}" == "install" ]] \
        && [[ "${TRAIN[dockerized]:?}" == "no" ]] \
        && ! command -v git > /dev/null 2>&1; then
        # Instruct the user to install Git on their host machine
        echo ""
        echo "  Please install Git on your host machine"
        echo ""
        exit 1
    fi

    # Configure Git
    if [[ "${TRAIN[task]:?}" == "install" ||
        "${TRAIN[task]:?}" == "config" ]] \
        && [[ "${TRAIN[dockerized]:?}" == "no" ]] \
        && command -v git > /dev/null 2>&1; then

        # Display Git settings on the TRAIN
        echo "  Application Git Repository Settings . . ."
        echo "    git_app_repository: ${TRAIN[git_app_repository]:-}"
        echo "    git_app_remote: ${TRAIN[git_app_remote]:-}"
        echo "    git_app_branch: ${TRAIN[git_app_branch]:-}"
        echo "    git_app_path: ${TRAIN[git_app_path]:-}"
        echo "    git_app_user: ${TRAIN[git_app_user]:-}"
        echo "    git_app_email: ${TRAIN[git_app_email]:-}"
        echo ""
        echo "  Port Git Repository Settings . . ."
        echo "    git_prompts_repository: ${TRAIN[git_prompts_repository]:-}"
        echo "    git_prompts_remote: ${TRAIN[git_prompts_remote]:-}"
        echo "    git_prompts_branch: ${TRAIN[git_prompts_branch]:-}"
        echo "    git_prompts_path: ${TRAIN[git_prompts_path]:-}"
        echo "    git_prompts_user: ${TRAIN[git_prompts_user]:-}"
        echo "    git_prompts_email: ${TRAIN[git_prompts_email]:-}"

        if [[ -n "${TRAIN[git_app_user]:-}" &&
            -n "${TRAIN[git_app_email]:-}" ]]; then
            echo "  Storing the git app user and email . . ."

            git config --global credential.helper store
            git config --global user.name "${TRAIN[git_app_user]}"
            git config --global user.email "${TRAIN[git_app_email]}"
        fi

        if [[ -n "${TRAIN[git_prompts_user]:-}" &&
            -n "${TRAIN[git_prompts_email]:-}" ]]; then
            echo "  Storing the git prompts user and email . . ."

            git config --global credential.helper store
            git config --global user.name "${TRAIN[git_prompts_user]}"
            git config --global user.email "${TRAIN[git_prompts_email]}"
        fi

        echo "  Global User: $(git config --global user.name)"
        echo "  Global Email: $(git config --global user.email)"

        if [[ -z "${MM_DIR:-}" ]]; then
            return 0
        fi

        # PROMPTS
        # If the port code has NOT already been cloned, clone it
        if [[ ! -d "${MM_DIR}/prompts/.git" &&
            -n "${TRAIN[git_prompts_repository]:-}" ]]; then
            echo "  Cloning ${TRAIN[git_prompts_repository]} . . ."

            # Delete any updated code in /prompts
            if [[ -d "${MM_DIR}/prompts" ]]; then
                rm -rf "${MM_DIR}/prompts"
            fi

            # Clone to the host machine file space
            git clone "${TRAIN[git_prompts_repository]}" "${MM_DIR}/prompts"
        fi

        if [[ "${TRAIN[task]:-}" != "install" &&
            "${TRAIN[task]:-}" != "config" ]] \
            || [[ "${TRAIN[dockerized]:?}" == "yes" ]]; then
            return 0
        fi

        # APP
        # If the app code has NOT already been cloned, clone it
        if [[ ! -d "${MM_DIR}/app/.git" &&
            -n "${TRAIN[git_app_repository]:-}" ]]; then
            echo "  Cloning ${TRAIN[git_app_repository]} . . ."

            # Delete any updated code in /app
            if [[ -d "${MM_DIR}/app" ]]; then
                rm -rf "${MM_DIR:-}/app"
            fi

            # Clone to the host machine file space
            git clone "${TRAIN[git_app_repository]}" "${MM_DIR}/app"
        fi

        if [[ ! -d "${MM_DIR}/app/.git" &&
            ! -d "${MM_DIR}/prompts/.git" ]]; then
            echo "Configure the git_prompts_directory in the .env file"

            return 1
        fi
    fi

    # Run git

}
