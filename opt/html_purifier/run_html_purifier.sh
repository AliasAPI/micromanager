#!/bin/bash

# Run HTML Purifier to automatically check HTML files
# https://github.com/ezyang/htmlpurifier
function run_html_purifier {
    echo "${FUNCNAME[0]}():"

    # todo:: Consider using Doxygen like HTML_Purifier: http://htmlpurifier.org/doxygen/html/

    # Installation
    # http://htmlpurifier.org/live/INSTALL

    # These optional extensions can enhance the capabilities of HTML Purifier:
    # todo:: Add iconv  : Converts text to and from non-UTF-8 encodings
    # todo:: Add bcmath : Used for unit conversion and imagecrash protection
    # todo:: Add tidy   : Used for pretty-printing HTML
    # These optional libraries can enhance the capabilities of HTML Purifier:

    # todo:: Add CSSTidy : Clean CSS stylesheets using %Core.ExtractStyleBlocks
    #         Note: You should use the modernized fork of CSSTidy available
    #         at https://github.com/Cerdic/CSSTidy
    # todo:: Net_IDNA2 (PEAR) : IRI support using %Core.EnableIDNA
    #         Note: This is not necessary for PHP 5.3 or later

    # todo:: Make sure the HTML Purifier cache directory is writable
    # HTML Purifier generates some cache files (generally one or two) to speed up
    # its execution. For maximum performance, make sure that
    # library/HTMLPurifier/DefinitionCache/Serializer is writeable by the webserver.

    # If you are in the library/ folder of HTML Purifier, you can set the
    # appropriate permissions using:
    # chmod -R 0755 HTMLPurifier/DefinitionCache/Serializer
    # If the above command doesn't work, you may need to assign write permissions
    # to group:
    #     chmod -R 0775 HTMLPurifier/DefinitionCache/Serializer

    # Install HTMLPurifier
    if [[ "${TRAIN[task]:?}" == "install" ]] \
        && [ ! -f "${TRAIN[toolbox]:?}/vendor/ezyang/htmlpurifier/library/HTMLPurifier.auto.php" ]; then
        cd "${TRAIN[toolbox]:?}" || return 1

        composer require --dev ezyang/htmlpurifier
    fi

    # Configure HTML Purifier
    # todo:: Ask r/html5 for the most strict HTML configuration
    # https://www.reddit.com/r/html5/
    # Copy the custom purify.php file to the /vendor htmlpurifier directory
    # * Am I using UTF-8?
    # * Am I using XHTML 1.0 Transitional?
    # If you answered no to any of these questions, instantiate a configuration
    # object and read on:
    #     $config = HTMLPurifier_Config::createDefault();

    # 4.3. Other settings
    # There are more configuration directives which can be read about
    # here: <http://htmlpurifier.org/live/configdoc/plain.html>  They're a bit boring,
    # but they can help out for those of you who like to exert maximum control over
    # your code.  Some of the more interesting ones are configurable at the
    # demo <http://htmlpurifier.org/demo.php> and are well worth looking into
    # for your own system.

    # For example, you can fine tune allowed elements and attributes, convert
    # relative URLs to absolute ones, and even autoparagraph input text! These
    # are, respectively, %HTML.Allowed, %URI.MakeAbsolute and %URI.Base, and
    # %AutoFormat.AutoParagraph. The %Namespace.Directive naming convention
    # translates to:

    #     $config->set('Namespace.Directive', $value);
    # The following increase security risks:
    # Allowing script, applet, embed, iframe or object elements, or certain of their attributes like allowscriptaccess
    # Allowing HTML comments (some Internet Explorer versions are vulnerable with, e.g., <!--[if gte IE 4]><script>alert("xss");</script><![endif]-->
    # Allowing dynamic CSS expressions (a feature of the IE browser)
    # Allowing the style attribute

    # Sanitize the user input:
    # https://devwerks.net/blog/16/how-not-to-use-html-purifier/

    # Configure HTML Purifier

    # Configure HTML Purifier
    # It's OK to make 3 functions from the /opt/micromanager/html_purifier/purify.php class
    # Setup html_purifier_cache directory, configure_html_purifier, and purify_html($html)
    # purify_html($html) takes in dirty html and returns clean html
    # https://stackoverflow.com/questions/6779576/how-to-pass-parameters-from-bash-to-php-script

    # Run HTML Purifier
    if [[ "${TRAIN[file_type]:?}" != "html" ]]; then
        echo "  Skipping [${TRAIN[file_type]:?}] file"
        return 0
    fi

    # todo:: Add scan Is there scan (dry run) functionality?
    if [[ "${TRAIN[task]:?}" == "scan" ]]; then
        # todo:: Run html_purifier to get the errors in file_log
        # todo:: insert_file_flaws
        return 0
    fi

    if [[ "${TRAIN[task]:?}" != "fix" &&
        "${TRAIN[task]:?}" != "use_html_purifier" ]]; then
        return 0
    fi

    # set_file_scanned

    # php "${TRAIN[bin_dir]:?}/html_purifier.php" "${TRAIN[file_path]:?}"
}
