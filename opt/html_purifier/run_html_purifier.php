<?php

declare(strict_types = 1);

// How to sanitize the user input with HTML_Purifier:
// https://devwerks.net/blog/16/how-not-to-use-html-purifier/
function run_html_purifier() : void
{
    $dirty_html = null;
    // http://htmlpurifier.org/docs

    // The GIT_APP_PATH should be configured in Micromanager
    require_once GIT_APP_PATH.'/vendor/ezyang/library/HTMLPurifier.auto.php';

    require_once '/opt/micromanager/html_purifier/configure_cache_directory.php';

    $cache_directory = APP_DATA_FILES.'/.html_purifier';

    // Make sure the cache directory exists, as the purifier won't create it for you
    if ( ! file_exists($cache_directory) && ! mkdir($cache_directory, 0o777, true) && ! is_dir($cache_directory)) {
        throw new \RuntimeException(sprintf('HTML purifier directory "%s" can not be created', $cache_directory));
    }

    // Configure HTML_Purifier
    // See https://blog.digital-craftsman.de/configure-html-purifier-cache-directory/
    $config = HTMLPurifier_Config::createDefault();

    $config->set('Cache.SerializerPath', $cache_directory);
    $allowedElements = array('a[href]', 'p[style]', 'br', 'b', 'strong', 'i', 'em', 's', 'u', 'ul', 'ol', 'li', 'span[class|data-custom-id|contenteditable]', 'table[border|cellpadding|cellspacing]', 'tbody', 'tr', 'td[valign]');
    $config->set('HTML.Allowed', implode(',', $allowedElements));
    // HTTP_HOST should be configured in /data/files/.docker.env
    $config->set('URI.Base', 'http://'.HTTP_HOST);
    $config->set('URI.MakeAbsolute', true);
    $config->set('AutoFormat.AutoParagraph', true);
    $config->set('Core.Encoding', 'UTF-8');

    $def = $config->getHTMLDefinition(true);
    $def->addAttribute('span', 'data-custom-id', 'Text');
    $def->addAttribute('span', 'contenteditable', 'Text');

    // Run HTML_Purifier
    $purifier = new HTMLPurifier($config);

    $clean_html = $purifier->purify($dirty_html);

    // todo:: Write the clean HTML file
}
