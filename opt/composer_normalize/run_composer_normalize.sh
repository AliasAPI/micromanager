#!/bin/bash

# Run Composer Normalize to lint composer.json files
# https://github.com/ergebnis/composer-normalize
# https://www.reddit.com/r/PHP/comments/ggy91u/normalizing_composerjson/
# https://www.reddit.com/user/localheinzis not normalized/
function run_composer_normalize {
    echo "${FUNCNAME[0]}():"

    # Install Composer Normalize
    if [[ "${TRAIN[task]:?}" == "install" ]] \
        && [ ! -d "${TRAIN[toolbox]:?}/vendor/ergebnis/composer-normalize" ]; then
        cd "${TRAIN[toolbox]:?}" || return 1

        composer config --no-plugins allow-plugins.ergebnis/composer-normalize true
        composer config allow-plugins.ergebnis/composer-normalize true
        composer require --dev ergebnis/composer-normalize
    fi

    if [[ "${TRAIN[manage_file]:?}" != "yes" &&
        "${TRAIN[task]:?}" != "${FUNCNAME[0]}" ]]; then
        return 0
    fi

    # Configure Composer Normalize
    if [[ "${TRAIN[task]:?}" == "fix" ]]; then
        command="composer normalize --indent-size=2 --indent-style=space "
    fi

    if [[ "${TRAIN[task]:?}" == "scan" ||
        "${TRAIN[task]:?}" == "run_composer_normalize" ]]; then
        command="composer normalize --dry-run --indent-size=2 --indent-style=space --no-update-lock "
    fi

    # Run Composer Normalize
    # Composer-normalize only processes composer.json (and, in turn, composer.lock files)
    if [[ "${TRAIN[file_name]:?}" != "composer.json" ]]; then
        echo "  Skipping [${TRAIN[file_type]:?}] file"
        return 0
    fi

    cd "${TRAIN[toolbox]:?}" || return 1

    set_file_scanned

    error=$(${command} "${TRAIN[file_path]:?}" 2>&1 1> "${TRAIN[file_log_path]:?}")

    exit_code=$?

    if [[ "${exit_code}" -gt 0 ]]; then
        file_flaws=$(jq -n --arg file_path "${TRAIN[file_path]:?}" --arg error "${error}" '{file_path: $file_path, line: 0, column: 0, flaw: $error}' | jq -c '.')

        insert_file_flaws "${file_flaws}" "composer_normalize"
    fi
}
