#!/bin/bash

function clean_rector {
    echo "${FUNCNAME[0]}():"

    rector_file="rector_stuff.php"
    rector_directory="/home/drew/micromanager/tools/vendor/rector"

    # Loop through the rector.php file
    while IFS= read -r line; do
        # if the line begins with "use "
        if [ "${line:0:4}" = "use " ]; then
            # Set the line as a variable called "class_path"
            # Remove the "use " from the beginning of $class_path
            class_path=${line#"use "}

            # Remove the ";" from the end of $class_path
            class_path=${class_path%;}

            # echo "${class_path}"

            # Escape backslashes in class_path
            escaped_class_path=$(sed 's/\\/\\\\/g' <<< "$class_path")

            # Get the class name from the path
            class=$(echo "$class_path" | awk -F'\\' '{print $NF}')

            # Check if the string does NOT exist (grep -r "$class" /tools/vendor/rector)
            if ! grep -rq "$escaped_class_path" "$rector_directory" &> /dev/null; then
                echo "  Commenting lines containing [${class}]"
                # Add "# " to the beginning of each line in rector.php that contains the $class string
                sed -i "/$class/ s/^/# /" "$rector_file"

                # Remove extra line commenters
                sed -i "/^# # / s/# //" "$rector_file"
            fi
        fi
    done < "$rector_file"
}
