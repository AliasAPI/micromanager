#!/bin/bash

# Run Rector to automatically refactor and review code
# https://github.com/rectorphp/rector
# Note: Run Rector before linting software to restore formatting
function run_rector {
    echo "${FUNCNAME[0]}():"

    # Install Rector
    if [[ "${TRAIN[task]:?}" == "install" ]] \
        && [ ! -f "${TRAIN[bin_dir]:?}/rector" ]; then
        cd "${TRAIN[toolbox]:?}" || return 1

        composer require --dev rector/rector
    fi

    # todo:: Remove this delete
    if [[ "${TRAIN[task]:?}" == "reset" ]]; then
        rm -f /tools/rector.php
    fi

    # Configure Rector
    # https://raw.githubusercontent.com/rectorphp/rector/main/docs/rector_rules_overview.md
    # todo:: Use "vendor/bin/rector init" on the first installation run
    # Example: https://gitee.com/mirrors/CodeIgniter4/blob/develop/rector.php
    if [[ "${TRAIN[task]:?}" == "install" ||
        "${TRAIN[task]:?}" == "config" ||
        "${TRAIN[task]:?}" == "reset" ]]; then
        cd "${TRAIN[toolbox]:?}" || return 1

        if [ ! -f "${TRAIN[opt_dir]}/rector/rector.php" ]; then
            echo "  The rector.php configuration file is missing"
        fi

        cp "${TRAIN[opt_dir]}/rector/rector.php" "${TRAIN[toolbox]:?}/rector.php"
    fi

    if [[ "${TRAIN[manage_file]:?}" != "yes" &&
        "${TRAIN[task]:?}" != "${FUNCNAME[0]}" ]]; then
        return 0
    fi

    RECTOR_CACHE_DIRECTORY="${TRAIN[app_data_files]:?}/.rector"

    # Set the cache directory used in the rector.php configuration file
    export RECTOR_CACHE_DIRECTORY

    # https://getrector.com/documentation/define-paths
    # https://getrector.com/documentation/config-configuration
    command="php ${TRAIN[bin_dir]:?}/rector --config=${TRAIN[toolbox]:?}/rector.php --output-format=json "

    if [[ "${TRAIN[task]:?}" == "scan" ||
        "${TRAIN[task]:?}" == "run_rector" ]]; then
        command+="--dry-run "
    fi

    command+="process ${TRAIN[file_path]:?} "

    # Run Rector
    if [[ "${TRAIN[file_type]:?}" != "php" ]]; then
        echo "  Skipping [${TRAIN[file_type]:?}] file"
        return 0
    fi

    set_file_scanned

    cd "${TRAIN[toolbox]:?}/" || return 1

    set +e
    error=$(${command} 2>&1 > "${TRAIN[file_log_path]:?}")
    exit_code=$?
    set -e

    # Process the JSON for files that are parsed without any problems
    if [[ "${exit_code}" -eq 0 ]]; then
        # Convert Rector's diff output into commands
        file_flaws=$(jq -c '.file_diffs[] 
        | {file_path: (if .file | startswith("..") then 
        .file[2:] else .file end), line: 0, column: 0, flaw: (.diff | split("\n") | map(
            if startswith("---") or startswith("+++") or startswith("@@") then empty
            elif startswith("-") then "Remove " + .[1:]
            elif startswith("+") then "Add " + .[1:]
            else .
        end
        ) | join("\n"))}' "${TRAIN[file_log_path]}")
    fi

    # IF the file is worthless, delete it
    # Rector adds files that are
    # {
    #     "totals": {
    #         "changed_files": 0,
    #         "errors": 0
    #     }
    # }

    if [[ "${exit_code}" -eq 1 ]]; then
        # Consider running the entire command again with command+=" --debug"
        # Example: {"fatal_errors":["syntax error, unexpected identifier \"out\", expecting \"]\""]}
        file_flaws=$(jq -c --arg file_path "${TRAIN[file_path]:?}" \
            '.fatal_errors[] | {file_path: $file_path, line:0, column:0, flaw: .}' "${TRAIN[file_log_path]}")
    fi

    if [[ "${exit_code}" -ge 3 ]]; then
        # Consider running the entire command again with command+=" --debug"
        flaw=$(clean_flaw "${error} Exit code ${exit_code}")
        file_flaws="[{\"file_path\":\"${TRAIN[file_path]:?}\",\"line\":0,\"column\":0,\"flaw\":\"${flaw}\"}]"
    fi

    # echo $file_flaws; exit 1
    insert_file_flaws "${file_flaws}" "rector"
}
