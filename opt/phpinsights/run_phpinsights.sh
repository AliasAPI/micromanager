#!/bin/bash

# Run PHP Insights to simplify the analysis of the applicatino code
# https://github.com/nunomaduro/phpinsights
function run_phpinsights {
    echo "${FUNCNAME[0]}():"

    # Install PhpInsights
    if [[ "${TRAIN[task]:?}" == "install" ]] \
        && [ ! -f "${TRAIN[toolbox]:?}/phpinsights" ]; then
        cd "${TRAIN[toolbox]:?}" || return 1

        composer require --dev nunomaduro/phpinsights
    fi

    # Configure PhpInsights
    # todo:: Add the most strict configuration file in etc/phpinsights/run_phpinsights.sh
    # todo:: Copy the configuration file to the correct location
    # if [[ "${TRAIN[task]:?}" == "install" || "${TRAIN[task]:?}" == "config" ]]; then
    # fi

    log_file="${TRAIN[app_logs_dir]:?}/phpinsights_output.json"

    # Run PhpInsights
    if [[ "${TRAIN[task]:?}" != "report" &&
        "${TRAIN[task]:?}" != "phpinsights" ]]; then
        return 0
    fi

    # todo:: Test run_phpinsights.sh
    cd "${TRAIN[app_logs_dir]:?}" || return 1

    php "${TRAIN[bin_dir]:?}/phpinsights analyse --format=json < /dev/null >> ${log_file}" || true
}
