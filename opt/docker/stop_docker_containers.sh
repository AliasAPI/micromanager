#!/bin/bash

function stop_docker_containers {
    echo "${FUNCNAME[0]}():"

    # If NOT reconfiguring and restarting the docker containers, simply return
    if [[ "${TRAIN[task]:?}" != "delete"
        && "${TRAIN[task]:?}" != "restart" 
        && "${TRAIN[task]:?}" != "stop" ]]; then
        return 0
    fi

    if ! docker info > /dev/null 2>&1; then
        return 0
    fi

    set -e

    echo "  Disconnecting Docker containers for this instance from the [${MM_NETWORK:?}] network . . ."

    if [[ -z "${TRAIN[docker_containers]:-}" ]]; then
        echo "  Define the custom TRAIN[docker_containers]"
        exit 1
    fi

    for container in ${TRAIN[docker_containers]}; do
        container_exists=false

        # Check to see if the container exists to assist the docker inspect below
        if [ "$(docker ps -aqf name="$container")" ] \
            && [ "$(docker ps -qf status=running -f name="$container")" ]; then

            container_exists=true
        fi

        # If the container exists, disconnect it from the Micromanager network
        if [ "$container_exists" == true ] \
            && docker inspect "$container" -f '{{range $k,$v := .NetworkSettings.Networks}}{{$k}}{{end}}' | grep -q "${MM_NETWORK}"; then
            echo "  Disconnecting the ${container} container from the ${MM_NETWORK} network"

            docker network disconnect "${MM_NETWORK}" "$container" > /dev/null
        fi

        if [ "$container_exists" == true ]; then
            echo "Stopping the $container container . . ."

            docker container stop "$container" > /dev/null
        fi
    done

    echo ""
}
