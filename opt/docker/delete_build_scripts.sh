#!/bin/bash

function delete_build_scripts {
    echo "${FUNCNAME[0]}():"

    if [[ "${TRAIN[task]}" != "install"
       || "${TRAIN[dockerized]}" != "no" ]]; then
        return 0
    fi

    echo "  Deleting the build script directories . . ."

    build_scripts_directories="${MM_DIR}/opt/docker/*/build_scripts"

    # Delete the build_scripts directories
    for build_scripts_directory in $build_scripts_directories; do
        if [[ -d "$build_scripts_directory" ]]; then
            echo "  Deleting $build_scripts_directory"
            rm -rf "$build_scripts_directory"
        fi
    done

    # Unset TRAIN variables
    unset 'TRAIN[build_scripts]'
    unset 'TRAIN[docker_context_directory]'

    echo "  Deleting the Docker .bashrc . . ."

    if [[ ! -d "${MM_DIR}/opt/docker" ]]; then
        echo "  The ${MM_DIR}/opt/docker directory does not exist"
        return 1
    fi

    find "${MM_DIR}/opt/docker/" -mindepth 2 -maxdepth 2 -name .bashrc -delete | while IFS= read -r line; do
        echo "    $line"
    done

    echo "  Deleting the associated custom Dockerfiles . . ."

    find "${MM_DIR}/opt/docker/" -mindepth 2 -maxdepth 2 -name Dockerfile -delete | while IFS= read -r line; do
        echo "    $line"
    done
}
