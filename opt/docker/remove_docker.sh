#!/bin/bash

function remove_docker {
    echo "${FUNCNAME[0]}():"

    if [[ "${TRAIN[task]:?}" != "remove" ]]; then
        return 0
    fi

    # Stop Docker
    sudo service docker stop

    if confirm_yes_or_no "  Remove Docker from your host machine?"; then
        packages=(
            "apparmor"
            "cgroupfs-mount"
            "containerd"
            "docker"
            "docker.io"
            "docker-compose"
            "needrestart"
            "runc")

        for p in "${packages[@]}"; do
            if command -v "${p}"; then
                echo "  Removing ${p} . . ."

                sudo apt -y remove "$p"
            fi
        done

        sudo apt -y autoremove
        sudo apt -y clean
        sudo rm -rf /var/lib/docker
    fi
}
