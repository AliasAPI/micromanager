#!/bin/bash

function delete_docker_base_images {
    echo "${FUNCNAME[0]}():"

    if [[ "${TRAIN[task]:?}" != "delete" 
        && "${TRAIN[task]:?}" != "stop" ]]; then
        return 0
    fi

    if [[ -z "${TRAIN[docker_images]:-}" ]]; then
        echo "  Define the custom TRAIN[docker_images]"
        return 1
    fi

    if confirm_yes_or_no "  Permanently delete the base Docker images and network?"; then
        # Delete the "MicroManager" network
        if docker network inspect MicroManager &>/dev/null; then
            echo "  Deleting the 'MicroManager' Docker network . . ."

            docker network rm MicroManager | while IFS= read -r line; do
                echo "    $line"
            done
        fi

        #Delete the base images that were downloaded . . ."
        for image in ${TRAIN[docker_images]}; do
            if docker image inspect "$image" > /dev/null 2>&1; then
                # Identify the parent image (base image) of the custom MicroManager image
                base_image=$(docker inspect --format "{{.Parent}}" "$image" 2>/dev/null)

                # If the base image exists and it is unused, delete it
                if [[ -n "$base_image" ]] &&
                    ! docker ps -a --filter "ancestor=$base_image" --format "{{.ID}}" | grep -q .; then
                    
                    echo "    Deleting image: $base_image"
                    docker rmi "$base_image" --force

                    if [[ $? -ne 0 ]]; then
                        echo "  Failed to delete base image: $base_image"
                        return 1
                    fi
                fi
            fi
        done
    fi

    echo ""
}
