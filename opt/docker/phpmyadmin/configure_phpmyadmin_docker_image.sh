#!/bin/bash

function configure_phpmyadmin_docker_image {
    echo "${FUNCNAME[0]}():"

    # Configure the PHPMyAdmin image if it is not set
    if [ -z "${PHPMYADMIN_IMAGE:-}" ]; then
        export PHPMYADMIN_IMAGE="bitnami/phpmyadmin:latest"
    fi

    echo "  PHPMyAdmin image: ${PHPMYADMIN_IMAGE}"

    local container_name="phpmyadmin${TRAIN[mm_id]:?}"

    set_train "docker_images" "${TRAIN[docker_images]:?} $PHPMYADMIN_IMAGE"

    set_train "docker_containers" "${TRAIN[docker_containers]:?} ${container_name}"

    if [[ "${TRAIN[task]:-}" != 'install' &&
          "${TRAIN[task]:-}" != 'config' ]]; then
        return 0
    fi

    local docker_run_command="docker run \
        --name \"${container_name}\" \
        --hostname myadmin \
        --detach \
        --user \"${UID}\" \
        --restart always \
        --env \"PMA_ARBITRARY=1\" \
        --env \"PMA_HOST=mariadb${MM_ID}\" \
        --volume \"$MM_DIR/app:$GIT_APP_PATH\" \
        --volume \"$MM_DIR/prompts:$GIT_PROMPTS_PATH\" \
        --volume \"$MM_DIR/data:/data\" \
        --volume \"$MM_DIR/opt/nginx:/opt/bitnami/nginx/conf/server_blocks\" \
        --network \"${MM_NETWORK}\" \
        --publish \"127.0.0.${MM_ID}:8080:8080\" \
        --publish \"127.0.0.${MM_ID}:8443:8443\" \
        --add-host \"host.docker.internal:host-gateway\" \
        \"${PHPMYADMIN_IMAGE}\""

    set_train "spec_run_${container_name}" "${docker_run_command}"

    set_train "spec_deps_${container_name}" "mariadb${TRAIN[mm_id]:?}"
}
