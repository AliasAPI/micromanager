#!/bin/bash

function configure_docker {
    echo "${FUNCNAME[0]}():"

    if [[ -z "${MM_ID}" ]]; then
        echo "  Define the MicroManager MM_ID in .env"
        exit 1
    fi

    echo "  Docker Configurations . . ."

    # Define the MicroManager network
    export MM_NETWORK="MicroManager"
    echo "    MicroManager Network: ${MM_NETWORK}"

    if [[ "${TRAIN[task]:?}" != "install" &&
        "${TRAIN[task]:?}" != "config" ]]; then
        return 0
    fi
    
    # If the docker group does not exist, create it
    if ! getent group docker > /dev/null; then
        sudo groupadd --system docker
        echo "  Docker group created"
    fi

    sudo usermod -a -G docker "$USER"
}
