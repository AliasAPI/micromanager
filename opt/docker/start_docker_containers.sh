#!/bin/bash

function __start_docker_containers {
    local container
    for container in "${@}"; do
        echo "----- Processing: ${container} -----"
        # check if container is not running
        if ! docker ps --filter "name=$container" --filter "status=running" -q | grep -q .; then
            echo "  Container $container is not running, need to start it"
            container_requires="${TRAIN[${container}_spec_deps]:-}"
            if [ -n "${container_requires}" ]; then
                echo "  Container $container depends on ${container_requires}"
                __start_docker_containers ${container_requires}
            fi

            do_create="true"
            
            # check if the not running container exists
            if [ "$(docker ps -a -q -f name=${container})" ]; then
                # if container exited start it and do not create
                if [ "$(docker ps -aq -f status=exited -f name=${container})" ]; then
                    do_create="false"
                    echo -n "  Started "
                    docker start "${container}"
                # otherwise remove it
                else
                    docker rm "${container}"
                fi
            fi

            if [ "${do_create}" == "true" ]; then
                echo -n "  Created "
                eval ${TRAIN["spec_run_${container}"]}
            fi

            # wait for it to be running
            until [ "$(docker inspect -f {{.State.Running}} ${container})"=="true" ]; do
                sleep 0.2;
            done;
        else
            echo "  Container $container is already running, no need to start it"
        fi
    done
}

function start_docker_containers {
    echo "${FUNCNAME[0]}():"

    if [[ "${TRAIN[task]:?}" != "install" &&
        "${TRAIN[task]:?}" != "restart" ]]; then
        return 0
    fi

    if [ -z "${TRAIN[docker_containers]:-}" ]; then
        echo "  Please configure the TRAIN[docker_containers]"
        return 1
    fi

    # Create and join the Docker network
    if ! docker network inspect "${MM_NETWORK}" &> /dev/null; then
        echo "  Creating and joining the [${MM_NETWORK}] network . . ."
        result=$(docker network create --driver=bridge "${MM_NETWORK}")
        echo "  $result"
    fi

    all_docker_containers_running=true

    # Check if every container is running
    for container in ${TRAIN[docker_containers]}; do
        if ! docker ps --filter "name=$container" --filter "status=running" -q | grep -q .; then
            all_docker_containers_running=false
            break
        fi
    done

    # If the containers are running, leave this function
    if [ "$all_docker_containers_running" = false ]; then
        echo "  Starting the Docker containers . . ."
        __start_docker_containers ${TRAIN[docker_containers]}
    fi

    echo "  All of the Docker containers are running."
}
