#!/bin/bash

function configure_php_fpm_docker_image {
    echo "${FUNCNAME[0]}():"

    # Configure the PHP-FPM image if it is not set
    if [[ -z "${PHP_FPM_IMAGE:-}" ]]; then
        export PHP_FPM_IMAGE="mm/php-fpm:latest"
    fi

    if [[ -z "${PHP_FPM_SOURCE_IMAGE:-}" ]]; then
        export PHP_FPM_SOURCE_IMAGE="bitnami/php-fpm:latest"
    fi

    echo "  PHP-FPM image: ${PHP_FPM_IMAGE}"

    local container_name="php${TRAIN[mm_id]:?}"

    set_train "build_image" "${PHP_FPM_IMAGE}"

    set_train "docker_images" "${TRAIN[docker_images]:-} $PHP_FPM_IMAGE $PHP_FPM_SOURCE_IMAGE"

    set_train "docker_containers" "${TRAIN[docker_containers]:-} ${container_name}"

    if [[ "${TRAIN[task]:-}" != 'install' &&
          "${TRAIN[task]:-}" != 'config' ]]; then
        return 0
    fi

    # Set the Dockerfile.template (that is used to create the customized Dockerfile)
    set_train "dockerfile_template_path" "${MM_DIR}/opt/docker/php-fpm/Dockerfile.template"

    set_train "build_scripts_path" "${MM_DIR}/opt/docker/php-fpm/build_scripts"

    local build_scripts="
        ${MM_DIR}/opt/apt_get/run_apt_get.sh
        ${MM_DIR}/opt/jq/run_jq.sh
        ${MM_DIR}/opt/debug/run_command.sh
        ${MM_DIR}/opt/debug/log_message.sh
        ${MM_DIR}/opt/traintrack/set_train.sh
        ${MM_DIR}/opt/traintrack/load_train.sh
        ${MM_DIR}/opt/docker/set_dockerized.sh
        ${MM_DIR}/opt/user/.bashrc
        ${MM_DIR}/${APP_DATA_FILES}/.train.json
        ${MM_DIR}/opt/pecl/run_pecl.sh
        ${MM_DIR}/opt/ast/use_ast.sh
        ${MM_DIR}/opt/docker/finish_docker_build.sh"

    set_train "build_scripts" "$build_scripts"

    local docker_run_command="docker run \
        --name \"${container_name}\" \
        --hostname php \
        --detach \
        --user \"${UID}\" \
        --restart always \
        --env \"APP_DATA_FILES=${APP_DATA_FILES}\" \
        --volume \"$MM_DIR/app:$GIT_APP_PATH\" \
        --volume \"$MM_DIR/prompts:$GIT_PROMPTS_PATH\" \
        --volume \"$MM_DIR/data:/data\" \
        --volume \"$MM_DIR/opt:/opt/micromanager\" \
        --volume \"$MM_DIR/opt/php/php.ini:/opt/bitnami/php/etc/conf.d/php.ini\" \
        --volume \"$MM_DIR/opt/opcache/opcache.ini:/opt/bitnami/php/etc/conf.d/opcache.ini\" \
        --volume \"$MM_DIR/opt/xdebug/xdebug.ini:/opt/bitnami/php/etc/conf.d/xdebug.ini\" \
        --network \"${MM_NETWORK}\" \
        --add-host \"host.docker.internal:host-gateway\" \
        \"${PHP_FPM_IMAGE}\""

    set_train "spec_run_${container_name}" "${docker_run_command}"

    set_train "spec_deps_${container_name}" "mariadb${TRAIN[mm_id]:?}"
}
