#!/bin/bash

function list_docker_containers {
    echo "${FUNCNAME[0]}():"

    if [[ "${TRAIN[task]:?}" != "install" &&
        "${TRAIN[task]:?}" != "restart" &&
        "${TRAIN[task]:?}" != "list" ]]; then
        return 0
    fi

    # SC2016: Expressions don't expand in single quotes, use double quotes for that.
    # We don't want the expressions to expand, that's why we're using single quotes. Ignore this check
    # shellcheck disable=SC2016
    docker ps -a --format '{{ .ID }}' | xargs -I {} docker inspect -f '{{ .Name }}{{ printf "\n  ID: " }}{{ .Id }}{{ printf "\n  Ports:" }}{{ range $p, $conf := .NetworkSettings.Ports }}{{ printf "\n    " }}{{ if $conf }}{{(index $conf 0).HostIp}}:{{(index $conf 0).HostPort}} -> {{ end }}{{$p}}{{end}}{{ printf "\n  Networks:" }}{{ range $n, $conf := .NetworkSettings.Networks }}{{ printf "\n    " }}{{$n}}:{{ printf "\n      IPAddresses: " }}{{ $conf.IPAddress }}{{ printf "\n      Aliases:" }}{{ range $conf.Aliases }} {{ . }}{{end}}{{end}}{{ printf "\n  Mounts: " }}{{ range .Mounts }}{{ printf "\n\t" }}{{ .Type }} {{ if eq .Type "bind" }}{{ .Source }}{{ else }}{{ .Name }}{{ end }} => {{ .Destination }}{{ end }}' {}
}
