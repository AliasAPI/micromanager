#!/bin/bash

# Set up the system for MicroManager
function install_docker {
    echo "${FUNCNAME[0]}():"

    if [[ "${TRAIN[task]:?}" != "install" ]]; then
        return 0
    fi

    # Make sure required packages are installed
    if ! command -v apt-get &> /dev/null; then
        echo ""
        echo "  Please use an operating system that has apt-get"
        exit 1
    fi

    # If Docker is already installed, leave this function
    if command -v docker > /dev/null 2>&1 \
        && command -v docker-compose > /dev/null 2>&1; then
        echo "  Docker and Docker Compose are already installed"
        return 0
    fi

    echo ""

    packages=(
        "debian-archive-keyring"
        "apparmor"
        "apt-utils"
        "cgroupfs-mount"
        "containerd"
        "docker"
        "docker.io"
        "docker-compose"
        "needrestart"
        "runc")

    for package in "${packages[@]}"; do
        if ! command -v "${package}" &> /dev/null \
            || ! dpkg -l "${package}" &> /dev/null; then
            echo "  Installing ${package} . . ."

            sudo apt-get -y install "$package" | while IFS= read -r line; do
                echo "    $line"
            done
        fi
    done

    # Start Docker
    sudo service docker start | while IFS= read -r line; do
        echo "    $line"
    done

    # Grant permissions to the user
    sudo chmod 666 /var/run/docker.sock

    # Check to make sure Docker is installed on the developer's system
    if ! command -v docker > /dev/null 2>&1; then
        echo ""
        echo "  Please install Docker"
        echo ""
        exit 1
    fi

    echo ""
    echo "  Docker version $(docker version --format '{{.Server.Version}}')"

    # Check to make sure docker-compose is installed on the developer's system
    if ! command -v docker-compose > /dev/null 2>&1; then
        echo ""
        echo "  Please install Docker Compose"
        echo ""
        exit 1
    fi

    echo "  Docker Compose version $(docker-compose version --short)"
}
