#!/bin/bash

function build_docker_image {
    echo "${FUNCNAME[0]}():"

    if [[ "${TRAIN[task]:?}" != "install"
       || "${TRAIN[dockerized]:?}" != "no" ]]; then
        return 0
    fi

    if [ -z "${TRAIN[dockerfile_template_path]:-}" ]; then
        echo "  Error: Set TRAIN[dockerfile_template_path]"
        return 1
    fi

    # Set the custom dockerfile_path from the Dockerfile.template 
    local dockerfile_path="${TRAIN[dockerfile_template_path]%.template}"
    local image_name="${TRAIN[build_image]}"
    local build_context="$(dirname ${dockerfile_path})"


    #if image exists return
    if docker image inspect "${image_name}" >/dev/null 2>&1; then
        echo "  Image ${image_name} exists, no need to build"
        return 0
    fi
    
    docker build --tag "${image_name}" "${build_context}"

}
