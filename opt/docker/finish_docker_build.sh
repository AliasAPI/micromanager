#!/bin/bash

function finish_docker_build {
    echo "${FUNCNAME[0]}():"

    if [[ "${TRAIN[dockerized]:?}" == "yes" ]]; then
        return 0
    fi

    # Cleanup temporary files and package caches
    echo "  Cleaning up temporary files and package caches . . ."
    rm -rf /tmp/* /var/lib/apt/lists/* /var/cache/apt/archives/*

    # Create a flag file to indicate successful Docker build
    local flag_file="/var/lib/dockerized"

    touch "$flag_file"

    echo "Docker build finished successfully."
}
