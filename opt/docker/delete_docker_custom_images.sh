#!/bin/bash

function delete_docker_custom_images {
    echo "${FUNCNAME[0]}():"

    # If not deleting (during development), just return
    if [[ "${TRAIN[task]:?}" != "delete" ]] || 
        ! command -v docker &>/dev/null; then
        return 0
    fi

    if confirm_yes_or_no "  Permanently delete the CUSTOM docker images?"; then
        # Loop through the Docker images to handle their associated volumes
        for image in ${TRAIN[docker_images]}; do
            containers=$(docker ps -a --filter "ancestor=$image" --format "{{.ID}}")

            if [[ -n "$containers" ]]; then
                echo "  Inspecting containers to find associated volumes . . ."

                for container in $containers; do
                    volumes=$(docker inspect "$container" --format "{{ range .Mounts }}{{ .Name }} {{ end }}" 2>/dev/null)

                    if [[ -n "$volumes" ]]; then
                        echo "  Found associated volumes for container $container: $volumes"

                        for volume in $volumes; do
                            docker volume rm "$volume" --force | while IFS= read -r line; do
                                echo "    $line"
                            done

                            if [[ $? -ne 0 ]]; then
                                echo "    Failed to delete volume: $volume"
                            fi
                        done
                    fi
                done

                echo "  Deleting Docker containers found for image $image . . ."

                for container in $containers; do
                    # Stop and remove any containers created from this specific image
                    docker container rm -f -v "$container" | while IFS= read -r line; do
                        echo "    $line"
                    done

                    if [[ $? -ne 0 ]]; then
                        echo "    Failed to remove container: $container"
                    fi
                done
            fi

            # Ensure image exists before trying to delete
            if docker image inspect "$image" > /dev/null 2>&1; then
                echo "  Deleting Docker image: $image . . ."
             
                docker rmi "$image" --force | while IFS= read -r line; do
                    echo "    $line"
                done

                if [[ $? -ne 0 ]]; then
                    echo "  Failed to delete Docker image: $image"
                    return 1
                fi
            fi
        done
    fi

    echo ""
}
