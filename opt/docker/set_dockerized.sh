#!/bin/bash

# Set dockerenv to yes when inside the docker (container) environment
function set_dockerized {
    echo "${FUNCNAME[0]}():"

    set_train "dockerized" "no"

    # todo:: Elrod, comment the following 3 lines
    if [[ -f "/var/lib/dockerized" ]]; then
        set_train "dockerized" "yes"
    fi

    echo "  train[dockerized] is set to ${TRAIN[dockerized]}"
}
