#!/bin/bash

function create_dockerfiles {
    echo "${FUNCNAME[0]}():"

    if [[ "${TRAIN[task]:?}" != "install" 
        && "${TRAIN[task]:?}" != "config" 
        && "${TRAIN[task]:?}" != "restart" ]]; then
        return 0
    fi

    if [ -z "${TRAIN[docker_images]:-}" ]; then
        echo "  Please configure the train[docker_images]"
        exit 1
    fi

    # If the user deleted the images OR they have not been created, run docker-compose --build
    local create_docker_images=false

    for image in ${TRAIN[docker_images]}; do
        # Remove characters preceding a slash and the slash itself
        image_name=$(echo "$image" | sed 's|.*/||')

        echo "  Checking to make sure the $image_name image exists"

        if ! docker images --format "{{.Repository}}:{{.Tag}}" | grep -q "$image_name"; then
            echo "  The [$image] Docker image does not exist yet"
            local create_docker_images=true

            break
        fi
    done

    for image in ${TRAIN[docker_images]}; do
        # Remove characters preceding a slash and the slash itself
        image_name=$(echo "$image" | sed 's|.*/||')

        echo "  Checking to make sure the $image_name image exists"

        if ! docker images --format "{{.Repository}}:{{.Tag}}" | grep -q "$image_name"; then
            echo "  The [$image] Docker image does not exist yet"
            local create_docker_images=true

            break
        fi
    done
}
