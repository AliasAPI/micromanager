#!/bin/bash

function create_custom_dockerfile {
    echo "${FUNCNAME[0]}():"

    if [[ "${TRAIN[task]:?}" != "install"
       || "${TRAIN[dockerized]:?}" != "no" ]]; then
        return 0
    fi

    if [ -z "${TRAIN[dockerfile_template_path]:-}" ]; then
        echo "  Error: Set TRAIN[dockerfile_template_path]"
        return 1
    fi

    if [ -z "${TRAIN[build_scripts]:-}" ]; then
        echo "  Error: Set TRAIN[build_scripts]"
        return 1
    fi

    if [ -z "${TRAIN[build_scripts_path]:-}" ]; then
        echo "  Error: Set TRAIN[build_scripts_path]"
        return 1
    fi

    if [ ! -f "${TRAIN[dockerfile_template_path]:-}" ]; then
        echo "  Error: Create the ${TRAIN[dockerfile_template_path]} file"
        return 1
    fi

    # Set the custom dockerfile_path from the Dockerfile.template 
    dockerfile_path="${TRAIN[dockerfile_template_path]%.template}"
    build_scripts_path="${TRAIN[build_scripts_path]}"

    # echo "  dockerfile_path: $dockerfile_path"
    # echo "  train[build_scripts]: ${TRAIN[build_scripts]}"

    if [ ! -f "${dockerfile_path}" ]; then
        echo "  Creating the Dockerfile . . ."
        touch "${dockerfile_path}"

        # Replace the environment variable placeholders with the right values 
        envsubst < "${TRAIN[dockerfile_template_path]}" > "${dockerfile_path}"
    fi

    # Count the number of non-empty scripts (separated by new lines)
    last=$(echo "${TRAIN[build_scripts]}" | grep -v '^$' | wc -l)

    # Iterate through the scripts in reverse order
    for ((i = last; i > 0; i--)); do
        # echo " " 
        # Get the i-th non-empty script (separated by new lines)
        script=$(echo "${TRAIN[build_scripts]}" | grep -v '^$' | sed -n "${i}p")

        # Trim the value for $script
        script=$(echo "$script" | sed 's/^[[:space:]]*//;s/[[:space:]]*$//')

        # echo "  script $script"

        # Verify the source file exists
        if [[ ! -f "${script}" ]]; then
            echo "  Make ${script} exist"
            exit 1
        fi

        # Extract the parent directory and filename
        parent_directory_and_filename="${script#${MM_DIR}/opt/}"
        # echo "  parent_directory_and_filename $parent_directory_and_filename"

        parent_directory=$(basename "$(dirname "${parent_directory_and_filename}")")

        # echo "  parent_directory $parent_directory"

        filename=$(basename "${parent_directory_and_filename}")

        # echo "  filename $filename"

        # Create the parent directory in the build_scripts_path
        if [[ ! -d "${TRAIN[build_scripts_path]}/${parent_directory}" ]]; then
            echo "  Creating ${parent_directory}/ in ${TRAIN[build_scripts_path]}"

            mkdir -p "${TRAIN[build_scripts_path]}/${parent_directory}"
        fi

        # Shorten the script_link path
        build_link="${TRAIN[build_scripts_path]}/${parent_directory}/${filename}"

        relative_link="build_scripts/${parent_directory}/${filename}"

        if [[ ! -e "${build_link}" || "${script}" -nt "${build_link}" ]]; then
            cp -a "${script}" "${build_link}"
        fi


        # Rewrite COPY COMMAND
        copy_command="COPY ${relative_link} /tmp/${parent_directory}/${filename}"

        # echo "  copy_command $copy_command"

        # echo "  dockerfile_path $dockerfile_path"

        if ! grep "${copy_command}" "$dockerfile_path" &> /dev/null; then
            sed -i '/# COPY COMMANDS/a '"${copy_command//\//\\\/}"'' "$dockerfile_path"
        fi

        if [[ "${filename}" == *".sh" ]]; then
            # Rewrite SOURCE COMMANDS
            source_command="\ \ \ \ source /tmp/${parent_directory}/${filename} && \\\\"

            if ! grep "${source_command}" "$dockerfile_path" &> /dev/null; then
                sed -i '/RUN echo SOURCE COMMANDS && \\/a '"${source_command//\//\\\/}"'' "$dockerfile_path"
            fi

            # Rewrite CALL FUNCTIONS
            if [[ "${filename}" != "set_train.sh" ]]; then
                run_command="\ \ \ \ ${filename%%.sh} && \\\\"
         
                if ! grep -E "^ *${filename%%.sh}( &&)?.*" "$dockerfile_path" &> /dev/null; then
                    sed -i '/echo CALL FUNCTIONS/a '"${run_command}"'' "$dockerfile_path"
                fi
            fi
        fi

        # Rewrite BASHRC COPY
        bashrc_copy_command="cat /tmp/user/.bashrc >> /etc/bash.bashrc"

        if [[ "${filename}" == ".bashrc" ]] && ! grep -qE "$bashrc_copy_command" "$dockerfile_path"; then
            awk -v cmd="$bashrc_copy_command" '/echo BASHRC COPY/ { print $0 " && \\"; print "    " cmd; next } { print }' "$dockerfile_path" > tmpfile && mv tmpfile "$dockerfile_path"
        fi
    done

    if [[ -f "$dockerfile_path" ]] \
        && ! grep -q "echo CALL FUNCTIONS && \\\\" "$dockerfile_path"; then
        echo "  Writing the Docker commands into $dockerfile_path . . ."

        sed -i 's/echo CALL FUNCTIONS/echo CALL FUNCTIONS \&\& \\/g' "$dockerfile_path"
    fi

    # Remove the trailing "&& \" from the CALL FUNCTIONS section
    sed -i 's/finish_docker_build && \\/finish_docker_build/g' "$dockerfile_path"
}
