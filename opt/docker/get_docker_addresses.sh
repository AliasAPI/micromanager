#!/bin/bash

function get_docker_addresses {
    echo "${FUNCNAME[0]}():"

    if [[ "${TRAIN[task]:?}" != "install" &&
        "${TRAIN[task]:?}" != "restart" &&
        "${TRAIN[task]:?}" != "list" ]]; then
        return 0
    fi

    # If the custom Docker nginx container is not present and running, do not create files lists
    if ! docker ps --filter "name=nginx${MM_ID}" --filter "status=running" -q | grep -q .; then
        echo "  The nginx${MM_ID} Docker container is not present AND running"
        return 0
    fi

    # Set the Nginx server IP
    query="{{range.NetworkSettings.Networks}}{{.IPAddress}}{{end}}"
    SERVER_ADDR=$(sudo docker inspect -f "${query}" "nginx${MM_ID}")
    export SERVER_ADDR

    SERVER_PORT=8080
    export SERVER_PORT=8080

    echo "  server_addr: $SERVER_ADDR"
    echo "  server_port: $SERVER_PORT"

    set_train "server_addr" "$SERVER_ADDR"
    set_train "server_port" "$SERVER_PORT"
}
