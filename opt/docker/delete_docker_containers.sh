#!/bin/bash

function delete_docker_containers {
    echo "${FUNCNAME[0]}():"

    if [[ "${TRAIN[task]:?}" != "delete" 
        && "${TRAIN[task]:?}" != "stop" ]]; then
        return 0
    fi

    echo "  Deleting the Docker containers for this instance . . ."

    if [[ -z "${TRAIN[docker_containers]:-}" ]]; then
        echo "  Define the custom TRAIN[docker_containers]"
        exit 1
    fi

    for container in ${TRAIN[docker_containers]}; do
        if [ "$(docker ps -a | grep -c "$container")" -gt 0 ]; then
            docker container rm -f -v "$container" | while IFS= read -r line; do
                echo "    $line"
            done
        fi
    done

    echo ""
}
