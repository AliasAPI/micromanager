#!/bin/bash

function configure_nginx_docker_image {
    echo "${FUNCNAME[0]}():"

    # Configure the Nginx version and host
    if [ -z "${NGINX_IMAGE:-}" ]; then
        export NGINX_IMAGE="bitnami/nginx:latest"
    fi

    if [ -z "${NGINX_HOST:-}" ]; then
        export NGINX_HOST="localhost"
    fi

    echo "  Nginx image: ${NGINX_IMAGE}"

    local container_name="nginx${TRAIN[mm_id]:?}"

    set_train "docker_images" "${TRAIN[docker_images]:-} $NGINX_IMAGE"

    set_train "docker_containers" "${TRAIN[docker_containers]:-} ${container_name}"

    if [[ "${TRAIN[task]:-}" != 'install' &&
          "${TRAIN[task]:-}" != 'config' ]]; then
        return 0
    fi

    local docker_run_command="docker run \
        --name \"${container_name}\" \
        --hostname app \
        --detach \
        --user \"${UID}\" \
        --restart always \
        --env \"NGINX_HOST=$NGINX_HOST\" \
        --volume \"$MM_DIR/app:$GIT_APP_PATH\" \
        --volume \"$MM_DIR/prompts:$GIT_PROMPTS_PATH\" \
        --volume \"$MM_DIR/data:/data\" \
        --volume \"$MM_DIR/opt/nginx:/opt/bitnami/nginx/conf/server_blocks\" \
        --network \"${MM_NETWORK}\" \
        --publish \"127.0.0.${MM_ID}:8000:8080\" \
        --publish \"127.0.0.${MM_ID}:3000:8443\" \
        --add-host \"host.docker.internal:host-gateway\" \
        \"${NGINX_IMAGE}\""

    set_train "spec_run_${container_name}" "${docker_run_command}"

    set_train "spec_deps_${container_name}" "php${TRAIN[mm_id]:?}"
}
