#!/bin/bash

function configure_tools_docker_image {
    echo "${FUNCNAME[0]}():"

    if [ -z "${TOOLS_SOURCE_IMAGE:-}" ]; then
        export TOOLS_SOURCE_IMAGE="mm/php-fpm:latest"
    fi

    if [ -z "${TOOLS_IMAGE:-}" ]; then
        export TOOLS_IMAGE="mm/tools:latest"
    fi

    echo " Tools image: ${TOOLS_IMAGE}"

    local container_name="tools${TRAIN[mm_id]:?}"


    set_train "docker_images" "${TRAIN[docker_images]:-} $TOOLS_IMAGE"

    set_train "docker_containers" "${TRAIN[docker_containers]:-} ${container_name}"

    if [[ "${TRAIN[task]:-}" != 'install' &&
          "${TRAIN[task]:-}" != 'config' ]]; then
        return 0
    fi

    set_train "build_image" "${TOOLS_IMAGE}"

    set_train "dockerfile_template_path" "${MM_DIR}/opt/docker/tools/Dockerfile.template"

    set_train "build_scripts_path" "${MM_DIR}/opt/docker/tools/build_scripts"

    local build_scripts="
        ${MM_DIR}/opt/apt_get/run_apt_get.sh
        ${MM_DIR}/opt/jq/run_jq.sh
        ${MM_DIR}/opt/debug/run_command.sh
        ${MM_DIR}/opt/debug/log_message.sh
        ${MM_DIR}/opt/traintrack/set_train.sh
        ${MM_DIR}/opt/traintrack/load_train.sh
        ${MM_DIR}/opt/docker/set_dockerized.sh
        ${MM_DIR}/opt/user/.bashrc
        ${MM_DIR}/${APP_DATA_FILES}/.train.json
        ${MM_DIR}/opt/python/run_python.sh
        ${MM_DIR}/opt/npm/run_npm.sh
        ${MM_DIR}/opt/pecl/run_pecl.sh
        ${MM_DIR}/opt/ast/use_ast.sh
        ${MM_DIR}/opt/tiktoken/run_tiktoken.sh
        ${MM_DIR}/opt/code_chunker/run_code_chunker.sh
        ${MM_DIR}/opt/playwright/run_playwright.sh
        ${MM_DIR}/opt/shellcheck/run_shellcheck.sh
        ${MM_DIR}/opt/shfmt/run_shfmt.sh
        ${MM_DIR}/opt/prettier/run_prettier.sh
        ${MM_DIR}/opt/prettier/install_prettier_plugins.sh
        ${MM_DIR}/opt/docker/finish_docker_build.sh"

    set_train "build_scripts" "$build_scripts"

    local docker_run_command="docker run \
        --name \"${container_name}\" \
        --hostname tools \
        --detach \
        --user \"${UID}\" \
        --restart always \
        --env \"TRAIN_JSON=${TRAIN_JSON}\" \
        --env \"APP_DATA_FILES=${APP_DATA_FILES}\" \
        --volume \"${MM_DIR}/app:$GIT_APP_PATH\" \
        --volume \"${MM_DIR}/prompts:$GIT_PROMPTS_PATH\" \
        --volume \"/etc/passwd:/etc/passwd:ro\" \
        --volume \"${MM_DIR}/data:/data\" \
        --volume \"${MM_DIR}/tools:/tools\" \
        --volume \"${MM_DIR}/opt:/opt/micromanager\" \
        --volume \"${MM_DIR}/opt/php/php.ini:/opt/bitnami/php/etc/conf.d/php.ini\" \
        --volume \"${MM_DIR}/opt/opcache/opcache.ini:/opt/bitnami/php/etc/conf.d/opcache.ini\" \
        --volume \"${MM_DIR}/opt/xdebug/xdebug.ini:/opt/bitnami/php/etc/conf.d/xdebug.ini\" \
        --network \"${MM_NETWORK}\" \
        --add-host \"host.docker.internal:host-gateway\" \
        \"${TOOLS_IMAGE}\""

    set_train "spec_run_${container_name}" "${docker_run_command}"

    set_train "spec_deps_${container_name}" "php${TRAIN[mm_id]:?} mariadb${TRAIN[mm_id]:?}"
}
