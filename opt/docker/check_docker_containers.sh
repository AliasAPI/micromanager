#!/bin/bash

function check_docker_containers {
    echo "${FUNCNAME[0]}():"

    if [ -z "${TRAIN[docker_containers]:-}" ]; then
        echo "  Configure TRAIN[docker_containers]"
        return 1
    fi

    # Loop through each container and check if it is running
    for container in ${TRAIN[docker_containers]}; do
        if ! docker container inspect -f '{{.State.Status}}' "$container" > /dev/null 2>&1; then
            echo "  The \"$container\" Docker container is not running"
            echo "  Use \"mm --help\" to list commands to start it"
            return 1
        fi
    done

    echo "  All MicroManager Docker containers are running"
}
