#!/usr/bin/env bash

function delete_docker_train_keys {
    if [[ "${TRAIN[task]:?}" == "install" ]] \
        && [[ "${TRAIN[dockerized]:?}" == "no" ]]; then
 
        # Loop through the array and unset keys that begin with spec_run_ or spec_deps_
        for key in "${!TRAIN[@]}"; do
            if [[ $key == spec_run_* || $key == spec_deps_* ]]; then
                unset "TRAIN[$key]"
            fi
        done

        # List the keys that are not needed after the Docker build 
        keys_to_unset="
            build_image 
            build_scripts
            build_scripts_path 
            dockerfile_template_path 
            dockerfiles 
            gpu_enable 
            gpu_type"

        # Add the keys that begin with spec_run_ or spec_deps_ 
        for key in "${!TRAIN[@]}"; do
            if [[ $key == spec_deps_* || $key == spec_run_* ]]; then
                keys_to_unset+=" $key"
            fi
        done

        # Loop through the keys in the string and unset them
        for key in $keys_to_unset; do
            unset "TRAIN[$key]"
        done
    fi
}