#!/bin/bash

function configure_gpu_docker_image {
    echo "${FUNCNAME[0]}():"

    if [[ "${GPU_ENABLE:?}" != "true" ||
        "${GPU_TYPE:?}" != "nvidia" ]]; then
        export NVIDIA_SOURCE_IMAGE="bitnami/minideb:latest"
    fi

    # Configure the GPU image
    if [ -z "${NVIDIA_SOURCE_IMAGE:-}" ]; then
        export NVIDIA_SOURCE_IMAGE="nvidia/cuda:12.2.2-base-ubuntu22.04"
    fi

    if [ -z "${GPU_IMAGE:-}" ]; then
        export GPU_IMAGE="mm/gpu:latest"
    fi

    if [ -z "${PYTHON_SOURCE_IMAGE:-}" ]; then
        export PYTHON_SOURCE_IMAGE="bitnami/python:latest"
    fi

    echo "  GPU image: ${GPU_IMAGE}"

    local container_name="gpu${TRAIN[mm_id]:?}"

    set_train "build_image" "${GPU_IMAGE}"

    set_train "docker_images" "${TRAIN[docker_images]:-} $GPU_IMAGE $NVIDIA_SOURCE_IMAGE $PYTHON_SOURCE_IMAGE"

    set_train "docker_containers" "${TRAIN[docker_containers]:-} ${container_name}"

    if [[ "${TRAIN[task]:-}" != 'install' &&
          "${TRAIN[task]:-}" != 'config' ]]; then
        return 0
    fi

    set_train "dockerfiles" "${TRAIN[dockerfiles]:-} ${MM_DIR}/opt/docker/gpu/Dockerfile"

    set_train "dockerfile_template_path" "${MM_DIR}/opt/docker/gpu/Dockerfile.template"

    set_train "build_scripts_path" "${MM_DIR}/opt/docker/gpu/build_scripts"

    local build_scripts="
        ${MM_DIR}/opt/apt_get/run_apt_get.sh
        ${MM_DIR}/opt/jq/run_jq.sh
        ${MM_DIR}/opt/debug/run_command.sh
        ${MM_DIR}/opt/debug/log_message.sh
        ${MM_DIR}/opt/traintrack/set_train.sh
        ${MM_DIR}/opt/traintrack/load_train.sh
        ${MM_DIR}/opt/docker/set_dockerized.sh
        ${MM_DIR}/opt/user/.bashrc
        ${MM_DIR}/${APP_DATA_FILES}/.train.json
        ${MM_DIR}/opt/poetry/run_poetry.sh
        ${MM_DIR}/opt/python/run_python.sh
        ${MM_DIR}/opt/ollama/pyproject.toml
        ${MM_DIR}/opt/ollama/run_ollama.sh
        ${MM_DIR}/opt/litellm/run_litellm.sh
        ${MM_DIR}/opt/docker/finish_docker_build.sh"

    set_train "build_scripts" "$build_scripts"

    local docker_run_command="docker run \
        --name \"${container_name}\" \
        --hostname gpu \
        --detach \
        --user \"${UID}\" \
        --restart always \
        --env \"TRAIN_JSON=${TRAIN_JSON}\" \
        --env \"APP_DATA_FILES=${APP_DATA_FILES}\" \
        --volume \"${MM_DIR}/app:$GIT_APP_PATH\" \
        --volume \"/etc/passwd:/etc/passwd:ro\" \
        --volume \"${MM_DIR}/data:/data\" \
        --volume \"${MM_DIR}/tools:/tools\" \
        --volume \"$MM_DIR/opt:/opt/micromanager\" \
        --network \"${MM_NETWORK}\" \
        --publish \"127.0.0.${MM_ID}:4000:4000\" \
        --add-host \"host.docker.internal:host-gateway\""

    if [[ "${GPU_ENABLE:?}" == "true" ]]; then
        docker_run_command="${docker_run_command} --gpus all"
    fi

    docker_run_command="${docker_run_command} \"mm/gpu:latest\""

    set_train "spec_run_${container_name}" "${docker_run_command}"

    set_train "spec_deps_${container_name}" "php${TRAIN[mm_id]:?} mariadb${TRAIN[mm_id]:?}"
}
