#!/bin/bash

function configure_mariadb_docker_image {
    echo "${FUNCNAME[0]}():"

    # Configure the MariaDB image if it is not set 
    if [ -z "${MARIADB_IMAGE:-}" ]; then
        export MARIADB_IMAGE="bitnami/mariadb:latest"
    fi

    echo "  MariaDB image: ${MARIADB_IMAGE}"

    local container_name="mariadb${TRAIN[mm_id]:?}"

    set_train "docker_images" "$MARIADB_IMAGE"

    set_train "docker_containers" "${container_name}"

    if [[ "${TRAIN[task]:-}" != 'install' &&
          "${TRAIN[task]:-}" != 'config' ]]; then
        return 0
    fi

    local docker_run_command="docker run \
        --name \"${container_name}\" \
        --hostname mariadb \
        --detach \
        --user \"${UID}\" \
        --restart always \
        --env \"MM_DIR=$MM_DIR\" \
        --env \"MARIADB_DATABASE=$MARIADB_DATABASE\" \
        --env \"ALLOW_EMPTY_PASSWORD=yes\" \
        --env \"MARIADB_ROOT_USER=$MARIADB_ROOT_USER\" \
        --env \"MARIADB_ROOT_PASSWORD=$MARIADB_ROOT_PASSWORD\" \
        --env \"MARIADB_USER=$MARIADB_USER\" \
        --env \"MARIADB_PASSWORD=$MARIADB_PASSWORD\" \
        --volume \"${MARIADB_DIR}:/bitnami/mariadb\" \
        --network \"${MM_NETWORK}\" \
        --publish \"127.0.0.${MM_ID}:8989:3306\" \
        --add-host \"host.docker.internal:host-gateway\" \
        \"${MARIADB_IMAGE}\""

    set_train "spec_run_${container_name}" "${docker_run_command}"

    set_train "spec_deps_${container_name}" ""
}
