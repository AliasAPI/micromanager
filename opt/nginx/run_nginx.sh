#!/bin/bash

function run_nginx {
    echo "${FUNCNAME[0]}():"

    if [[ "${TRAIN[task]:?}" != "install" &&
        "${TRAIN[task]:?}" != "restart" ]]; then
        return 0
    fi

    # Elrod todo:: Please finish this configure_nginx function

    # https://scotthelme.co.uk/a-new-security-header-referrer-policy/
    # https://blog.g3rt.nl/nginx-add_header-pitfall.html
    # https://community.letsencrypt.org/t/incomplete-chain-when-using-nginx-1-8-0-and-1-9-9/8500

    # https://serverfault.com/questions/577370/how-can-i-use-environment-variables-in-nginx-conf

    # Note creating certificates is in create_certificates.sh
    # to generate your dhparam.pem file, run in the terminal

    # openssl dhparam -out /opt/nginx/ssl/dhparam.pem 2048

    # Define the template
    # /opt/nginx/default.conf or http_template
    # else
    # /opt/nginx/https_template

    # replace the tags

    # write nginx.conf
}
