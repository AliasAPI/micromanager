function run_pecl {
    echo "${FUNCNAME[0]}():"

    # Install pecl
    if [[ "${TRAIN[task]:?}" == "install" ]] \
       && [[ "${TRAIN[dockerized]:?}" == "no" ]] \
       && ! command -v npm &> /dev/null; then

        echo "  Installing pecl . . ."
       
        # todo:: Elrod, Remove this hack (and use run_apt_get)
        apt-get clean
        rm -rf /var/lib/apt/lists/*
        apt-get update

        # Install pecl while indenting the output by 2 spaces
        apt-get install -y pecl 2>&1 | while IFS= read -r line; do
            echo "  $line"
        done

        # Update pecl 
        pecl channel-update pecl.php.net

        if [ $? -ne 0 ]; then
            echo "  Error: Failed to install pecl"
            exit 1
        fi
    fi

    # Configure pecl

    # Run pecl

    # Process pecl output
}
