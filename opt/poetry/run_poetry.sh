#!/bin/bash

# Run Poetry to manage the Python dependencies (while installing PrivateGPT)
# https://github.com/python-poetry/poetry
function run_poetry {
    echo "${FUNCNAME[0]}():"

    # Install Poetry
    # Set a custom directory for poetry
    export POETRY_HOME=/opt/poetry

    # The run_poetry function is run from a Dockerfile
    # The installation is located in the host computer
    # Checking the poetry command stops reinstallation
    if [[ "${TRAIN[task]:?}" == "install" ]] \
        && ! command -v poetry &> /dev/null; then
        echo "  Installing Poetry . . ."

        apt-get install -y curl

        # Capture all the output from the installation process
        install_output=$(curl -sSL https://install.python-poetry.org | python3 -)

        if [ $? -ne 0 ]; then
            echo "  Error: Failed to install Poetry"
            exit 1
        fi

        # Display the installation output indented 2 spaces
        while IFS= read -r line; do
            echo "$(sed 's/^/  /' <<< "$line")"
        done <<< "$install_output"
    fi

    # Configure Poetry
    # Set an alias for the Poetry command
    if [ -f "/opt/poetry/bin/poetry" ]; then
        export PATH="$PATH:/opt/poetry/bin"
    fi

    if [[ "${TRAIN[task]:?}" == "install" ||
        "${TRAIN[task]:?}" == "config" ]] \
        && command -v poetry &> /dev/null; then
        echo "  Configuring Poetry . . ."
        poetry config virtualenvs.in-project true

        echo " "
    fi

    # Run Poetry
}
