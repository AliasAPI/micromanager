<coding_conventions>
"""
File: code-terse.md
Description: Coding conventions and guidelines for software development
Version: 2024-10-10-013
"""

# Coding Conventions

# Loading Instructions
Please load and adhere to the coding guidelines specified in this file for all software development tasks in this conversation, including both new development and modifications to existing code. Apply these guidelines consistently when providing code suggestions, reviews, or edits. Ensure that any changes to existing code align with these standards while maintaining the original intent and functionality.

# File Header and Version Control
Include a header in all files:

<comment_start>
<comment> File: <filename>
<comment> Description: <brief purpose>
<comment> Version: YYYY-MM-DD-###
<comment_end_if_needed>

- Use existing comment syntax or infer from file type/extension
- Add missing header lines
- Update description if needed
- Version format: YYYY-MM-DD-###
- When editing:
  - Update date to current
  - Increment ### portion

Always update version when changing file.

## Introduction
These guidelines apply to all code. Refer to supplementary documents for language-specific details.

## General Principles
- Prioritize readability, maintainability, consistency, clarity, and simplicity.
- Adapt guidelines to fit language and project requirements.
- When modifying existing code, maintain consistency with the established style while improving adherence to these guidelines where possible.

## Code Modifications
- Refactor gradually, making incremental improvements that align with these guidelines.
- Document significant changes or deviations from the original implementation.
- Ensure that modifications do not introduce new bugs or break existing functionality.
- Update comments and documentation to reflect changes made to the code.

## Language-Specific Adaptation
- Apply principles in alignment with language idioms and community standards.

## General Guidelines
- File Header: Include the standard file header at the top of each file.
- Comments: Use high-level, plain English. Include inline comments for complex logic.
  - **Inline Comment Example**:
    ```python
    total = subtotal * tax_rate  # Calculate total price
    ```
  - **Block Comment Example**:
    ```python
    # Update inventory levels:
    # - Decrease stock by quantity sold
    # - Trigger reorder if below threshold
    ```
- Formatting: Consistent indentation and style per language standards.
- Naming: Descriptive, meaningful names following language conventions.
- Constants: Use named constants instead of magic numbers/strings.
- Organization: Group related functions/classes, use logical structure.
- Nesting: Avoid deep nesting.
- Data Structures: Use efficient, suitable structures.
- Performance: Optimize for efficiency, avoid redundancy.

## Documentation
- Module-Level: Describe purpose, components, dependencies, usage.
- Docstrings/Comments: Cover purpose, parameters, returns, exceptions, notes, checks.
- Use language-specific conventions and type annotations where supported.

- **Example Docstring**:

  ```python
  def create_task(title: str, description: str, due_date: date, priority: int = 1) -> Task:
      """
      Purpose:
          Create a new task with the given details.
      Parameters:
          title (str): Non-empty string up to 200 characters.
          description (str): Optional, up to 1000 characters.
          due_date (date): Must be a future date.
          priority (int, optional): Between 1 and 5 (default is 1).
      Returns:
          Task: The newly created task object.
      Raises:
          ValidationError: If inputs fail validation.
      Notes:
          - Initializes task status to 'Not Started'.
          - Triggers notification to project managers.
      Sanity Checks:
          - Validate input lengths and ranges.
          - Ensure due_date is in the future.
      """
      # Implementation details...
  ```

## Design Principles
- Single Responsibility Principle (SRP)
- Function Decomposition
- Encapsulation
- Reusability
- Conciseness
- Meaningful Names
- Self-Documentation
- Testing: Include comprehensive test cases, use TDD where appropriate.

## Advanced Concepts
- SOLID Principles
- Performance Considerations
- Generators and Lazy Evaluation

- **Generator Example** (Python):
  ```python
  def fibonacci_sequence():
      a, b = 0, 1
      while True:
          yield a
          a, b = b, a + b
  ```

## Validation and Error Handling
- Specify input requirements, validate explicitly.
- Handle exceptions gracefully, use meaningful error messages.
- Implement appropriate logging mechanisms.

## Debugging and Logging
- Use CustomLogger for flexible debugging and logging.
- Implement debug_interceptor for detailed function logging.
- Control debugging at global and function levels.
- Use delayed logging for object initialization.
- Implement safe __repr__ or separate debug_repr methods.
- Filter sensitive data in logs.

- **Implementation Example**:

  ```python
  from debug_interceptor import CustomLogger

  logger = CustomLogger(__name__)

  @logger.debug_interceptor(enabled=True, verbose=True, delay_logging=False)
  def process_data(data):
      # Function implementation
      processed = [d * 2 for d in data]
      return processed

  # Example with delayed logging for object initialization
  class MyClass:
      @logger.debug_interceptor(delay_logging=True)
      def __init__(self, x, y):
          self.x = x
          self.y = y
  ```

## Security Best Practices
- Protect sensitive data, use environment variables.
- Validate and sanitize inputs, prevent injection attacks.
- Implement secure authentication and authorization.
- Use encryption for data protection.
- Secure logging and monitoring.
- Manage dependencies securely.
- Follow least privilege principle.
- Conduct security-focused code reviews.

## Handling New Languages
- Apply general principles, adapt to language best practices.
- Follow official style guides or community standards.

## Additional Resources
- Refer to language-specific documents when needed.

## Safe Debugging Practices
- Use delay_logging for object initialization.
- Implement safe __repr__ methods.
- Consider separate debug_repr methods for detailed info.

- **Examples**:
  - Safe `__repr__` method:
    ```python
    def __repr__(self):
        attrs = ', '.join(f"{k}={getattr(self, k, 'uninitialized')}" for k in self.__dict__)
        return f"{self.__class__.__name__}({attrs})"
    ```
  - Using `delay_logging`:
    ```python
    @logger.debug_interceptor(delay_logging=True)
    def __init__(self, x, y):
        self.x = x
        self.y = y
    ```
  - Implementing `debug_repr`:
    ```python
    def debug_repr(self):
        return f"<{self.__class__.__name__} at {hex(id(self))}: " + \
               ', '.join(f"{k}={getattr(self, k, 'uninitialized')}" for k in self.__dict__) + ">"
    ```

## Quick Reference Checklist

- [ ] Consistent formatting and indentation according to language standards
- [ ] Module-level documentation explaining purpose and usage
- [ ] Comprehensive inline comments and documentation blocks
- [ ] Proper use of docstrings for functions, classes, and modules
- [ ] Single Responsibility Principle applied to functions and modules
- [ ] Function decomposition
- [ ] Encapsulation and reusability
- [ ] Concise functions and methods
- [ ] Meaningful names
- [ ] Adherence to naming conventions suited to the language
- [ ] Avoidance of magic numbers and hard-coded values
- [ ] Comments for complex algorithms
- [ ] Proper handling of external APIs
- [ ] Test cases and error handling
- [ ] Efficient data structures and algorithms
- [ ] Performance considerations
- [ ] Use of generators or iterators where appropriate
- [ ] Logging and debugging practices
- [ ] Implementation of debug_interceptor for detailed function logging where appropriate
- [ ] Use of debugging tools where appropriate
- [ ] Security best practices implemented

## Handling New Languages

- **Guidelines**:
  - If working with a language not specifically covered, apply the general principles and adapt them to the language's best practices.
  - Research and follow the language's official style guides or widely accepted community standards.
  - Maintain consistency and clarity in code.

- **Example**:
  - **TypeScript**: Follow similar conventions as in JavaScript, with added type annotations.
  - **C**: Use `snake_case` for variables and functions, `UPPER_CASE` for constants, and follow K&R or other accepted formatting styles.

## Python-Specific Guidelines
- Follow PEP 8 for formatting and style.
- Use appropriate naming conventions (snake_case, PascalCase, UPPER_CASE).
- Implement docstrings as per PEP 257.
- Use type hints (PEP 484).
- Use pytest or unittest for testing.

- **Example Validation Logic**:

  ```python
  import logging

  logging.basicConfig(
      level=logging.INFO,
      format='%(asctime)s - %(name)s - %(levelname)s - %(message)s'
  )
  logger = logging.getLogger(__name__)

  def create_task(title: str, description: str, due_date: date, priority: int = 1) -> Task:
      # Validate title
      if not title or len(title) > 200:
          logger.error("Invalid title length")
          raise ValidationError("Title must be 1-200 characters.")
      # Validate description
      if len(description) > 1000:
          logger.error("Invalid description length")
          raise ValidationError("Description must not exceed 1000 characters.")
      # Validate due_date
      if due_date <= date.today():
          logger.error("Due date is not in the future")
          raise ValidationError("Due date must be in the future.")
      # Validate priority
      if not 1 <= priority <= 5:
          logger.error("Priority out of range")
          raise ValidationError("Priority must be between 1 and 5.")
      logger.info("Task validation successful")
      # Create and return the task
      return Task(title, description, due_date, priority)
  ```
</coding_conventions>
