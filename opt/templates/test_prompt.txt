

MC/DC testing can be helpful. Unlike the scope of conditional coverage which becomes a combinatorial explosion, MC/DC scope scales very well, which makes writing unit tests faster.

https://en.m.wikipedia.org/wiki/Modified_condition/decision_coverage

https://arjayosma.medium.com/how-mc-dc-can-speedup-unit-test-creation-752f0fd9638c

Edit: added second link, wording.
