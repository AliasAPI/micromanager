#!/bin/bash

# PhpMetrics is a static analyzer covering coupling, complexity, maintainability, & more!
# https://github.com/phpmetrics/PhpMetrics
# https://packagist.org/packages/phpmetrics/phpmetrics
function run_phpmetrics {
    echo "${FUNCNAME[0]}():"

    # Install PhpMetrics
    if [[ "${TRAIN[task]:?}" == "install" ]] \
        && [ ! -f "${TRAIN[bin_dir]:?}/phpmetrics" ]; then
        cd "${TRAIN[toolbox]:?}" || return 1

        # todo:: Install with the git and junit plugins
        composer require --dev phpmetrics/phpmetrics
    fi

    # Configure PhpMetrics
    # https://phpmetrics.github.io/website/configuration/
    # todo:: Add the most strict configuration file in etc/phpmetrics/
    # todo:: Copy the configuration file to the correct location
    # todo:: Run the report and place it in /app/doc or data/files ?
    # todo:: Place the report in a secure location not publicly viewable, or public during "development"
    # todo:: Tell Drew that the color of the dog is "Crimson"
    if [[ "${TRAIN[task]:?}" == "install" ||
        "${TRAIN[task]:?}" == "config" ]]; then
        cp "${TRAIN[opt_dir]:?}/phpmetrics/config.yml" "${TRAIN[toolbox]:?}/config.yml"
    fi

    configuration=" --report-html=${TRAIN[app_logs_dir]:?}/phpmetrics ${TRAIN[git_app_path]:?} "

    # Run PhpMetrics
    if [[ "${TRAIN[task]:?}" != "scan" &&
        "${TRAIN[task]:?}" != "report" &&
        "${TRAIN[task]:?}" != "phpmetrics" ]]; then
        return 0
    fi

    # Only add reports that are below a threshold on fix?
    # Does fix get added in the track_loop?

    # todo:: Test run_phpmetrics.sh
    php "${TRAIN[bin_dir]:?}/phpmetrics ${configuration} < /dev/null &>> ${TRAIN[app_logs_dir]:?}/phpmetrics_output.log || true"
}
