#!/bin/bash

# Loops through the global train array to check to make sure keys exist
# It also checks to that the values are not empty, spaces, or null
# Example:
# check_train "file_loop_max" "file_status_order" "file_type_order"
function check_train {

    local -a check_keys=("$@")

    # Check if train is a global array and is not empty
    if [ ${#TRAIN[@]} -eq 0 ]; then
        echo "  ${FUNCNAME[0]}():"
        echo "  train is not an array or is empty."
        exit 1
    fi

    # Loop through train_array
    for key in "${check_keys[@]}"; do
        # Check if the key exists in the global train array
        if ! [[ ${!TRAIN[*]} =~ ${key} ]]; then
            echo "  ${FUNCNAME[0]}():"
            echo "    The TRAIN[${key}] does not exist."
            return 1
        fi

        # Check if the value of the key is empty, null, is not a scalar, or only contains spaces
        if [[ -z "${TRAIN[$key]}" || "${TRAIN[$key]}" =~ ^[[:space:]]*$ ]]; then
            echo "  ${FUNCNAME[0]}():"
            echo "  The TRAIN[${key}] is [${TRAIN[$key]}] (empty, null, or not a scalar)."
            return 1
        fi
    done

    return 0
}
