#!/bin/bash

function load_train {
    echo "${FUNCNAME[0]}():"

    if [ -f "/tmp/files/.train.json" ]; then
        train_file_path="/tmp/files/.train.json"
    fi

    if [ -f "/data/files/.train.json" ]; then
        train_file_path="/data/files/.train.json"
    fi

    if ! command -v jq > /dev/null 2>&1; then
        echo "  Error: Install jq to handle JSON files"
        return 1
    fi

    # Read the JSON file using jq and add the data to train
    json_data=$(jq -r '.' "$train_file_path")

    # Loop through the key-value pairs in the JSON data
    while IFS='=' read -r key value; do
        # Remove leading double quote from the key and value
        key=${key/#\"/}
        value=${value/#\"/}

        # Remove trailing double quote from the key and value
        key=${key/%\"/}
        value=${value/%\"/}

        # Add the key-value pair to the train associative array
        set_train "${key}" "${value}"

    done < <(jq -r 'to_entries[] | .key + "=" + (.value | tostring)' <<< "${json_data}")
}
