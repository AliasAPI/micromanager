#!/bin/bash

# create_file_port_path creates the new file_path needed to port from TrainTrack to another language
function create_file_port_path {
    echo "${FUNCNAME[0]}():"

    # IF MicroManager is not porting TrainTrack to another language, exit
    if [[ "${TRAIN[task]}" != 'fix' ||
        "${TRAIN[file_type]}" != 'tt' ||
        -z "${TRAIN[file_type_port]:-}" ]]; then
        return 0
    fi

    # Remove the git_prompts_path from the beginning of the file_path
    file_port_path="${TRAIN[file_path]#${TRAIN[git_prompts_path]}/}"
    echo "  The file_port_path is set to $file_port_path"

    # Prepend the git_app_path
    file_port_path="${TRAIN[git_app_path]:?}/${file_port_path}"

    # Remove the file_type extension from the end of the file_path
    file_port_path="${file_port_path%.${TRAIN[file_type]}}"
    echo "  The file_port_path is set to $file_port_path"

    # Append the file_type_port extension to the end of the file_path
    file_port_path="${file_port_path}.${TRAIN[file_type_port]:?}"

    set_train "file_port_path" "$file_port_path"

    echo "  Set file_port_path to ${file_port_path}"

    # Create the file_port_path so Aider can write the file
    local port_directory=$(dirname "${TRAIN[file_port_path]}")

    if [[ -d "$port_directory" ]]; then
        return 0
    fi

    mkdir -p "$port_directory"

    echo "  $port_directory created"
}
