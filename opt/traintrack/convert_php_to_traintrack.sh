#!/bin/bash

function convert_php_to_traintrack {
    echo "  ${FUNCNAME[0]}():"

    # If the first parameter is not a valid directory, exit()
    if [[ ! -d "$1" ]]; then
        echo "Specify a valid directory to convert"
        exit 1
    fi

    # Get all the files in the directory
    files=$(ls "$1"/*.php)

    # Loop through the files
    for file in $files; do
        # Get the filename without the extension
        basename=$(basename "$file" .php)

        # Check if the filename with a Txt extension doesn't exist
        if [ ! -e "$1/$basename.tt" ]; then
            echo "Processing $file . . ."

            # Open the file with the extension
            content=$(cat "$file")

            # Perform replacements
            content=${content//;/}
            content=${content//\(/[}
            content=${content//\)/]}
            content=${content//function/station}
            content=${content//namespace/area}
            content=${content//\'/}
            content=${content//$/}
            content=${content//\/\//\#}
            content=${content//\{/}
            content=${content//\}/}
            content=${content//<?php/}

            # Remove trailing spaces
            content=$(echo "$content" | sed 's/[[:space:]]*$//')

            # Save the modified file with a TxT extension
            echo "$content" > "$1/$basename.tt"
        fi
    done
}

# convert_php_to_traintrack "$@"
