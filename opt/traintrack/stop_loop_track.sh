#!/bin/bash

# Stop looping the libraries
function stop_loop_track {
    echo "${FUNCNAME[0]}():"

    if [[ "${TRAIN[task]:?}" != "fix" &&
        "${TRAIN[task]:?}" != "scan" &&
        "${TRAIN[task]:0:4}" != "run_" ]]; then
        set_train "loops_remaining" 0

        echo "  Setting loops_remaing to 0 since the task is [${TRAIN[task]:?}]"
        return 0
    fi

    # If there are flaws and force_flaw_fix=true, stop so that the developer can fix flaws
    if [[ "${TRAIN[force_flaw_fix]:?}" == true &&
        -n "${TRAIN[file_flaws]:-}" ]]; then
        set_train "loops_remaining" 0

        echo "  Stopping the loop_track since there are file_flaws"

        return 0
    fi
}
