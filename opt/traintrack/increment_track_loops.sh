#!/bin/bash

function increment_track_loops {
    echo "${FUNCNAME[0]}():"

    if [[ "${TRAIN[manage_file]:?}" != "yes" &&
        "${TRAIN[task]:0:4}" != "run_" ]]; then
        return 0
    fi

    if [[ -z "${TRAIN[track_loops]:-}" ]]; then
        set_train "track_loops" 0
    fi

    # Increment track_loops on each main loop process
    track_loops=$((TRAIN[track_loops] + 1))

    set_train "track_loops" "${track_loops}"

    echo "  There have been [${track_loops}] track_loops"
}
