#!/bin/bash

# Declare the TRAIN array globally
declare -gA TRAIN

function set_train {
    if [[ $# -lt 2 ]]; then
        caller_info=$(caller 0)
        caller_script=$(echo "$caller_info" | awk '{print $2}')
        echo "  Error in $caller_script: set_train requires two arguments (key and value)"

        return 1
    fi

    key="$1"
    value="$2"

    # Lowercase the key
    key="${key,,}"

    # Initialize TRAIN if not already set
    if [[ -z "${TRAIN+x}" ]]; then
        declare -gA TRAIN
    fi

    # Update the train with the key-value pair
    TRAIN["$key"]=$value
}
