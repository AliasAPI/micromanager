#!/bin/bash

function save_train {
    echo "${FUNCNAME[0]}():"

    if [[ "${TRAIN[task]:-}" == 'delete' ]]; then
        return 0
    fi

    # Check to make sure the TRAIN array exists
    if [[ ! "$(declare -p TRAIN 2>/dev/null)" =~ "declare -A" ]]; then
        echo "  The TRAIN associative array does not exist yet."
        return 0
    fi

    # Check if jq is installed
    if ! command -v jq &> /dev/null; then
        echo "  jq command not found. Please install jq to continue."
        return 1
    fi

    # Sort the train associative array to make reading easier
    mapfile -t sorted_keys < <(echo "${!TRAIN[@]}" | tr ' ' '\n' | LC_ALL=C sort)

    # Create an empty JSON object
    json_object="{}"

    # Loop through the sorted keys in the train associative array
    for key in "${sorted_keys[@]}"; do
        # Escape double quotes in the value
        value=$(printf '%s' "${TRAIN[$key]}" | sed 's/"/\\"/g')

        # Add key-value pair to the JSON object
        json_object=$(jq --arg k "$key" --arg v "$value" '.[$k]=$v' <<< "$json_object")
    done

    export TRAIN_JSON=$(echo "$json_object" | tr -d '\n' | sed 's/"/\\"/g' | tr -s ' ' | sed 's/: \\"/:\\"/g')

    if [[ "${TRAIN[dockerized]:-}" == "no" ]]; then
        set_train "train_file_path" "${DATA_FILES_DIR}/.train.json"
    fi

    if [[ "${TRAIN[dockerized]:-}" == "yes" ]]; then
        # Save the JSON object to a file inside the Docker container
        set_train "train_file_path" "${TRAIN[app_files_dir]}/.train.json"
    fi

    # Ensure train_file_path is set
    if [[ -z "${TRAIN[train_file_path]}" ]]; then
        echo "  train_file_path is not set in the TRAIN array."
        return 1
    fi

    # Create the directory path if it does not exist
    mkdir -p "$(dirname "${TRAIN[train_file_path]}")"

    touch "${TRAIN[train_file_path]}"

    echo "$json_object" > "${TRAIN[train_file_path]}"

    echo "  The TRAIN JSON file is located at ${TRAIN[train_file_path]}"
}