#!/bin/bash

function show_train {
    echo "${FUNCNAME[0]}():"

    if [ ${#TRAIN[@]} -eq 0 ]; then
        echo "The train global associative array does not exist"
        exit 1
    fi

    # Sort the keys and store them in an array
    local sorted_keys=()

    mapfile -t sorted_keys < <(echo "${!TRAIN[@]}" | tr ' ' '\n' | LC_ALL=C sort)

    # Print all key-value pairs in train with two-space indentation
    echo "  train = ["
    for key in "${sorted_keys[@]}"; do
        # todo:: Remove this if statement when show_train can handle values that contain \n
        if [[ ! -v "TRAIN[$key]" ]]; then
            continue
        fi

        value="${TRAIN[$key]}"

        # If the value is spaces, null, or an empty string, add quotes
        if [[ -z "${value// /}" ]]; then
            echo -e "    $key = \"$value\""
            continue
        fi

        echo -e "    $key = ${value}"
    done
    echo "  ]"

    exit 1
}
