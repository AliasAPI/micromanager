#!/bin/bash

function select_loops_remaining {
    echo "${FUNCNAME[0]}():"

    # If MicroManager is NOT processing files, return
    if [[ "${TRAIN[task]:-}" != "fix" &&
        "${TRAIN[task]:-}" != "scan" &&
        "${TRAIN[task]:0:4}" != "run_" ]]; then
        set_train "loops_remaining" 1
        return 0
    fi

    echo "  Counting the remaining files that should be processed . . ."

    # Set the query as a variable so that it can be echoed for debugging
    query="SELECT COUNT(file_path) FROM file_lists 
           WHERE file_status IN (${TRAIN[file_status_order]:-})
           AND file_status NOT IN ('failed', 'skipped', 'passed', 'ported', 'fixed', 'unapproved', 'approved' )
           AND file_scans < ${TRAIN[file_scan_max]:-}"

    # Execute the SELECT statement to retrieve the file(s) to fix
    if ! result=$(${TRAIN[mariadb_connect]:-} -s -e "${query}" 2>&1); then
        echo "  ${result}"
        echo "  Query: ${query}"
        exit 1
    fi

    # Check if the result is a number
    if [[ ! ${result} =~ ^[0-9]+$ ]]; then
        echo "  ${result}"
        echo "  Query: ${query}"
        exit 1
    fi

    # Set the value in a variable
    row=($result)

    # Multiply the number of files to fix by the maximum number of times each should be processed
    loops_remaining=$((row[0] * ${TRAIN[file_loop_max]:?}))

    set_train "loops_remaining" "${loops_remaining}"

    echo "  The [${row[0]}] remaining files X [${TRAIN[file_loop_max]}] file_loop_max = ${loops_remaining}"
}
