#!/bin/bash

# Delete the file_flaws each time the file is scanned
# The linters and static analyzers will insert new flaws
function delete_file_flaws {
    echo "${FUNCNAME[0]}():"

    if [[ "${TRAIN[manage_file]:?}" != "yes" &&
        "${TRAIN[task]:0:4}" != "run_" ]]; then
        return 0
    fi

    # Use a prepared statement to delete the flaws related to the file
    # The linters and static analyzers will insert the flaws every run
    query="PREPARE statement FROM 
          'DELETE FROM file_flaws
           WHERE file_path = ?';

           SET @file_path = '${TRAIN[file_path]:?}';

           EXECUTE statement USING @file_path; "

    echo "  Deleting all ${TRAIN[file_name]:?} related flaws"

    # Check if there is a MySQL flaw in the query result
    if ! result=$(${TRAIN[mariadb_connect]:?} -s -e "${query}" 2>&1); then
        echo "  ${result}"
        echo "  Query: ${query}"
        exit 1
    fi
}
