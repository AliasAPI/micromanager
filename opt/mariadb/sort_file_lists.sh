#!/bin/bash

function sort_file_lists {
    echo "${FUNCNAME[0]}():"

    if [[ "${TRAIN[task]:?}" != "install" &&
        "${TRAIN[task]:?}" != "restart" &&
        "${TRAIN[task]:?}" != "config" ]] \
        || [[ -z "${TRAIN[file_status_order]:-}" ]] \
        || [[ -z "${TRAIN[file_type_order]:-}" ]] \
        || [[ -z "${TRAIN[mariadb_connect]:-}" ]]; then
        return 0
    fi

    # Drop sorted_file_lists table if it already exists
    error=$(${TRAIN[mariadb_connect]:?} -e "DROP TABLE IF EXISTS sorted_file_lists" 2>&1 > /dev/null)

    # Check if there is a MySQL error in the query result
    if [[ -n "${error}" ]]; then
        echo "  ${error}"
        exit 1
    fi

    echo "  Creating sorted_file_lists table"

    # Create sorted_file_lists table
    error=$(${TRAIN[mariadb_connect]:?} -e "CREATE TABLE sorted_file_lists LIKE file_lists" 2>&1 > /dev/null)

    # Check if there is a MySQL error in the query result
    if [[ -n "${error}" ]]; then
        echo "  ${error}"
        exit 1
    fi

    echo "  Sorting the file lists and copying to the sorted_file_lists table"

    # Set the query as a variable so that it can be echoed for debugging
    query="INSERT INTO sorted_file_lists
           SELECT file_name, file_path, file_type, file_scans, file_tokens, file_md5sum, file_mods, file_status
           FROM file_lists 
           ORDER BY 
               FIELD(file_status, ${TRAIN[file_status_order]:?} ),
               FIELD(file_type, ${TRAIN[file_type_order]:?} ),
               file_path ASC"

    # Run the query to insert sorted data into the sorted_file_lists table
    error="$(${TRAIN[mariadb_connect]:?} -s -e "${query}" 2>&1 > /dev/null)"

    # Check if there is a MySQL error in the query result
    if [[ -n "${error}" ]]; then
        echo "  ${error}"
        echo "  ${query}"
        exit 1
    fi

    echo "  Dropping the file_lists table"

    # Drop file_lists table
    error=$(${TRAIN[mariadb_connect]:?} -e "DROP TABLE IF EXISTS file_lists" 2>&1 > /dev/null)

    # Check if there is a MySQL error in the query result
    if [[ -n "${error}" ]]; then
        echo "  ${error}"
        exit 1
    fi

    echo "  Creating the file_lists table"

    # Create file_lists table with the sorted data
    error=$(${TRAIN[mariadb_connect]:?} -e "CREATE TABLE file_lists LIKE sorted_file_lists" 2>&1 > /dev/null)

    # Check if there is a MySQL error in the query result
    if [[ -n "${error}" ]]; then
        echo "  ${error}"
        exit 1
    fi

    echo "  Copying sorted_file_lists data to file_lists table"

    error=$(${TRAIN[mariadb_connect]:?} -e "INSERT INTO file_lists SELECT * FROM sorted_file_lists" 2>&1 > /dev/null)

    if [[ -n "${error}" ]]; then
        echo "  ${error}"
        exit 1
    fi

    echo "  Dropping sorted_file_lists table"

    error=$(${TRAIN[mariadb_connect]:?} -e "DROP TABLE IF EXISTS sorted_file_lists" 2>&1 > /dev/null)

    # Check if there is a MySQL error in the query result
    if [[ -n "${error}" ]]; then
        echo "  ${error}"
        exit 1
    fi
}
