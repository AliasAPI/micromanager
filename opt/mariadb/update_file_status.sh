#!/bin/bash

# fixing, new, passed, failed, fixed, unapproved, approved
function update_file_status {
    echo "${FUNCNAME[0]}():"

    if [[ "${TRAIN[manage_file]:?}" != "yes" &&
        "${TRAIN[task]:0:4}" != "run_" ]]; then
        return 0
    fi

    # Recalculate the tokens when fixing the files in the git_app_path
    if [[ "${TRAIN[task]}" == "fix" &&
          "${TRAIN[file_path]}" == *"${TRAIN[git_app_path]}"* ]]; then
        run_tiktoken
    fi

    query="PREPARE statement FROM 
        'UPDATE file_lists SET file_scans = ?, file_tokens = ?, file_mods = ?, file_status = ?
         WHERE file_path = ? 
         LIMIT 1';

         SET @file_scans = '${TRAIN[file_scans]:?}';
         SET @file_tokens = '${TRAIN[file_tokens]:?}';
         SET @file_status = '${TRAIN[file_status]:?}';
         SET @file_mods = '${TRAIN[file_mods]:?}';
         SET @file_path = '${TRAIN[file_path]:?}';

         EXECUTE statement USING @file_scans, @file_tokens, @file_mods, @file_status, @file_path;"

    echo "  Updating file_scans, file_tokens, file_mods, and file_status"

    # Update the file_scans, file_status, to help track the progress
    if ! result=$(${TRAIN[mariadb_connect]:?} -s -e "${query}" 2>&1); then
        echo "  ${result}"
        echo "  Query: ${query}"
        exit 1
    fi
}
