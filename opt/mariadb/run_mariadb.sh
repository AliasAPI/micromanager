#!/bin/bash

# Run MariaDB to set up an run MariaDB
function run_mariadb {
    echo "${FUNCNAME[0]}():"

    # Install MariaDB
    # MariaDB is installed while creating the custom Docker container

    # Configure MariaDB
    if [[ "${TRAIN[dockerized]:?}" == "no" ]]; then
        # https://docs.docker.com/compose/compose-file/build/#secrets
        export ALLOW_EMPTY_PASSWORD="yes"
        export MARIADB_DATABASE="micromanager"
        export MARIADB_HOST="mariadb${TRAIN[mm_id]:?}"
        export MARIADB_OPTIONS=""
        export MARIADB_PASSWORD=""
        export MARIADB_PORT="3306"
        export MARIADB_ROOT_PASSWORD=""
        export MARIADB_ROOT_USER="root"
        export MARIADB_TAG="latest"
        export MARIADB_USER="user"
        export MARIADB_DIR="${MM_DIR}/data/mariadb"

        # Set a command to be used to query MariaDB
        mariadb_connect="mysql --host=${MARIADB_HOST} --port=${MARIADB_PORT} --user=${MARIADB_ROOT_USER}"

        # Use a user password for a little better security
        if [[ -n ${MARIADB_PASSWORD} ]]; then
            local mariadb_connect+=" --password=${MARIADB_PASSWORD}"
        fi

        # If the user mariadb password is NOT set, but the root password is set, use the root password
        if [[ -z "${MARIADB_PASSWORD}" &&
            -n "${MARIADB_ROOT_PASSWORD}" ]]; then
            mariadb_connect+=" --password=${MARIADB_ROOT_PASSWORD}"
        fi

        if [[ -n ${MARIADB_OPTIONS} ]]; then
            mariadb_connect+=" ${MARIADB_OPTIONS}"
        fi

        # Set the database DSN to be used by the application
        export DATABASE_DSN="mysql:host=${MARIADB_HOST};port=${MARIADB_PORT};charset=utf8mb4"

        set_train "database_dsn" "${DATABASE_DSN}"
        set_train "mariadb_connect" "${mariadb_connect}"
        set_train "mariadb_database" "${MARIADB_DATABASE}"
        set_train "mariadb_dir" "${MARIADB_DIR}"
        set_train "mariadb_host" "${MARIADB_HOST}"
        set_train "mariadb_password" "${MARIADB_PASSWORD}"
        set_train "mariadb_port" "${MARIADB_PORT}"
        set_train "mariadb_root_password" "${MARIADB_ROOT_PASSWORD}"
        set_train "mariadb_root_user" "${MARIADB_ROOT_USER}"
        set_train "mariadb_user" "${MARIADB_USER}"
        set_train "mariadb_tag" "${MARIADB_TAG}"

        if [[ "${TRAIN[task]:?}" == "install" ||
            "${TRAIN[task]:?}" == "restart" ]]; then
            echo "  database_dsn: ${TRAIN[database_dsn]}"
            echo "  mariadb_database: ${TRAIN[mariadb_database]}"
            echo "  mariadb_dir: ${TRAIN[mariadb_dir]}"
            echo "  mariadb_host: ${TRAIN[mariadb_host]}"
            echo "  mariadb_password: ${TRAIN[mariadb_password]}"
            echo "  mariadb_port: ${TRAIN[mariadb_port]}"
            echo "  mariadb_root_password: ${TRAIN[mariadb_root_password]}"
            echo "  mariadb_root_user: ${TRAIN[mariadb_root_user]}"
            echo "  mariadb_tag: ${TRAIN[mariadb_tag]}"
            echo "  mariadb_user: ${TRAIN[mariadb_user]}"
        fi
    fi

    # Run MariaDB
    if [[ "${TRAIN[task]:?}" != "install" &&
        "${TRAIN[task]:?}" != "restart" ]] \
        || [[ "${TRAIN[dockerized]:?}" == "no" ]]; then
        return 0
    fi

    if ! command -v mysql &> /dev/null; then
        echo "The 'mysql' command is not found. Please install MariaDB client."
        exit 1
    fi

    # If there is a different error, display it and stop Micromanager
    if ! connection_error=$(${TRAIN[mariadb_connect]:?} -s -e "SHOW DATABASES;" 2>&1 > /dev/null); then
        # If the error is NOT "ERROR 2002 (HY000): Can't connect to server on 'mariadb' (115)"
        if [[ ! "${connection_error}" =~ "ERROR 2002" ]]; then
            # Display the error (that is probably not related to the connection)
            echo "  ${connection_error}"
            echo "  Command: ${command}"
            exit 1
        fi

        echo "  Waiting for the MariaDB server to start . . ."

        # Allow time for the MariaDB server to start
        until ${TRAIN[mariadb_connect]} -s -e "SHOW DATABASES;" > /dev/null 2>&1; do
            sleep 1
        done
    fi

    echo "  Created TRAIN[mariadb_connect] command and tested it successfully."
    echo "  Set TRAIN[database_dsn] to similar values."
}
