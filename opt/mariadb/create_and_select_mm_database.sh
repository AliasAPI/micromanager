#!/bin/bash

function create_and_select_mm_database {
    echo "${FUNCNAME[0]}():"

    if [[ -z "${TRAIN[mariadb_connect]+x}" ]]; then
        echo "  TRAIN[mariadb_connect] command variable not set."
        exit 1
    fi

    if [ -z "${TRAIN[mariadb_database]}" ]; then
        echo "  TRAIN[mariadb_database] environment variable not set."
        exit 1
    fi

    # Create the database
    error="$(${TRAIN[mariadb_connect]:?} -s -e "CREATE DATABASE IF NOT EXISTS ${TRAIN[mariadb_database]:?}")"

    if [[ -n "$error" ]]; then
        echo " eeeeeeee  ${error}"
        exit 1
    fi

    echo "  Created the [${TRAIN[mariadb_database]:?}] database"
    echo "  Updated the TRAIN[mariadb_connect] command"

    # Select the database using the MARIADB_CONNECT variable
    TRAIN[mariadb_connect]="${TRAIN[mariadb_connect]:?} --database ${TRAIN[mariadb_database]:?}"
}
