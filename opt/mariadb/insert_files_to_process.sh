#!/bin/bash

# TRAIN[files_to_process] is a file that contains file_paths to files one per line
# The file_paths have been sorted by file type and put in a preferred order for fixing each type
# Finally, the file_paths are put in alphabetical order within each type. This function inserts them
function insert_files_to_process {
    echo "${FUNCNAME[0]}():"

    if [[ "${TRAIN[task]:?}" != "install" 
       && "${TRAIN[task]:?}" != "restart" ]]; then
        return 0
    fi

    if [ -z "${TRAIN["files_to_process"]:-}" ]; then
        echo "  TRAIN[files_to_process] environment variable not set."
        exit 1
    fi

    if [ ! -f "/${TRAIN["files_to_process"]:?}" ]; then
        echo "  /${TRAIN["files_to_process"]:?} is not a file."
        exit 1
    fi

    # Loop through each file_path in the files_to_process file
    while read -r file_path; do
        if [ -z "${file_path}" ]; then
            continue
        fi

        set_train "file_path" "$file_path"

        if [ ! -f "${TRAIN[file_path]}" ]; then
            echo "  ${TRAIN[file_path]} does not exist."
            continue
        fi

        # Prepare the statement with a placeholder for the file_path value
        file_path_exists="$(${TRAIN[mariadb_connect]:?} -s -e "PREPARE statement FROM 
            'SELECT file_path FROM file_lists WHERE file_path = ?';

             SET @file_path='${TRAIN[file_path]}'; 

             EXECUTE statement USING @file_path;" 2>&1)"

        # If the file_path does NOT exist in the file_lists table
        if [[ -f "${TRAIN[file_path]}" ]] && [[ -z "${file_path_exists}" ]]; then
            # Set variables for basename, file extension, md5sum, and status
            file_name=$(basename "${TRAIN[file_path]}")

            file_type="${TRAIN[file_path]##*.}"

            # Compute the md5sum and save it to track file changes
            file_md5sum=$(md5sum "${TRAIN[file_path]}" | awk '{print $1}')

            set_train "file_tokens" 0
            
            # Calculate the file_tokens
            run_tiktoken

            echo "  Inserting ${TRAIN[file_path]} with ${TRAIN[file_tokens]:?} file tokens"

            # Create a prepared INSERT statement with placeholders for values
            error="$(${TRAIN[mariadb_connect]:?} -s -e "PREPARE statement FROM 
                'INSERT IGNORE INTO file_lists 
                 (file_name, file_path, file_type, file_scans, file_tokens, file_md5sum, file_mods, file_status) 
                 VALUES (?, ?, ?, ?, ?, ?, ?, ?);';

                 SET @file_name = '${file_name}';
                 SET @file_path = '${TRAIN[file_path]}';
                 SET @file_type = '${file_type}';
                 SET @file_scans = 0;
                 SET @file_tokens = '${TRAIN[file_tokens]}';
                 SET @file_md5sum = '${file_md5sum}';
                 SET @file_mods = 0;
                 SET @file_status = 'new';

                 EXECUTE statement USING @file_name, @file_path, @file_type, @file_scans, 
                 @file_tokens, @file_md5sum, @file_mods, @file_status;" 2>&1 > /dev/null)"

            if [[ -n "${error}" ]]; then
                echo "  Error: ${error}"
                exit 1
            fi
        fi
    done < "/${TRAIN[files_to_process]:?}"

    echo "  Files to process inserted in the [${TRAIN[mariadb_database]:?}] file_lists table"
}
