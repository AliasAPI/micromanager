#!/bin/bash

# todo:: Consider using https://github.com/quickshiftin/mysqlbkup
# https://github.com/mmerian/MySQL-Maint/blob/master/mysql_maint.sh
function export_mariadb_databases {
    echo "${FUNCNAME[0]}():"

    if [[ "${TRAIN[task]:?}" != "export" ]]; then
        return 0
    fi

    MARIADB_DUMPS_DIR="$MM_DIR/data/dumps/mariadb"

    export MARIADB_DUMPS_DIR

    mkdir -p "${TRAIN[db_dumps_dir]:?}"

    dump_file="${TRAIN[db_dumps_dir]:?}/$(TZ=America/New_York date "+%Y%m%d%H%M%S%z").sql"

    if [[ -z "${TRAIN[mariadb_root_password]:?}" ]]; then
        mysqldump --all-databases -u "${TRAIN[mariadb_root_user]:?}" > "${dump_file}" 2> /dev/null
    fi

    if [[ -n "${TRAIN[mariadb_root_password]:?}" ]]; then
        mysqldump --all-databases -u "${TRAIN[mariadb_root_user]:?}" -p"${TRAIN[mariadb_root_password]:?}" > "${dump_file}" 2> /dev/null
    fi

    # SC2181: Check exit code directly with e.g. 'if mycmd;', not indirectly with $?.
    # We are checking the return code of either of two commands run in the above if statiement with this one indirect check
    # shellcheck disable=SC2181
    if [ "$?" -ne 0 ]; then
        echo "  mysqldump failed"
        exit 1
    fi

    xz -z "${dump_file}"

    gpg --output "${dump_file}.xz.gpg" --symmetric "${dump_file}.xz"

    rm "${dump_file}.xz"
}
