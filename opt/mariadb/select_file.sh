#!/bin/bash

function select_file {
    echo ""
    echo "${FUNCNAME[0]}():"

    if [[ "${TRAIN[task]:?}" != "fix" &&
        "${TRAIN[task]:?}" != "scan" &&
        "${TRAIN[task]:0:4}" != "run_" ]]; then
        echo "  A file is only selected for fix, scan, and run_ tasks"
        return 0
    fi

    echo "  Selecting one file to scan or fix order by status and type"

    # Set the query as a variable so that it can be echoed for debugging
    query="SELECT file_name, file_path, file_type, file_scans, file_tokens, file_md5sum, file_mods, file_status
           FROM file_lists 
           WHERE file_scans < ${TRAIN[file_scan_max]:?} 
           ORDER BY 
               FIELD(file_status, ${TRAIN[file_status_order]:?} ),
               FIELD(file_type, ${TRAIN[file_type_order]:?} ),
               file_path ASC
           LIMIT 1"

    # Execute the SELECT statement to retrieve one file
    if ! result=$(${TRAIN[mariadb_connect]:?} -s -e "${query}" 2>&1); then
        echo "  ${result}"
        echo "  Query: ${query}"
        exit 1
    fi

    # Store the values in an array
    read -ra row <<< "${result}"

    # If there is NOT a file to fix or the file does not exist, unset the file keys
    # if [[ -z "${result}" || ! -f "${row[1]}" ]]; then
    if [[ -z "${result}" ]]; then
        echo "  No file found to fix"
        unset 'TRAIN[file_name]'
        unset 'TRAIN[file_path]'
        unset 'TRAIN[file_type]'
        unset 'TRAIN[file_scans]'
        unset 'TRAIN[file_tokens]'
        unset 'TRAIN[file_md5sum]'
        unset 'TRAIN[file_mods]'
        unset 'TRAIN[file_status]'
        unset 'TRAIN[file_changed]'
        unset 'TRAIN[file_scanned]'
        return 0
    fi

    # Start with the track_loops set to 0 for each file
    if [[ "${TRAIN[file_path]:-}" != "${row[1]}" ]]; then
        set_train "track_loops" 0
    fi

    # Onboard the file attributes onto the TRAIN
    set_train "file_name" "${row[0]}"
    set_train "file_path" "${row[1]}"
    set_train "file_type" "${row[2]}"
    set_train "file_scans" "${row[3]}"
    set_train "file_tokens" "${row[4]}"
    set_train "file_md5sum" "${row[5]}"
    set_train "file_mods" "${row[6]}"
    set_train "file_status" "${row[7]}"
    set_train "file_changed" false
    set_train "file_scanned" false

    unset 'TRAIN[file_flaws]'

    echo "  file_path: ${row[1]}"
    echo "  file_status: ${row[6]}"
}
