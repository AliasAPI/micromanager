#!/bin/bash

function create_mm_tables {
    echo "${FUNCNAME[0]}():"

    if [[ -z ${TRAIN[mariadb_connect]+x} ]]; then
        echo "  TRAIN[mariadb_connect] not set."
        exit 1
    fi

    if [[ "${TRAIN[task]:?}" != "install" &&
        "${TRAIN[task]:?}" != "restart" &&
        "${TRAIN[task]:?}" != "config" ]]; then
        return 0
    fi

    # Define the SQL queries to create tables if they do not exist
    local file_lists_query="CREATE TABLE IF NOT EXISTS file_lists (
        file_name VARCHAR(100),
        file_path VARCHAR(250) UNIQUE,
        file_type VARCHAR(6),
        file_scans INT(6),
        file_tokens INT(6),
        file_md5sum VARCHAR(32),
        file_mods INT(6),
        file_status VARCHAR(20)
    );"

    # Create the file_lists table
    error="$(${TRAIN[mariadb_connect]:?} -s -e "${file_lists_query}" 2>&1 > /dev/null)"

    if [[ -n ${error} ]]; then
        echo "  ${error}"
        exit 1
    fi

    echo "  The file_lists table has been created"

    local file_flaws_query="CREATE TABLE IF NOT EXISTS file_flaws (
        file_path VARCHAR(250),
        line INT(5) DEFAULT 0,
        columns INT(5) DEFAULT 0,
        flaw text DEFAULT NULL,
        tool VARCHAR(20)
    );"

    # Create the file_lists table
    error="$(${TRAIN[mariadb_connect]:?} -s -e "${file_flaws_query}" 2>&1 > /dev/null)"

    if [[ -n ${error} ]]; then
        echo "  ${error}"
        exit 1
    fi

    echo "  The file_flaws table has been created"
}
