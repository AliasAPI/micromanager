#!/bin/bash

# Call this function like this: insert_file_flaws "$file_flaws" "$tool"
function insert_file_flaws {
    echo "${FUNCNAME[0]}():"

    local file_flaws="$1"
    local tool="$2"
    local flaws_inserted=false

    # Replace all of the \" with * in the file_flaws JSON so the flaw_message won't be truncated
    file_flaws="${file_flaws//\\\"/*}"

    # Loop through lines and save the entire line as "message"
    while IFS= read -r line; do
        # If the flaw is blank or just spaces, do not insert it
        if [[ -z "${line}" || -z "${line//[^[:space:]]/}" ]]; then
            continue
        fi

        # Extract the values from the $line string (that was created by jq)
        file_path=$(grep -oE '"file_path":"[^"]*"' <<< "$line" | grep -oP '(?<="file_path":")[^"]+')
        line_number=$(grep -oE '"line":[0-9]+' <<< "$line" | grep -oE '[0-9]+')
        column_number=$(grep -oE '"column":[0-9]+' <<< "$line" | grep -oE '[0-9]+')
        flaw_message=$(grep -oE '"flaw":"[^"]*"' <<< "$line" | grep -oE '[^"]+')

        # Remove multiple spaces, backslashes, line returns, and trailing slashes
        flaw_message=$(clean_flaw "${flaw_message}")

        if [[ -z "${file_path}" ]]; then
            echo "    file_path is not set correctly"
            exit 1
        fi

        if ! [[ $line_number =~ ^[0-9]+$ ]]; then
            echo "    line_number is not a number"
            exit 1
        fi

        if ! [[ $column_number =~ ^[0-9]+$ ]]; then
            echo "    column_number is not a number"
            exit 1
        fi

        if [[ -z "${flaw_message}" ]]; then
            echo "    flaw is not set correctly"
            exit 1
        fi

        # If the file_path has leading "..", remove it
        file_path=${file_path#../}

        query="PREPARE statement FROM 
            'SELECT file_path FROM file_flaws 
             WHERE file_path = ? 
             AND line = ?
             AND columns = ?
             AND flaw = ?  LIMIT 1';

             SET @file_path = '${file_path}';
             SET @line = '${line_number}';
             SET @columns = '${column_number}';
             SET @flaw = '${flaw_message}';
             
             EXECUTE statement USING @file_path, @line, @columns, @flaw;"

        # If there is a row that matches file_path and message . . .
        result=$(${TRAIN[mariadb_connect]:?} -N -e "${query}")

        # . . . skip the insert so that only unique flaws are stored
        if [ -n "${result}" ]; then
            continue
        fi

        echo "    Inserting: [${file_path}], line:${line_number}, columns:${column_number}, flaw:${flaw_message}, tool:${tool}"

        query="PREPARE statement FROM 
            'INSERT INTO file_flaws 
            (file_path, line, columns, flaw, tool) 
            VALUES (?, ?, ?, ?, ?);';

            SET @file_path = '${file_path}';
            SET @line = '${line_number}';
            SET @columns = '${column_number}';
            SET @flaw = '${flaw_message}';
            SET @tool = '${tool}';

            EXECUTE statement USING @file_path, @line, @columns, @flaw, @tool; "

        # Insert flaws in the file_flaws table using a prepared statement
        if ! result=$(${TRAIN[mariadb_connect]:?} -s -e "${query}" 2>&1); then
            echo "  ${result}"
            echo "  Query: ${query}"
            exit 1
        fi

        if [ -n "${result}" ]; then
            echo "  ${result}"
            return 1
        fi

        # Update flaws_inserted so that the message below displays once
        flaws_inserted=true
    done <<< "$file_flaws"

    if [ "${flaws_inserted}" = true ]; then
        echo "    Unique flaws inserted into ${TRAIN[mariadb_database]:?} file_flaws table"
    fi
}
