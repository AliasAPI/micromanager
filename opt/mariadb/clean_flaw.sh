#!/bin/bash

function clean_flaw {
    local flaw="$1"

    # Example flaw with repeated backslashes, spaces, line return, and flaw:
    # flaw="flaw:\n{Th'is\\\\\\ is\\\\\\
    #       a\\\\\\ test\\\\\\ string} with\n \n\nflaw: spaces here [       ]\\\\"

    # Remove line returns (newline characters and carriage returns)
    # flaw=$(echo "${flaw}" | tr -d '\n\r')
    flaw=$(echo ${flaw})

    # Trim leading and trailing whitespace from $flaw
    flaw=$(echo "$flaw" | awk '{$1=$1};1')

    # Remove leading newline (\n) from the string
    flaw=${flaw##$'\n'}

    # Replace "\n " with "\n"
    flaw=${flaw//\\n\\n/\\n}
    flaw=${flaw//\\n\ /\\n}
    flaw=${flaw//\ \\n/\\n}

    # Replace multiple occurrences of \n with a single \n
    flaw=$(echo "$flaw" | sed 's/\\n\\n\+/\n/g')

    # Replace single backslashes (\) with two backslashes (\\)
    flaw="${flaw//\\/\\\\}"

    # Replace repeated backslashes with a double backslash
    flaw="${flaw//\\\\\\\\\\\\\\\\\\\\\\\\\\/\\}"
    flaw="${flaw//\\\\\\/\\}"

    # Remove the flaw: column name
    flaw=${flaw//flaw:/}
    flaw=${flaw//flaw :/}

    # Remove leading and trailing double quotes from flaw
    flaw=${flaw#\"}
    flaw=${flaw%\"}

    # Remove unwanted characters from flaw
    flaw=${flaw//\'/*}
    flaw=${flaw//\"/*}

    # Add trailing spaces to braces
    flaw="${flaw//\{/\{\ }"
    flaw="${flaw//\}/\}\ }"

    # Replace multiple spaces with a single space
    flaw=$(echo "${flaw}" | tr --squeeze-repeats ' ')

    # Squeeze repeated spaces into a single space
    # todo:: Test to see if spaces still need removing
    # This extra? line was in select_file_flaws()
    flaw=$(echo "$flaw" | tr -s ' ')

    # trim
    flaw="${flaw##*( )}"
    flaw="${flaw%%*( )}"

    # Truncate the flaw value if it exceeds the file_flaw flaw column's 250 character limit
    if [ ${#flaw} -gt 1000 ]; then
        flaw=${flaw:0:995}"..."
    fi

    # Remove all trailing backslashes. This works.
    flaw="${flaw%"${flaw##*[!\\]}"}"

    # Replace all of the \" with * in the file_flaws JSON so the flaw_message won't be truncated
    flaw="${flaw//\\\"/*}"

    # Return the modified flaw value
    echo "${flaw}"
}

# clean_flaw
