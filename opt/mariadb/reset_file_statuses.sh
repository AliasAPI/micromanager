#!/bin/bash

# Use "mm reset" to update file_lists to file_scans=0, file_status=new', and file_mods=0
# fixing, new, failed, skipped, passed, fixed, unapproved, rejected, approved
function reset_file_statuses {
    echo "${FUNCNAME[0]}():"

    # Allow developers to reset the file statuses of the files_to_process
    if [[ "${TRAIN[task]:?}" != "reset" &&
        "${TRAIN[task]:?}" != "retry" ]]; then
        return 0
    fi

    if [[ -z ${TRAIN[mariadb_connect]+x} ]]; then
        echo "  TRAIN[mariadb_connect] not set."
        return 1
    fi

    if [[ -z "${TRAIN[file_status_order]:-}" ]]; then
        echo "  Set file_status_order in configure_file_orders()"
        return 1
    fi

    # Reset the failed statuses of files so they can be processed again
    local statuses_to_reset="'fixing', 'failed', 'skipped', 'rejected'"

    if [[ "${TRAIN[task]:?}" == "reset" ]]; then
        local statuses_to_reset="${TRAIN[file_status_order]}"
    fi

    # Delete all the logs
    echo "  Deleting the log files in ${TRAIN[app_logs_dir]} . . ."
    rm -f "${TRAIN[app_logs_dir]}"/*.log

    reset_query="UPDATE file_lists SET 
                 file_scans = 0, 
                 file_status = 'new' 
                 WHERE file_status IN($statuses_to_reset)"

    error="$(${TRAIN[mariadb_connect]:?} -s -e "${reset_query}" 2>&1 > /dev/null)"

    if [[ -n ${error} ]]; then
        echo " ${error}"
        exit 1
    fi

    echo "  $statuses_to_reset updated to 'new'"
}
