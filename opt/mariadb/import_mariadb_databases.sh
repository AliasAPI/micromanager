#!/bin/bash

# todo:: Consider using https://github.com/quickshiftin/mysqlbkup
function import_mariadb_databases {
    echo "${FUNCNAME[0]}():"

    if [[ "${TRAIN[task]:?}" != "import" ]]; then
        return 0
    fi

    # mysql-restore:
    # 	@docker exec -i $(shell docker-compose ps -q mariadb) mysql -u"$(MARIADB_ROOT_USER)" -p"$(MARIADB_ROOT_PASSWORD)" < $(MARIADB_DUMPS_DIR)/db.sql 2>/dev/null

    if [ ! -e "${TRAIN[db_dumps_dir]:?}" ]; then
        echo "  Error: No dumps exist"
        exit 1
    fi

    # find must recent dump
    dump_file="$(find "${TRAIN[db_dumps_dir]:?}" -name "*.gpg" | sort -rn | head -1)"

    if [ -z "${dump_file}" ]; then
        echo "  Error: No dump found"
        exit 1
    fi

    dump_file=${dump_file/.xz*/}

    # decrypt the file
    gpg --output "${dump_file}.xz" -d "${dump_file}.xz.gpg"

    xz -d "${dump_file}.xz"

    # Import
    if [[ -z "${TRAIN[mariadb_root_password]:?}" ]]; then
        mysql -u "${TRAIN[mariadb_root_user]:?}" < "${dump_file}" 2> /dev/null
    fi

    if [[ -n "${TRAIN[mariadb_root_password]:?}" ]]; then
        if ! mysql -u "${TRAIN[mariadb_root_user]:?}" -p "${TRAIN[mariadb_root_password]:?}" < "${dump_file}" 2> /dev/null; then
            echo "  MySQL import failed"
            exit 1
        fi
    fi

    rm "${dump_file}"
}
