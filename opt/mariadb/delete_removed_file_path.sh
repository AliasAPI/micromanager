#!/bin/bash

# Delete the file_path from file_lists if it has been deleted from the file system
function delete_removed_file_path {
    echo "${FUNCNAME[0]}():"

    if [[ "${TRAIN[task]:?}" != "fix" &&
        "${TRAIN[task]:?}" != "scan" &&
        "${TRAIN[task]:0:4}" != "run_" ]] \
        || [[ -z "${TRAIN[file_path]:-}" ]] \
        || [[ -z "${TRAIN[mariadb_connect]:-}" ]]; then
        return 0
    fi

    # If the file still exists, do not delete it from file_lists
    if [ -f "${TRAIN[file_path]:?}" ]; then
        return 0
    fi

    echo "  ${TRAIN[file_path]:?} does not exist on the file system"
    echo "  Deleting it from the ${TRAIN[mariadb_database]:?} file_lists table"

    # Use a prepared statement to delete the file from the file_lists table
    query="PREPARE statement FROM 
          'DELETE FROM file_lists
           WHERE file_path = ?';

           SET @file_path = '${TRAIN[file_path]:?}';

           EXECUTE statement USING @file_path; "

    # Check if there is a MySQL flaw in the query result
    if ! result=$(${TRAIN[mariadb_connect]:?} -s -e "${query}" 2>&1); then
        echo "  ${result}"
        echo "  Query: ${query}"
        exit 1
    fi

    if grep -q "${TRAIN[file_path]:?}" "${TRAIN[files_to_process]}"; then
        # Escape special characters in the file_path for sed
        escaped_file_path=$(printf "%s\n" "${TRAIN[file_path]:?}" | sed 's/[\&/]/\\&/g')

        sed -i "\|$escaped_file_path|d" "${TRAIN[files_to_process]}"

        echo "  Removed ${TRAIN[file_path]:?} from files_to_process.txt"
    fi

    # Unset the file keys to prevent further processing
    echo "  Deleted the file_path from TRAIN to prevent further processing"
    unset 'TRAIN[file_name]'
    unset 'TRAIN[file_path]'
    unset 'TRAIN[file_type]'
    unset 'TRAIN[file_scans]'
    unset 'TRAIN[file_md5sum]'
    unset 'TRAIN[file_status]'
}
