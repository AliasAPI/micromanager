#!/bin/bash

function select_file_flaws {
    echo "${FUNCNAME[0]}():"

    if [[ "${TRAIN[task]:?}" != "fix" &&
        "${TRAIN[task]:?}" != "scan" &&
        "${TRAIN[task]:0:4}" != "run_" ]] \
        || [[ -z "${TRAIN[file_path]:-}" ]] \
        || [[ -z "${TRAIN[mariadb_connect]:-}" ]]; then
        return 0
    fi

    # Set the limit to 5 if if is not set
    FILE_FLAW_LIMIT=${FILE_FLAW_LIMIT:-5}

    # Select all the unique flaws
    query="PREPARE statement FROM 
          'SELECT CONCAT(''line '', line) AS line, 
           CONCAT(''column '', columns) AS columns, 
           flaw AS flaw
           FROM file_flaws
           WHERE file_path = ?
           ORDER BY line,columns,flaw
           LIMIT $FILE_FLAW_LIMIT';

           SET @file_path = '${TRAIN[file_path]:?}';

           EXECUTE statement USING @file_path; "

    # Check if there is a MySQL flaw in the query result
    if ! result=$(${TRAIN[mariadb_connect]:?} -s -e "${query}" 2>&1); then
        echo "  ${result}"
        echo "  Query: ${query}"
        exit 1
    fi

    # Declare a variable to store the concatenated flaws
    local file_flaws=""

    # Loop through the result of the SELECT statement
    while IFS= read -r flaw; do
        # Add each flaw to the file_flaws string with line breaks
        file_flaws+="\n  ${flaw}"
    done <<< "${result}"

    # If the file_flaws string has messages, add it to the train
    if [[ ${#file_flaws} -ge 10 ]]; then
        set_train "file_flaws" "${file_flaws}"
        echo "  file_flaws added to train"
    fi
}
