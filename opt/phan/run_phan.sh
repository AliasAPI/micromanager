#!/bin/bash

# Run Phan to automatically check PHP files
# https://github.com/phan/phan
function run_phan {
    echo "${FUNCNAME[0]}():"

    # Install Phan
    # todo:: Figure out where Phan is installing and prevent it from reinstalling
    if [[ "${TRAIN[task]:?}" == "install" ]] \
        && [ ! -f "${TRAIN[bin_dir]:?}/phan" ]; then
        cd "${TRAIN[toolbox]:?}" || return 1

        composer require --dev --ignore-platform-req phan/phan
    fi

    # Configure Phan
    if [[ "${TRAIN[task]:?}" == "install" ||
        "${TRAIN[task]:?}" == "config" ]] \
        && [ -f "/opt/phan/config.php" ]; then
        # Use the default Micromanager Phan configuration
        phan_config_file="/opt/phan/config.php"
    fi

    # If there is a custom .phan/config.php in the app, use it
    if [[ -f "${TRAIN[git_app_path]:?}/.phan/config.php" ]]; then
        phan_config_file="${TRAIN[git_app_path]:?}/.phan/config.php"
    fi

    if [[ -z "${phan_config_file}" ]]; then
        echo "  The phan/config.php file is missing"
    fi

    PHAN_ALLOW_XDEBUG=1
    export PHAN_ALLOW_XDEBUG

    if [[ "${TRAIN[manage_file]:?}" != "yes" &&
        "${TRAIN[task]:?}" != "${FUNCNAME[0]}" ]]; then
        return 0
    fi

    # todo:: Get Phan to work (while processing individual files)
    # Note: Currently running phan returns the following error:
    # Error: PHPUnitAssertionPlugin failed to find class PHPUnit\Framework\Assert, giving up
    # (set environment variable PHAN_PHPUNIT_ASSERTION_PLUGIN_QUIET=1 to ignore this)
    # However, do not use PHAN_PHPUNIT_ASSERTION_PLUGIN_QUIET=1
    # Micromanager will halt with a blank error
    # /tools/vendor/phpunit/phpunit/src/Framework/Assert.php exists

    command="php ${TRAIN[bin_dir]:?}/phan "
    command+="--output-mode=json --no-progress-bar --config-file=${phan_config_file} "
    command+="${TRAIN[file_path]:?} "

    # Run Phan
    if [[ "${TRAIN[file_type]:?}" != "php" ]]; then
        echo "  Skipping [${TRAIN[file_type]:?}] file"
        return 0
    fi

    set_file_scanned

    cd "${TRAIN[toolbox]:?}" || return 1

    error=$(${command} 2>&1 > "${TRAIN[file_log_path]:?}")

    exit_code=$?

    if [[ "${exit_code}" -gt 1 ]]; then
        file_flaws="{\"file_path\":\"${TRAIN[file_path]:?}\",\"line\":0,\"column\":0,"
        file_flaws+="\"flaw\":\"${error} Exit code: ${exit_code}\"}"
        insert_file_flaws "${file_flaws}" "phan"
    fi

    # Reduce Phan's output message to just the location of the error and the warning message
    file_flaws=$(jq -c '.[] | {file_path: .location.path, line: .location.lines.begin, column: 0, flaw: .description}' "${TRAIN[file_log_path]}")

    insert_file_flaws "${file_flaws}" "phan"
}
