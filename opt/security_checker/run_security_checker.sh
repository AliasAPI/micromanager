#!/bin/bash

# Run Security Checker by Enlighten with the Security Advisories Database
# https://github.com/enlightn/security-checker
function run_security_checker {
    echo "${FUNCNAME[0]}():"

    # Install Enlightn Security Checker
    if [[ "${TRAIN[task]:?}" == "install" ]] \
        && [ ! -f "${TRAIN[bin_dir]:?}/security-checker" ]; then
        cd "${TRAIN[toolbox]:?}" || return 1

        composer require --dev enlightn/security-checker
    fi

    if [[ "${TRAIN[manage_file]:?}" != "yes" &&
        "${TRAIN[task]:?}" != "${FUNCNAME[0]}" ]]; then
        return 0
    fi

    if [[ "${TRAIN[file_name]:?}" != "composer.lock" ]]; then
        echo "  Skipping file that is not composer.lock"
        return 0
    fi

    # Configure Enlightn Security Checker
    # todo:: Add the Run section to etc/security_checker/
    # todo:: Add the most strict configuration file in etc/security_checker/run_security_checker.sh
    # todo:: Copy the configuration file to the correct location
    command="php ${TRAIN[bin_dir]:?}/security-checker security:check ${TRAIN[file_path]:?} --no-dev --format=json "

    # Run Security Checker
    set_file_scanned

    error=$(${command} 2>&1 > "${TRAIN[file_log_path]:?}")

    exit_code=$?

    if [[ "${exit_code}" -gt 1 ]]; then
        file_flaws="{\"file_path\":\"${TRAIN[file_path]:?}\",\"line\":0,\"column\":0,"
        file_flaws+="\"flaw\":\"${error} Exit code: ${exit_code}\"}"
        insert_file_flaws "${file_flaws}" "security_checker"
    fi
}
