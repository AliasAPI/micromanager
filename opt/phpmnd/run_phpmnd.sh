#!/bin/bash

# Run PHP Magic Number Detector (PHPMND) to detect magic numbers
# https://github.com/povils/phpmnd
# https://github.com/phpro/grumphp/blob/master/doc/tasks/phpmnd.md
# Pro-tip:
# Sometimes magic numbers are required.
# For example implementing a known mathematical formula,
# by default intval(), floatval() and strval() mark a number as NOT magic.
# $percent  = $number / 100; would show 100 as a magic number
# $percent = $number / intval(100); would mark 100 as not magic.
function run_phpmnd {
    echo "${FUNCNAME[0]}():"

    # Install PHPMND
    if [[ "${TRAIN[task]:?}" == "install" ]] \
        && [ ! -f "${TRAIN[bin_dir]:?}/phpmnd" ]; then
        cd "${TRAIN[toolbox]:?}" || return 1

        composer require --dev povils/phpmnd --ignore-platform-reqs --with-all-dependencies
    fi

    # Configure PHPMND
    if [[ "${TRAIN[task]:?}" == "install" ||
        "${TRAIN[task]:?}" == "config" ]]; then
        cd "${TRAIN[toolbox]:?}" || return 1

        sed -i "s!\`tput cols\`!\`tput cols 2>/dev/null\`!" vendor/povils/phpmnd/src/Printer/Console.php
    fi

    if [[ "${TRAIN[manage_file]:?}" != "yes" &&
        "${TRAIN[task]:?}" != "${FUNCNAME[0]}" ]]; then
        return 0
    fi

    command="php ${TRAIN[bin_dir]:?}/phpmnd ${TRAIN[file_path]:?} --include-numeric-string "
    command+="--ignore-funcs=round,sleep --hint  --xml-output=${TRAIN[file_log_path]:?} "

    # Run PHPMND
    if [[ "${TRAIN[file_type]:?}" != "php" ]]; then
        echo "  Skipping [${TRAIN[file_type]:?}] file"
        return 0
    fi

    set_file_scanned

    cd "${TRAIN[toolbox]:?}" || return 1

    set +e
    error=$(${command} 2> "${TRAIN[file_log_path]:?}")
    exit_code="$?"
    set -e

    # Save stdout to file descriptor 3
    # exec 3>&1
    # Redirect stdout to file and stderr to file and tee
    # error=$(${command} > "${TRAIN[file_log_path]}" 2> >(tee -a "${standard_error_log}" >&2) | tee /dev/fd/3)
    # exec 3>&-
    # Close file descriptor 3

    if [[ "${exit_code}" -gt 2 ]]; then
        flaw=$(clean_flaw "${error} Exit code ${exit_code}")
        file_flaws="{\"file_path\":\"${TRAIN[file_path]:?}\",\"line\":0,\"column\":0,\"flaw\":\"${flaw}\"}"

        insert_file_flaws "${file_flaws}" "phpmnd"
        return 0
    fi

    json_parameters="{\"file_path\":\"${TRAIN[file_log_path]:?}\"}"

    # todo:: Consider converting the XML output to JSON using the xq utility
    set +e
    file_flaws=$(php "${TRAIN[opt_dir]:?}/phpmnd/parse_phpmnd_xml.php" "$json_parameters")
    set -e

    insert_file_flaws "${file_flaws}" "phpmnd"
}
