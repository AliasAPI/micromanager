<?php

declare(strict_types = 1);


function parse_phpmnd_xml($json_parameters) : void
{
    $file_flaws = '';

    try {
        $params = json_decode((string) $json_parameters, true, 512, \JSON_THROW_ON_ERROR);

        if (\is_array($params)
            && \array_key_exists('file_path', $json_parameters) === false) {
            $file_flaws = '{"file_path":"/opt/micromanager/phpmnd/run_phpmnd.sh","line":0,"column":0,';
            $file_flaws .= '"flaw":"The file_path parameter is not set"}';
            echo $file_flaws;

            return;
        }

        $output = file_get_contents($json_parameters['file_path']);

        // Remove the unnecessary output from php_assumptions
        $output = str_replace('PHPAssumptions analyser v0.8.0 by @rskuipers', '', $output);

        if (mb_strlen($output) < (int) 10) {
            $file_flaws = '{"file_path":"/opt/micromanager/phpmnd/run_phpmnd.sh","line":0,"column":0,';
            $file_flaws .= '"flaw":"PHPmnd has no output"}';
            echo $file_flaws;

            return;
        }

        $xml = simplexml_load_string($output, 'SimpleXMLElement', \LIBXML_NOCDATA | \LIBXML_ERR_FATAL | \LIBXML_ERR_WARNING);

        if (mb_strlen($xml) > (int) 5) {
            $array = json_decode(json_encode($xml), true);
        }

        foreach ($array['files']['file'] as $file) {
            $file_path = (string) $file['path'];

            foreach ($file['entry'] as $entry) {
                $line = (int) $entry['line'];
                $column = (int) $entry['start'];
                $flaw = trim((string) $entry->snippet);

                $file_flaws .= "{\"file_path\":\"{$file_path}\",";
                $file_flaws .= "\"line\":{$line}, \"column\":{$column},";
                $file_flaws .= "\"flaw\":\"Replace the magic number: {$flaw}\"}\n";
            }
        }
    } catch (Exception $exception) {
        $error_message = $exception->getMessage();
        $file_flaws = '{"file_path":"parse_phpmnd_xml.php","line":0,"column":0,';
        $file_flaws .= "\"flaw\":\"{$error_message}\"}";
        echo $file_flaws;
    }

    echo $file_flaws;
}

// Pass the JSON-encoded array from the run_phpmnd.sh bash script into parse_phpmnd_xml()
$params_json = $argv[1];

// Call the function with the JSON-encoded parameter
parse_phpmnd_xml($params_json);
