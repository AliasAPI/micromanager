#!/bin/bash

function run_nvidia {
    echo "${FUNCNAME[0]}():"

    # Install Nvidia Container Toolkit
    if [[ "${TRAIN[task]:?}" == "install"
        || "${TRAIN[task]:?}" == "config" ]] \
        && [[ "${TRAIN[dockerized]:?}" == "no" ]]; then
        # todo:: Detect Nvidia GPU here?
        gpgkey="https://nvidia.github.io/libnvidia-container/gpgkey"
        keyring="/usr/share/keyrings/nvidia-container-toolkit-keyring.gpg"
        toolkit="https://nvidia.github.io/libnvidia-container/stable/deb/nvidia-container-toolkit.list"

        curl -fsSL "$gpgkey" | sudo gpg --yes --dearmor -o "$keyring"

        curl -s -L "$toolkit" \
            | sed 's#deb https://#deb [signed-by='"$keyring"'] https://#g' \
            | sudo tee /etc/apt/sources.list.d/nvidia-container-toolkit.list

        sudo apt-get install -y nvidia-container-toolkit
    fi

    # Configure Nvidia Container Toolkit
    if [[ "${TRAIN[task]:?}" == "install" 
        || "${TRAIN[task]:?}" == "config" ]]; then
        sudo nvidia-ctk runtime configure --runtime=docker

        # todo:: Elrod, is this restart necessary?
        sudo service docker restart
    fi

    # Run Nvidia
    # Note: This section is empty in the original code
}
