#!/bin/bash

# Run ShellCheck to automatically check (MicroManager) bash scripts
# https://github.com/koalaman/shellcheck
# https://www.shellcheck.net is an online checker
function run_shellcheck {
    echo "${FUNCNAME[0]}():"

    # Install Shellcheck
    if [[ "${TRAIN[task]:?}" == "install" ]] \
        && [[ "${TRAIN[dockerized]:?}" == "no" ]]; then
        export DEBIAN_FRONTEND=noninteractive

        apt-get install -y shellcheck
    fi

    # Configure Shellcheck
    # https://www.shellcheck.net/wiki List of error codes
    if [[ "${TRAIN[task]:?}" == "install" ||
        "${TRAIN[task]:?}" == "config" ]] \
        && [[ -f "/opt/shellcheck/.shellcheckrc" ]]; then
        cp "/opt/shellcheck/.shellcheckrc" "${TRAIN[toolbox]:?}"
    fi

    if [[ "${TRAIN[manage_file]:?}" != "yes" &&
        "${TRAIN[task]:?}" != "run_shellcheck" ]]; then
        return 0
    fi

    command="shellcheck --format=json ${TRAIN[file_path]:?} "

    # Run Shellcheck
    if [[ "${TRAIN[file_type]:?}" != "sh" ]]; then
        echo "  Skipping [${TRAIN[file_type]:?}] file"
        return 0
    fi

    set_file_scanned

    set +e
    error=$(${command} 2>&1 > "${TRAIN[file_log_path]:?}")
    # ShellCheck exit codes; 0 and 1 are OK
    # https://github.com/koalaman/shellcheck/blob/master/shellcheck.1.md
    exit_code=$?
    set -e

    # Process Shellcheck output
    # If the exit_code greater than 0 and 1, exit
    if [[ "${exit_code}" -gt 1 ]]; then
        file_flaws="{\"file_path\":\"${TRAIN[file_path]:?}\",\"line\":0,\"column\":0,"
        file_flaws+="\"flaw\":\"${error} Exit code: ${exit_code}\"}"
        insert_file_flaws "${file_flaws}" "shellcheck"
    fi

    # Parse the output file to simplify the error messages
    file_flaws=$(jq -c '.[] | {file_path: .file, line: .line, column: .column, flaw: .message}' "${TRAIN[file_log_path]:?}")

    # Add each flaw to the file_flaws database table and the library that provided the message
    insert_file_flaws "${file_flaws}" "shellcheck"
}
