#!/bin/bash

# Opcache saves precompiled script bytecode in a server's cache to increase performance
# https://www.ibexa.co/blog/how-much-of-a-performance-boost-can-you-expect-for-a-symfony-5-app-with-php-opcache-preloading
function configure_opcache {
    echo "${FUNCNAME[0]}():"

    # Check if the task is one of the expected values
    if [[ "${TRAIN[task]:?}" != "install" &&
        "${TRAIN[task]:?}" != "produce" &&
        "${TRAIN[task]:?}" != "deploy" ]]; then
        return 0
    fi

    # Check if MM_DIR/app directory exists
    if [[ ! -d "${MM_DIR}${GIT_APP_PATH}" ]]; then
        echo "Error: Directory [${MM_DIR}] ${MM_DIR}${GIT_APP_PATH} not found."
        return 1
    fi

    # Count PHP files
    php_files=$(find "${MM_DIR}${GIT_APP_PATH}" -type f -name '*.php')

    if [[ -z "${php_files}" ]]; then
        echo "  [0] PHP files found. Skipping opcache configuration."
        return 0
    fi

    php_count=$(echo "${php_files}" | grep -c '\.php')

    # Check if opcache.ini file exists
    opcache_ini="${MM_DIR}/opt/opcache/opcache.ini"

    if [[ ! -f "${opcache_ini}" ]]; then
        echo "Error: File '$opcache_ini' not found."
        return 1
    fi

    echo "  [$php_count] PHP files found"

    # Update opcache.max_accelerated_files in opcache.ini
    sed -i "s/opcache.max_accelerated_files=.*/opcache.max_accelerated_files=${php_count}/" "$opcache_ini"
}
