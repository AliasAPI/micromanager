#!/bin/bash

# phpDocumentor
# todo:: Finish this run_phpdocumentor function
# https://www.phpdoc.org/
function run_phpdocumentor {
    echo "${FUNCNAME[0]}():"

    # Delete phpDocumentor (to reduce the software that exists in production systems)
    if [[ "${TRAIN[task]:?}" == "delete" ||
        "${TRAIN[task]:?}" == "produce" ]] \
        && [ -f "${TRAIN[bin_dir]:?}/phpDocumentor" ]; then
        echo "Deleting phpDocumentor . . ."
        rm -f "${TRAIN[bin_dir]:?}/phpDocumentor"
    fi

    # Install phpDocumentor
    if [[ "${TRAIN[task]:?}" == "install" ]] \
        && [ ! -f "${TRAIN[bin_dir]:?}/phpDocumentor" ]; then
        echo "Downloading phpDocumentor . . ."
        curl -L https://phpdoc.org/phpDocumentor.phar > "${TRAIN[bin_dir]:?}/phpDocumentor"
    fi

    # Configure phpDocumentor
    if [[ "${TRAIN[task]:?}" == "install" ||
        "${TRAIN[task]:?}" == "config" ]]; then
        cp "${TRAIN[opt_dir]:?}/phpdocumentor/phpdoc.dist.xml" "${TRAIN[toolbox]:?}"
    fi

    # Run phpDocumentor
    if [[ "${TRAIN[task]:?}" != "document" &&
        "${TRAIN[task]:?}" != "phpdocumentor" ]]; then
        return 0
    fi

    # https://docs.phpdoc.org/3.0/guide/getting-started/generating-documentation.html
    php "${TRAIN[bin_dir]:?}/phpDocumentor -d ${TRAIN[git_app_path]:?}/src -t ${TRAIN[git_app_path]:?}/public/doc "

    # Reset owner
    chown -R "$(id -u)":"$(id -g)" "${TRAIN[git_app_path]:?}/public/doc"
}
