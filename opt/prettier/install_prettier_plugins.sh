#!/bin/bash

# https://prettier.io/docs/en/plugins
function install_prettier_plugins {
    echo "${FUNCNAME[0]}():"

    # Install Prettier plugins
    if [[ "${TRAIN[task]:-}" != "install" ]] \
        || [[ "${TRAIN[dockerized]:?}" != "no" ]] \
        || ! command -v prettier > /dev/null 2>&1; then
        return 0
    fi

    cd "${TRAIN[git_app_path]:?}" || return 1

    # https://github.com/un-ts/prettier/tree/master/packages/sh
    if [[ ! -d "${TRAIN[git_app_path]:?}/node_modules/prettier-plugin-sh" ]]; then
        npm i -D prettier-plugin-sh --loglevel verbose | while IFS= read -r line; do
            echo "  $line"
        done
    fi
}
