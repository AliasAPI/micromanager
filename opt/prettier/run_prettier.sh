#!/bin/bash

# https://prettier.io
function run_prettier {
    echo "${FUNCNAME[0]}():"

    # Install Prettier
    if [[ "${TRAIN[task]:-}" == "install" ]] \
        && [[ "${TRAIN[dockerized]:?}" == "no" ]] \
        && command -v npm > /dev/null 2>&1; then
        echo "  Installing Prettier . . ."

        export DEBIAN_FRONTEND=noninteractive

        # The Prettier package gets installed in /app
        cd "${TRAIN[git_app_path]:?}" || return 1

        npm config set fund false --global

        npm install --global prettier | while IFS= read -r line; do
            echo "  $line"
        done

        install_prettier_plugins

        return 0
    fi

    # Configure Prettier

    if [[ "${TRAIN[manage_file]:?}" != "yes" &&
        "${TRAIN[task]:?}" != "run_prettier" ]]; then
        return 0
    fi

    # Set the default operation to 'scan' (--check)
    fix_or_scan="--check "

    command="prettier ${TRAIN[file_path]:?} "

    # Overwrite the file using Prettier's format
    if [[ "${TRAIN[task]}" == "fix" ||
        "${TRAIN[task]}" == "${FUNCNAME[0]}" ]]; then
        fix_or_scan="--write "
    fi

    command+="$fix_or_scan "
    # Configure the .prettierrc location to improve performance
    command+="--config ${TRAIN[opt_dir]}/prettier/.prettierrc "
    # Config file take precedence over CLI options (so devs can configure)
    command+="--config-precedence file-override "
    # Ignore unknown files matched by patterns
    command+="--ignore-unknown "
    # Cache
    command+="--cache "
    command+="--cache-location=${TRAIN[app_data_files]:?}/.prettier.cache "
    # Enforce single attribute per line in HTML, Vue and JSX
    command+="--single-attribute-per-line "
    command+="--tab-width 4 "
    # https://prettier.io/docs/en/options#bracket-line
    command+="--bracket-same-line "
    # https://prettier.io/docs/en/options#arrow-function-parentheses
    command+="--arrow-parens always "
    # https://prettier.io/docs/en/options#file-path
    command+="--stdin-filepath ${TRAIN[file_name]:?} "
    # https://prettier.io/docs/en/options#html-whitespace-sensitivity
    command+="--html-whitespace-sensitivity strict "
    # https://prettier.io/docs/en/options#end-of-line
    command+="--end-of-line lf "

    # echo "command: $command" exit

    # Run Prettier
    set_file_scanned

    set +e
    # Run the command and capture stdout and stderr separately
    error_output=$({ ${command} 1> "${TRAIN[file_log_path]:?}"; } 2>&1)
    exit_code=$?
    set -e

    # Check if the exit code indicates an error
    if [ $exit_code -gt 0 ]; then
        # Clean and format the error message
        flaw=$(clean_flaw "${error_output} Exit code $exit_code")
        file_flaws="[{\"file_path\":\"${TRAIN[file_path]:?}\",\"line\":0,\"column\":0,\"flaw\":\"${flaw}\"}]"

        # Save the error as JSON in the log file
        echo "${file_flaws}" > "${TRAIN[app_logs_dir]}/prettier_error.log"

        insert_file_flaws "${file_flaws}" "prettier"
    fi
}
