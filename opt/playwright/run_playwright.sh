#!/bin/bash

function run_playwright {
    echo "${FUNCNAME[0]}():"

    # Install Playwright
    if [[ "${TRAIN[task]:-}" == "install" ]] \
        && [[ "${TRAIN[dockerized]:?}" == "no" ]] \
        && command -v pipx &> /dev/null; then

        pipx install playwright

        pipx ensurepath

        # playwright install --with-deps chromium
    fi

    # Configure Playwright

    # Run Playwright (Aider runs Playwright)
}
