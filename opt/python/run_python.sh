#!/bin/bash

set -e

function run_python {
    echo "${FUNCNAME[0]}():"

    # Install Python during the custom Docker container build
    if [[ "${TRAIN[task]:?}" == "install" ]] && [[ "${TRAIN[dockerized]:?}" == "no" ]]; then
        echo "  Installing Python3 (full), pip, and pipx . . ."

        export DEBIAN_FRONTEND=noninteractive

        apt-get update -q

        # Install full Python3 package, pip, and pipx
        apt-get install -yq python3-full python3-pip pipx 2>&1 | while IFS= read -r line; do
            echo "  $line"
        done

        # Ensure pipx is added to the PATH
        python3 -m pipx ensurepath

        export PATH="$HOME/.local/bin:$PATH"

        # Verify Python installation
        installed_python=$(python3 --version 2>/dev/null || echo "Not Installed")

        if [[ "$installed_python" == "Not Installed" ]]; then
            echo "  Error: Python3 installation failed."
            return 1
        fi

        echo "  Installed Python version: $installed_python"

        # Final check
        if ! command -v python3 &>/dev/null; then
            echo "  Error: Python3 installation failed."
            return 1
        fi

        echo "  Python installation completed successfully."
    fi
}
