#!/bin/bash

function display_file_flaws {
    echo "${FUNCNAME[0]}():"

    if [[ "${TRAIN[force_flaw_fix]:?}" != true ]]; then
        echo "  Set FORCE_FLAW_FIX=true to display flaws"
        return 0
    fi

    if [[ "${TRAIN[task]:?}" != "fix" &&
        "${TRAIN[task]:?}" != "scan" &&
        "${TRAIN[task]:0:4}" != "run_" ]]; then
        return 0
    fi

    decor=""
    RESET='\033[0m'

    # todo:: Add code that says there are no file_flaws (and display PASS)
    # Green
    COLOR='\033[0;32m'
    ASCII_FILE="${TRAIN[opt_dir]:?}/debug/pass_ascii.txt"

    # If the log files contain errors, fail!
    if [ -n "${TRAIN[file_flaws]}" ]; then
        # Red
        COLOR='\033[0;31m'
        ASCII_FILE="${TRAIN[opt_dir]:?}/debug/fail_ascii.txt"
    fi

    if [ -n "${ASCII_FILE}" ]; then
        mapfile -t ASCII < "${ASCII_FILE}"

        decor="\n$(printf "%s\n" "${ASCII[@]}")\n"
    fi

    # Clear the console screen without installing clear

    # Show the message
    echo -e "$COLOR"
    echo -e "  $decor"

    echo -e "$RESET"

    if [[ -n "${TRAIN[file_path]}" &&
        -n "${TRAIN[file_flaws]}" ]]; then
        echo -e "  file_path: ${TRAIN[file_path]:?}"
        echo -e "   ${TRAIN[file_flaws]:-}"
    fi

    echo ""
}
