#!/bin/bash

# display_elapsed_time helps benchmark MicroManager
function display_elapsed_time {
    if [[ "${TRAIN[loops_remaining]}" -gt 0 ]]; then
        return 0
    fi

    echo "${FUNCNAME[0]}():"

    # Calculate elapsed time
    time_end=$(date +%s)

    time_elapsed=$((time_end - ${TRAIN[time_start]:?}))

    # Calculate days, hours, minutes, and seconds
    days=$((time_elapsed / 86400))
    hours=$(((time_elapsed % 86400) / 3600))
    minutes=$(((time_elapsed % 3600) / 60))
    seconds=$((time_elapsed % 60))

    # Display the elapsed time
    formatted_time="Elapsed time: "

    if [[ $days -gt 0 ]]; then
        formatted_time+="$days days, "
    fi

    if [[ $hours -gt 0 ]]; then
        formatted_time+="$hours hours, "
    fi

    if [[ $minutes -gt 0 ]]; then
        formatted_time+="$minutes minutes, "
    fi

    formatted_time+="$seconds seconds"

    set_train "elapsed_time" "$formatted_time"

    echo "  ${TRAIN[elapsed_time]}"
}
