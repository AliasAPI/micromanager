#!/bin/bash

function delete_log_files {
    # Delete the logs on each run during development
    echo "${FUNCNAME[0]}():"

    # If the logs_dir does NOT exist on the host machine, leave this function
    if [ ! -d "${TRAIN[app_logs_dir]:?}" ]; then
        return 0
    fi

    if [[ "${TRAIN[task]:?}" == "delete" ||
        "${TRAIN[task]:?}" == "reset" ]]; then
        # Note: delete_log_files runs on the host machine so it uses the logs_dir path
        rm -f "${TRAIN[app_logs_dir]:?}/*.log"

        echo "  All ${TRAIN[app_logs_dir]:?}/*.log files deleted"
    fi

    if [[ "${TRAIN[task]:?}" == "fix" ||
        "${TRAIN[task]:?}" == "scan" ||
        "${TRAIN[task]:0:4}" == "run_" ]]; then
        # Delete empty log files to help highlight the files have output flaw messages
        find "${TRAIN[app_logs_dir]:?}" -maxdepth 1 -type f -name "[!.]*" -size 0c -delete

        echo "  Empty ${TRAIN[app_logs_dir]:?} log files deleted"
    fi
}
