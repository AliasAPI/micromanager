#!/bin/bash

# Run Composer Require Checker to verify that no unknown symbols are used in packages
# https://github.com/maglnet/ComposerRequireChecker
function run_composer_require_checker {
    echo "${FUNCNAME[0]}():"

    # Install Composer Require Checker
    if [[ "${TRAIN[task]:?}" == "install" ]] \
        && [ ! -f "${TRAIN[bin_dir]:?}/composer-require-checker" ]; then
        cd "${TRAIN[toolbox]:?}" || return 1

        composer require --dev maglnet/composer-require-checker
    fi

    # Configure Composer Require Checker
    if [[ "${TRAIN[task]:?}" == "install" ||
        "${TRAIN[task]:?}" == "config" ]] \
        && [ ! -f "${TRAIN[toolbox]:?}/composer-require-checker.json" ]; then
        cd "${TRAIN[opt_dir]}/composer_require_checker/" || return 1
        cp ./composer-require-checker.json "${TRAIN[toolbox]:?}/composer-require-checker.json"
    fi

    if [[ "${TRAIN[manage_file]:?}" != "yes" &&
        "${TRAIN[task]:?}" != "${FUNCNAME[0]}" ]]; then
        return 0
    fi

    command="php ${TRAIN[bin_dir]:?}/composer-require-checker  "
    command+="--config-file=${TRAIN[toolbox]:?}/composer-require-checker.json "
    command+="--output=json check ${TRAIN[file_path]:?}"

    # Run Composer Require Checker
    if [[ "${TRAIN[file_name]:?}" != "composer.json" ]]; then
        echo "  Skipping [${TRAIN[file_type]:?}] file"
        return 0
    fi

    set_file_scanned

    cd "${TRAIN[git_app_path]:?}" || return 1

    # It is recommended to run ComposerRequireChecker without Xdebug.

    export XDEBUG_MODE=off

    error=$(${command} 2>&1 > "${TRAIN[file_log_path]:?}")

    exit_code=$?

    if [[ "${exit_code}" -gt 1 ]]; then
        file_flaws="{\"file_path\":\"${TRAIN[file_path]:?}\",\"line\":0,\"column\":0,"
        file_flaws+="\"flaw\":\"${error} Exit code: ${exit_code}\"}"
        insert_file_flaws "${file_flaws}" "composer_require_checker"
    fi

    export XDEBUG_MODE="${TRAIN[xdebug_mode]:?}"

    # todo:: Loop through the file_log and store the unknown-symbols errors in the file_flaws table
    # composer-require-checker does not seem to indicate the full file_path of the file with the flaw.
    # One solution is to copy each file to a temporary directory along with the composer.json and run composer-require-checker
    # Another solution is to loop through all the files looking for the dependencies and adding each file_path to file_flaws
    # Another solution is to run a different tool that will replace the namespaces in the code so that composer-require-checker is happy
    # The goal is to get file_flaws for specific files into the file_flaws table
}
