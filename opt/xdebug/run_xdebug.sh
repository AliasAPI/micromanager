#!/bin/bash

# Run XDebug to debug PHP applications step by step
# https://github.com/xdebug/xdebug
function run_xdebug {
    echo "${FUNCNAME[0]}():"

    installing_xdebug=false

    # Install Xdebug
    # todo:: Move "pecl install xdebug" from docker/php-fpm/Docker.template to here
    # https://xdebug.org/docs/install#linux
    # xdebug is installed when creating the Docker container
    if [[ "${TRAIN[task]:?}" == "install" ]] \
        && [[ "${TRAIN[dockerized]:?}" == "no" ]]; then
        installing_xdebug=true
    fi

    # Create the .xdebug output directory if it does not exist
    if [[ "${installing_xdebug}" == true &&
        ! -d "${TRAIN[app_data_files]:?}/.xdebug" ]]; then

        mkdir -p "${TRAIN[app_data_files]:?}/.xdebug"
    fi

    if [[ "${installing_xdebug}" == true &&
        ! -f "${TRAIN[opt_dir]}/xdebug/xdebug.ini" ]]; then
        echo "  The xdebug.ini configuration file is missing"
        exit 1
    fi

    # Check to see if Xdebug is installed
    if [[ "${installing_xdebug}" == true &&
        ! $(php -m | /bin/grep 'xdebug') ]]; then
        pecl install xdebug
        return 0
    fi

    # Configure Xdebug
    # todo:: Fix these XDEBUG settings so that they can be configured elsewhere
    # Xdebug (on enabled, off disabled)
    # XDEBUG="on"

    set_train "xdebug" "on"

    if [[ "${TRAIN[xdebug]:?}" == 'on' ]]; then
        # Turn on all of Xdebug's core features
        # Step debugging is disabled by default
        # because it relies on an IDE and human
        set_train "xdebug_mode" "coverage,develop,gcstats,profile,trace"

        # Make sure xdebug_mode is set and export it
        export XDEBUG_MODE="${TRAIN[xdebug_mode]:?}"
    fi

    # Feature request
    # todo:: Test turning on and off xdebug
    # Some of the Micromanager tools do not want xdebug running.
    # We should set TRAIN[xdebug]=on and off and then call run_xdebug (or toggle_xdebug)

    # Run XDebug
    # todo:: Add the Run section to etc/xdebug/run_xdebug.sh
    # todo:: Test run_xdebug.sh
    if [[ "${TRAIN[task]:?}" != "fix" &&
        "${TRAIN[task]:?}" != "scan" &&
        "${TRAIN[task]:?}" != "run_xdebug" ]]; then
        return 0
    fi

    # xdebug_info() is run in PHP
}
