#!/bin/bash

# Run Infection to check code for PHP mutations (based on Abstract Syntax Tree).
# https://github.com/infection/infection
function run_infection {
    echo "${FUNCNAME[0]}():"

    # Install Infection
    if [[ "${TRAIN[task]:?}" == "install" ]] \
        && [ ! -f "${TRAIN[bin_dir]:?}/infection" ]; then
        cd "${TRAIN[toolbox]:?}" || return 1

        composer config --no-plugins allow-plugins.infection/extension-installer true

        composer require --dev infection/infection
    fi

    # Configure Infection
    # todo:: Review https://infection.github.io/guide/usage.html
    # todo:: Review https://infection.github.io/guide/usage.html#Configuration-settings
    # todo:: Add the most strict configuration file in etc/infection/run_infection.sh
    if [[ "${TRAIN[task]:?}" == "install" ||
        "${TRAIN[task]:?}" == "config" ]]; then
        cp "${TRAIN[opt_dir]:?}/infection/infection.dist.json" "${TRAIN[toolbox]:?}/infection.json"
    fi

    # command="php ${TRAIN[task]:?}/infection --configuration=${TRAIN[toolbox]}/infection.json ${TRAIN[file_path]:?}"

    # Run Infection
    if [[ "${TRAIN[file_type]:?}" != "php" ]]; then
        echo "  Skipping [${TRAIN[file_type]:?}] file"
        return 0
    fi

    if [[ "${TRAIN[task]:?}" != "fix" &&
        "${TRAIN[task]:?}" != "scan" &&
        "${TRAIN[task]:?}" != "run_infection" ]]; then
        return 0
    fi

    set_file_scanned
}
