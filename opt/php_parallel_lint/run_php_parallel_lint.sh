#!/bin/bash

# Run PHP-Parallel-Lint to automatically check and beautify PHP files
# https://github.com/php-parallel-lint/PHP-Parallel-Lint
# https://packagist.org/packages/php-parallel-lint/php-parallel-lint
function run_php_parallel_lint {
    echo "${FUNCNAME[0]}():"

    # Install PHP-Parallel-Lint
    if [[ "${TRAIN[task]:?}" == "install" ]] \
        && [ ! -f "${TRAIN[bin_dir]:?}/parallel-lint" ]; then
        cd "${TRAIN[toolbox]:?}" || return 1

        composer require --dev php-parallel-lint/php-parallel-lint

        composer require --dev php-parallel-lint/php-console-highlighter
    fi

    if [[ "${TRAIN[manage_file]:?}" != "yes" &&
        "${TRAIN[task]:?}" != "${FUNCNAME[0]}" ]]; then
        return 0
    fi

    # Configure PHP-Parallel-Lint
    # todo:: Get the most strict configuration for php-parallel-lint
    command="php ${TRAIN[bin_dir]:?}/parallel-lint --json --show-deprecated --no-progress "
    command+="--show-deprecated ${TRAIN[file_path]:?}"

    # Run PHP-Parallel-Lint
    if [[ "${TRAIN[file_type]:?}" != "php" ]]; then
        echo "  Skipping [${TRAIN[file_type]:?}] file"
        return 0
    fi

    # todo:: Add scan (dry run) functionality
    cd "${TRAIN[git_app_path]:?}" || return 1

    set_file_scanned

    set +e
    error=$(${command} 2>&1 > "${TRAIN[file_log_path]:?}")
    exit_code=$?
    set -e

    if [[ "${exit_code}" -gt 1 ]]; then
        file_flaws="{\"file_path\":\"${TRAIN[file_path]:?}\",\"line\":0,\"column\":0,"
        file_flaws+="\"flaw\":\"${error} Exit code: ${exit_code}\"}"
        insert_file_flaws "${file_flaws}" "php_parallel_lint"
    fi

    file_flaws=$(jq -c '.results.errors[] | {file_path: .file, line: .line, column: 0, flaw: .normalizeMessage}' "${TRAIN[file_log_path]:?}")

    insert_file_flaws "${file_flaws}" "php_parallel_lint"
}
