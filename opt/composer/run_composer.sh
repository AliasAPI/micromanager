#!/bin/bash

# Install Composer in the /app and /review directories
# https://github.com/composer/composer
function run_composer {
    echo "${FUNCNAME[0]}():"

    # Install Composer
    if [[ "${TRAIN[task]:?}" == "install" ]]; then
        echo "  Running Composer install for the app . . ."

        export COMPOSER_CACHE_DIR="/data/files/.composer/cache"
        export COMPOSER_FUND=0
        export COMPOSER_HOME="/data/files/.composer"
        export COMPOSER_NO_INTERACTION=1
        export COMPOSER_PREFER_STABLE=1

        cd "${TRAIN[git_app_path]:?}" || return 1

        # todo:: Write logic that makes sure Composer is needed for a PHP app
        if [ ! -f composer.json ]; then
            # composer init -n --name "${TRAIN[git_app_path]:?}"
            return 0
        fi

        # Composer segmentation fault caused by xdebug
        # https://github.com/composer/composer/issues/11740
        # I have released Xdebug 3.3.1, in which I believe this is now all fixed.
        # https://bugs.xdebug.org/view.php?id=2221#c6700
        composer install | while IFS= read -r line; do
            echo "  $line"
        done

        # The composer.lock file is created anyway, so we simply create it to avoid error code 9
        if [ -f "${TRAIN[git_app_path]:?}/composer.lock" ]; then
            touch "${TRAIN[git_app_path]:?}/composer.lock"
        fi
    fi

    # Configure Composer
    # Configure the unique name of the service (without spaces)
    export COMPOSE_PROJECT_NAME="MicroManager${TRAIN[mm_id]:?}"
    echo " Compose Project Name: ${COMPOSE_PROJECT_NAME}"

    # Run Composer preparing a production server
    if [[ "${TRAIN[task]:?}" == "produce" ]] \
        && [ -f "${TRAIN[git_app_path]:?}/composer.json" ]; then
        echo ""
        echo "  Running Composer update for the app . . ."

        cd "${TRAIN[git_app_path]:?}" || return 1

        composer update \
            --no-interaction \
            --no-plugins \
            --no-scripts \
            --no-dev \
            --prefer-dist \
            --optimize-autoloader \
            --prefer-dist
    fi
}
