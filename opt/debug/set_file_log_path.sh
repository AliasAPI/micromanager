#!/bin/bash

# set_file_log_path must run after set_port_file_path
function set_file_log_path {
    echo "${FUNCNAME[0]}():"

    if [[ "${TRAIN[task]:?}" != "fix" &&
        "${TRAIN[task]:?}" != "scan" &&
        "${TRAIN[task]:0:4}" != "run_" ]] \
        || [[ -z "${TRAIN[file_name]:-}" ]]; then
        return 0
    fi

    # Generate a random suffix to create unique file log names (and reduce race conditions)
    file_log_name=$(echo $RANDOM | md5sum | head -c 5).log

    # Create the path where the tools will write error messages
    file_log_name="${TRAIN["file_name"]}.${file_log_name}"

    # Set the file_log_path or crash if app_logs_dir is not set in TRAIN
    echo "  Setting file_log_path to ${TRAIN["app_logs_dir"]:?}/${file_log_name}"

    set_train "file_log_path" "${TRAIN["app_logs_dir"]:?}/${file_log_name}"
}
