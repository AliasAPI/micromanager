#!/usr/bin/env bash

function run_command {
    local command="$1"
    local output_file

    # Create temporary files securely using mktemp
    if ! output_file=$(mktemp); then
        echo "  Failed to create temporary file for output"
        return 1
    fi

    local error_file

    if ! error_file=$(mktemp); then
        echo "  Failed to create temporary file for error"
        return 1
    fi

    # Save the original output and error file descriptors
    exec 3>&1 4>&2

    set +e
    # Run the command in a subshell and redirect stdout and stderr
    (
        exec 1>"$output_file" 2>"$error_file"
        command $command 
    )
    set -e

    local exit_code=$?

    # Restore the original stdout and stderr file descriptors
    exec 1>&3 2>&4

    # Read the file contents from the output and error files
    local output
    output=$(<"$output_file")

    local error
    error=$(<"$error_file")

    # Delete the files to regain space
    rm -f "$output_file" "$error_file"

    # Write the JSON error log
    if [[ -n "$error" ]]; then
        log_message "$error Exit code: $exit_code"

        return 1 
    fi

    # Display the results while indenting lines by 2 spaces
    if [[ -n "$output" ]]; then
        echo "$output" | while IFS= read -r line; do
            echo "  $line"
        done

        return 0
    fi

    return $exit_code
}
