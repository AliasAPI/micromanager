#!/usr/bin/env bash

function log_message {
    local -r message="$1"
    # todo:: Make this dynamic:
    logs_directory="/data/logs"

    if [[ -z "$message" ]]; then
        return 0
    fi

    if [[ $(basename "$0") != "run_command" ]]; then
        # Get the line_number of the run_command call for some tracing
        local caller_info=$(caller 0)

        # Get the absolute file_path of the script calling run_command
        local file_path=$(realpath "$0" 2>/dev/null || readlink -f "$0")
 
        local line_number=${BASH_LINENO[0]}

        # Get the filename of the calling script without the extension
        local calling_script_name=$(basename "$0" | sed 's/\.[^.]*$//')
    fi

    # If log_message is called from run_command . . . 
    if [[ $(basename "$0") == "run_command" ]]; then
        # Get info referring to the script that called run_command
        local caller_info=$(caller 1)

        local file_path=$(echo "$caller_info" | awk '{print $3}')

        local line_number=$(echo "$caller_info" | awk '{print $1}')
        
        local calling_script_name=$(basename "$file_path")
    fi

    # Generate a timestamp
    local datetime=$(date +"%Y-%m-%d_%H:%M:%S")

    # Define the log file path
    local log_file_path="${logs_directory}/${calling_script_name}_${datetime}.json"

    # Construct JSON log entry
    local json

    json="
        [
            {
            \"file_path\":\"${file_path}\",
            \"line\":${line_number},
            \"column\":0,
            \"flaw\":\"${message}\",
            \"tool\":\"${calling_script_name}\"
            }
        ]"

    # Use jq when available for better JSON handling
    if command -v jq &>/dev/null; then
        json=$(jq -n --arg file_path "$file_path" \
                --argjson line "$line_number" \
                --arg flaw "$message" \
                --arg tool "$calling_script_name" \
                '[{file_path: $file_path, line: $line, column: 0, flaw: $flaw, tool: $tool}]')
    fi

    # Write the JSON log to the file
    if ! echo "${json}" > "$log_file_path"; then
        echo "  Error: Could not write to $log_file_path. Please check directory permissions."
        return 1
    fi
}
