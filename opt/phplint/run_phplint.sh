#!/bin/bash

# Run php_lint to automatically check and beautify PHP files
# https://github.com/overtrue/phplint
function run_phplint {
    echo "${FUNCNAME[0]}():"

    # Install PHPLint
    if [[ "${TRAIN[task]:?}" == "install" ]] \
        && [ ! -f "${TRAIN[bin_dir]:?}/phplint" ]; then
        cd "${TRAIN[toolbox]:?}" || return 1

        composer require --dev overtrue/phplint -vvv
    fi

    # PHPlint does NOT update the file; It only detects the flaws (so fix and scan are the same)
    if [[ "${TRAIN[manage_file]:?}" != "yes" &&
        "${TRAIN[task]:?}" != "${FUNCNAME[0]}" ]]; then
        return 0
    fi

    # Configure PHPLint
    # https://github.com/overtrue/phplint/blob/main/docs/usage/console.md
    # https://github.com/overtrue/phplint/blob/main/docs/configuration.md#json-output-log-json
    local command="php ${TRAIN[bin_dir]:?}/phplint "
    command+="${TRAIN[file_path]:?} "
    # --no-configuration: The configuration file is static, but the file name changes, so set the full path here
    command+="--jobs=10 "
    command+="--no-interaction "
    command+="--no-progress "
    command+="--no-configuration "
    command+="--warning "
    command+="--ignore-exit-code "
    command+="--format=JSON "
    command+="--cache=${TRAIN[app_data_files]:?}/.phplint.cache "
    command+="--output=${TRAIN[file_log_path]:?} "

    # Run PHPLint
    if [[ "${TRAIN[file_type]:?}" != "php" ]]; then
        echo "  Skipping [${TRAIN[file_type]:?}] file"
        return 0
    fi

    set_file_scanned

    set +x
    set +e
    # Do not try to catch errors since PHPlint adds them to the JSON output
    error_output=$({ ${command} 1> "${TRAIN[file_log_path]:?}"; } 2>&1)
    exit_code=$?
    set -e

    if [ $exit_code -gt 1 ]; then
        file_flaws="{\"file_path\":\"${TRAIN[file_path]:?}\",\"line\":0,\"column\":0,"
        file_flaws+="\"flaw\":\"${error} Exit code: ${exit_code}\"}"
        insert_file_flaws "${file_flaws}" "phplint"
    fi

    # Set column=0 (since phplint does not define it)
    file_flaws=$(jq -c '.failures | to_entries[] | .value | 
        {file_path: .absolute_file, line: .line, column: 0, flaw: .error}' "${TRAIN[file_log_path]:?}")

    # Add each flaw to the file_flaws database table and the library that provided the message
    insert_file_flaws "${file_flaws}" "phplint"
}
