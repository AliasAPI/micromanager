#!/bin/bash

# An extra configuration function to demonstrate abstraction when the run_pestphp function becomes too long
# https://pestphp.com/docs/cli-api-reference
# https://docs.phpunit.de/en/11.0/textui.html
function set_pestphp_options {
    echo "  ${FUNCNAME[0]}():"

    # Specify test directory containing Pest.php, TestCase.php, helpers and app tests
    options="--test-directory ${TRAIN[git_app_path]:?}/tests "

    # Only run tests when files in the /app/app/ /app/src/ /app/tests/ directories change
    options="--cache-directory=${TRAIN[app_data_files]:?}/.phpunit "

    options+="--no-output "

    # Generate a test code coverage report. JSON output is not available
    options+="--log-junit=${TRAIN[app_logs_dir]:?}/phpunit_junit.xml "

    options+="--stderr "

    options+="--coverage-clover=${TRAIN[app_logs_dir]:?}/phpunit-coverage-clover.xml "

    # Be strict about code coverage metadata
    options+="--strict-coverage "

    # Set the minimum passing code coverage percent to 90%
    options+="--min=90 "

    # Measure the percentage of the code covered by type declarations
    options+="--type-coverage "

    # Enforce the minimum percentage for type coverage
    options+="--min=100 "

    # Be strict about changes to global state
    options+="--strict-global-state "

    # Output the type coverage report in JSON format

    # todo:: Use the --parallel and --order-by random to run the tests simultaneously and randomly
    # Isolate testing; Resources are not always shared between tests
    # Randomize order; Tests should not rely on a order of execution
    # Eliminate races; Handle potential race conditions in the tests
    # options+="--parallel --order-by random "

    # Set the detailed text output file path
    # testdox_file="${TRAIN[app_logs_dir]:?}/phpunit_testdox.log"
    # coverage_file="${TRAIN[app_logs_dir]:?}/phpunit_crap4j.xml"

    # Stop execution upon first failed test
    if [[ "${TRAIN[force_flaw_fix]:?}" == true ]]; then
        options+="--stop-on-defect "
        options+="--stop-on-error "
        options+="--stop-on-failure "
        options+="--stop-on-warning "
        options+="--stop-on-risky "
        options+="--stop-on-skipped "
        options+="--stop-on-incomplete "
        options+="--fail-on-incomplete "
        options+="--fail-on-risky "
        options+="--fail-on-skipped "
        options+="--fail-on-warning "
    fi

    # Rant
    # I swear Sebastian Bergmann seems like a drawback of using PHP.
    # His work is popular AND garbage. Regarding adding JSON output:

    # sebastianbergmann commented Feb 21, 2022
    # "I do not think that such functionality should be added."
    # https://github.com/sebastianbergmann/php-code-coverage/issues/829
    # JSON makes it much easier to process the output automatically.

    # Further, it is ironic that PHPUnit has the flaws as listed below;
    # PHPUnit is meant to promote code quality and it itself is buggy.

    # I cannot wait to eliminate his libraries from all of my codebases!
    # It will remain a part of Micromanager only because it's pervasive.

    # FLAW: If you set --coverage-crap4j=/data/logs/pestphp_crap4j.xml the following error occurs:
    # ERROR  SebastianBergmann\CodeCoverage\Node\Builder::reducePaths(): Argument #1 ($coverage) must be of type
    # SebastianBergmann\CodeCoverage\ProcessedCodeCoverageData, SebastianBergmann\CodeCoverage\Data\ProcessedCodeCoverageData given,
    # called in /app/vendor/phpunit/php-code-coverage/src/Node/Builder.php on line 45
    # Location: /app/vendor/phpunit/php-code-coverage/src/Node/Builder.php:197
    # options+="--coverage-crap4j=${TRAIN[app_logs_dir]:?}/pestphp_crap4j.xml "

    # FLAW: If you set --colors=always, and output to file (like --log-junit), colors may not show,
    # and the output file will not get created. There is no error stating not to set both options.
    # This flaw occurs in PHPUnit and PestPHP.
    # FLAW: https://docs.phpunit.de/en/9.6/textui.html says --colors <flag> not --colors=<flag>
    # FLAW: https://pestphp.com/docs/cli-api-reference says --colors <flag> not --colors=<flag>
    # The use of the = sign in the documentation is inconsistent. "="" is used in other options
    # Use colors in output ("never", "auto" or "always")
    # options="--colors=always "

    # FLAW: If you are on https://docs.phpunit.de/en/9.6/code-coverage-analysis.html
    # a message says "You are not reading the most recent version of this documentation. 10.5 is the latest version available."
    # The link to the 10.5 documentation is "404 file not found": https://docs.phpunit.de/en/10.5/code-coverage-analysis.html
    # The correct link is https://docs.phpunit.de/en/10.5/code-coverage.html

    echo "    The PestPHP configuration is ${options}"

    set_train "pestphp_options" "${options}"
}
