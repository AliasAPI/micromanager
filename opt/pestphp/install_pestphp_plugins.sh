#!/bin/bash

# https://github.com/pestphp/awesome-pest
# https://pestphp.com/docs/plugins
function install_pestphp_plugins {
    echo "  ${FUNCNAME[0]}():"

    # Install Pest PHP plugins
    if [[ "${TRAIN[task]:?}" == "install" ]] \
        && [[ -f "${TRAIN[bin_dir]:?}/pest" ]]; then
        # Install in /tools
        cd "${TRAIN[toolbox]:?}" || return 1

        # Define the directory where the plugins are installed
        pestphp_directory="${TRAIN[toolbox]:?}/vendor/pestphp"

        # Allow Pest plugins
        # https://github.com/crater-invoice/crater/issues/1006
        composer config --no-plugins allow-plugins.pestphp/pest-plugin true

        # Migrate the PHPUnit tests to Pest tests
        # https://github.com/pestphp/pest-plugin-drift
        # This plugin installs, but I could not get it to work.
        # When I call "pest --drift", the output is "INFO  Unknown option "--drift"
        # It is NOT necessary to reformat the PHPUnit tests to PestPHP
        # if [[ ! -d "${pestphp_directory}/pest-plugin-drift" ]]; then
        #     echo "    Installing pest-plugin-drift . . ."

        #     composer require pestphp/pest-plugin-drift --dev
        # fi

        # Monitor the changes in files in directories
        # Do not install pest-plugin-watch
        # Micromanager uses set_file_md5sum_changed instead

        # Measure the percentage of the code covered by type declarations
        # https://pestphp.com/docs/type-coverage
        # todo:: Uncomment this to install pest-plugin-type-coverage
        # if [[ ! -d "${pestphp_directory}/pest-plugin-type-coverage" ]]; then
        #     echo "    Installing pest-plugin-watch . . ."

        #     composer require pestphp/pest-plugin-type-coverage --dev
        # fi

        # Do not add the pest-plugin-stressless plugin
        # It couples PHP and Go to benchmarking (and is not language agnostic)
        # Consider using pest --profile instead
        # https://pestphp.com/docs/optimizing-tests
    fi
}
