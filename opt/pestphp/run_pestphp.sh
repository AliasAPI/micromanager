#!/bin/bash

# Run Pest last, after linting and checking the code
# https://github.com/pestphp/pest
function run_pestphp {
    echo "${FUNCNAME[0]}():"

    # If a phpunit.xml file is not found in the application root, stop processing
    # https://github.com/elonmallin/vscode-phpunit/issues/33
    if [[ ! -f "${TRAIN[git_app_path]:?}/phpunit.xml" &&
        ! -f "${TRAIN[git_app_path]:?}/phpunit.xml.dist" ]]; then
        echo "  No phpunit.xml found in the application root directory; Skipping"
        return 0
    fi

    # Install PHPUnit
    if [[ "${TRAIN[task]:?}" == "install" ]] \
        && [[ ! -f "${TRAIN[bin_dir]:?}/pest" ]]; then
        cd "${TRAIN[toolbox]:?}" || return 1

        # todo:: Install fswatch for pest-plugin-watch

        # Allow Pest plugins
        composer config --no-plugins allow-plugins.pestphp/pest-plugin true

        composer require pestphp/pest --dev --with-all-dependencies

        install_pestphp_plugins
    fi

    # Configure Pest
    if [[ "${TRAIN[task]:?}" == "install" ||
        "${TRAIN[task]:?}" == "config" ]]; then
        # Change to the /git_app_path where the unit /tests are
        cd "${TRAIN[git_app_path]:?}" || return 1

        echo "  Migrating the phpunit.xml file as needed"
        "${TRAIN[bin_dir]:?}/pest" --migrate-configuration > /dev/null

        return 0
    fi

    if [[ "${TRAIN[manage_file]:?}" != "yes" &&
        "${TRAIN[task]:?}" != "${FUNCNAME[0]}" ]]; then
        return 0
    fi

    # The configuration options are set in TRAIN[opt_dir]/pestphp/set_pestphp_options.sh
    set_pestphp_options

    # Run PestPHP
    run_tests=false

    # Only run the unit tests for fix or scan processing if the file_changed
    if [[ "${TRAIN[task]:?}" == "fix" ||
        "${TRAIN[task]:?}" == "scan" ]] \
        && [[ "${TRAIN[file_changed]:?}" == true ]]; then
        echo "  Running the PHPUnit tests . . ."
        run_tests=true
    fi

    # Otherwise, if the test or run_pestphp commands are used, run the tests
    if [[ "${TRAIN[task]:?}" == "test" ||
        "${TRAIN[task]:?}" == "run_pestphp" ]]; then
        echo "  Running the PHPUnit tests . . ."
        run_tests=true
    fi

    # If all the run_tests conditions failed, stop processing
    if [[ ${run_tests} != true ]]; then
        return 0
    fi

    cd "${TRAIN[git_app_path]:?}" || return 1

    "${TRAIN[bin_dir]:?}/pest" "${TRAIN[pestphp_options]:?}"

    # todo:: Convert XML output to JSON with the xq Utility
    # todo:: Process the pest-type-coverage-report.json file

    unset 'TRAIN[pestphp_options]'
}
