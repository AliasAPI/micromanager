#!/bin/bash

# Run PHPStan to automatically check PHP files
# https://github.com/phpstan/phpstan
# Extensions: https://packagist.org/explore/?type=phpstan-extension
function run_phpstan {
    echo "${FUNCNAME[0]}():"

    # todo:: Fix PHPStan?
    # https://getcomposer.org/doc/03-cli.md#diagnose
    # https://getcomposer.org/doc/03-cli.md#composer-debug-events

    # Install PHPStan
    if [[ "${TRAIN[task]:?}" == "install" ]] \
        && [ ! -f "${TRAIN[bin_dir]:?}/phpstan" ]; then
        cd "${TRAIN[toolbox]:?}" || return 1
        composer require --dev phpstan/phpstan
        install_phpstan_extensions
    fi

    # Configure PHPStan
    # Review https://github.com/phpro/grumphp/blob/master/doc/tasks/phpstan.md
    # todo:: Add the most strict configuration file in etc/phpstan/run_phpstan.sh
    # https://github.com/phpstan/phpstan-strict-rules
    if [[ "${TRAIN[task]:?}" == "install" ||
        "${TRAIN[task]:?}" == "config" ]] \
        && [ ! -f "${TRAIN[toolbox]:?}/phpstan.neon" ]; then
        cp "${TRAIN[opt_dir]}/phpstan/phpstan.neon" "${TRAIN[toolbox]:?}"
    fi

    if [[ "${TRAIN[manage_file]:?}" != "yes" &&
        "${TRAIN[task]:?}" != "${FUNCNAME[0]}" ]]; then
        return 0
    fi

    command="php ${TRAIN[bin_dir]:?}/phpstan analyze -c ${TRAIN[toolbox]:?}/phpstan.neon --no-progress --error-format=json --memory-limit=256M ${TRAIN[file_path]:?}"

    # Run PHPStan
    if [[ "${TRAIN[file_type]:?}" != "php" ]]; then
        echo "  Skipping [${TRAIN[file_type]:?}] file"
        return 0
    fi

    set_file_scanned

    cd "${TRAIN[git_app_path]:?}" || return 1

    set +e
    error=$(${command} 2>&1)
    exit_code=$?
    set -e

    if [[ "${exit_code}" -gt 1 ]]; then
        file_flaws="{\"file_path\":\"${TRAIN[file_path]:?}\",\"line\":0,\"column\":0,\"flaw\":\"${error} Exit code: ${exit_code}\"}"
        insert_file_flaws "${file_flaws}" "phpstan"
    fi

    file_flaws=$(jq -c '.files | to_entries[] | {file_path: .key} + (.value.messages[] | {line, column: 0, flaw: .message})' "${TRAIN[file_log_path]:?}")

    insert_file_flaws "${file_flaws}" "phpstan"
}
