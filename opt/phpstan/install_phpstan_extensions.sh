#!/bin/bash

# List of plugins that can fix code: https://github.com/orklah
function install_phpstan_extensions {
    echo "${FUNCNAME[0]}():"

    if [[ "${TRAIN[task]:?}" != "install" ]]; then
        return 0
    fi

    cd "${TRAIN[toolbox]:?}" || return 1

    # Allow extensions to be installed
    composer config --no-plugins allow-plugins.phpstan/extension-installer true

    # PHPStan Extension Installer
    # https://github.com/phpstan/extension-installer
    composer require --dev phpstan/extension-installer

    # https://github.com/phpstan/phpstan-strict-rules
    composer require --dev phpstan/phpstan-strict-rules

    # https://github.com/ergebnis/phpstan-rules
    composer require --dev ergebnis/phpstan-rules

    # @why Analyze SQL and and infer type for the database access layer
    # https://github.com/staabm/phpstan-dba
    composer require --dev staabm/phpstan-dba

    # @why Reduce the cognative complexity of PHP code
    # https://github.com/TomasVotruba/cognitive-complexity
    # https://tomasvotruba.com/blog/keep-cognitive-complexity-low-with-phpstan/
    composer require --dev tomasvotruba/cognitive-complexity

    # https://github.com/phpstan/phpstan-phpunit
    composer require --dev phpstan/phpstan-phpunit

    # @why Detect usage of deprecated classes, methods, properties, constants and traits
    # https://github.com/phpstan/phpstan-deprecation-rules
    composer require --dev phpstan/phpstan-deprecation-rules

    # @why Detect disallowed method & function calls, constant, namespace & superglobal usages
    # https://github.com/spaze/phpstan-disallowed-calls
    composer require --dev spaze/phpstan-disallowed-calls

    # @why Rewrite all native PHP functions to throw exceptions instead of returning false
    # https://github.com/thecodingmachine/safe
    composer require thecodingmachine/safe
    # https://github.com/thecodingmachine/phpstan-safe-rule
    composer require --dev thecodingmachine/phpstan-safe-rule

    # todo:: Install https://github.com/phpstan/phpdoc-parser

    # https://github.com/Jan0707/phpstan-prophecy
    composer require --dev jangregor/phpstan-prophecy

    # @why Detect banned PHP code
    # https://github.com/ekino/phpstan-banned-code
    composer require --dev ekino/phpstan-banned-code

    # @why Detect unused public properties, constants and methods in PHP code
    # https://github.com/TomasVotruba/unused-public
    composer require --dev tomasvotruba/unused-public

    # @why Require minimal type-coverage
    # https://github.com/TomasVotruba/type-coverage
    composer require --dev tomasvotruba/type-coverage --update-with-all-dependencies

    # @why Define dynamic methods for MyCLabs\Enum\Enum subclasses
    # https://github.com/timeweb/phpstan-enum
    composer require --dev timeweb/phpstan-enum

    # https://github.com/symplify/phpstan-extensions
    composer require --dev symplify/phpstan-extensions

    # @why Migrate to a clock abstraction like lcobucci/clock
    # https://github.com/Slamdunk/phpstan-extensions
    composer require --dev slam/phpstan-extensions

    # @why Define PHP language extensions
    # https://github.com/DaveLiddament/php-language-extensions
    # dave-liddament/phpstan-php-language-extensions is locked to version 0.4.0 and an update of this package was not requested.
    # dave-liddament/phpstan-php-language-extensions 0.4.0 requires php >=8.0 <8.3 -> your php version (8.3.0) does not satisfy that requirement.
    composer require --dev dave-liddament/phpstan-php-language-extensions

    # @why Rewrite double equals to triple equals in PHP code
    # https://github.com/killov/phpstan-banned-double-equals
    composer require --dev killov/phpstan-banned-double-equals

    # @why Clean up unit tests
    # https://github.com/ikvasnica/phpstan-clean-test
    composer require --dev ikvasnica/phpstan-clean-test --ignore-platform-reqs --with-all-dependencies

    # @why Detect unfavorable regular expressions in PHP code
    # todo:: Uncomment this to get struggle-for-php/angry-regex working again
    # https://github.com/struggle-for-php/angry-regex
    # composer require --dev struggle-for-php/angry-regex

    # @why Check for smells described in the XUnit Patterns book by Gerard Meszaros
    # https://github.com/ethancarlsson/xunitpatterns-stan
    # composer require --dev xunitpatterns/xunitpatterns-stan
}
