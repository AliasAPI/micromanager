#!/bin/bash

function run_php {
    echo "${FUNCNAME[0]}():"

    if [[ "${TRAIN[task]:?}" != "install" &&
        "${TRAIN[task]:?}" != "restart" ]]; then
        return 0
    fi

    # Install

    # Configure
    # This php.ini file is from Bitnami and has extension=imap uncommmented
    # cp -ra /opt/php/php.ini /opt/bitnami/php/opt/

    # Run

    # Process the PHP log
    # Parse the errors from php_errors.log and add them to the file_flaws table
}
