#!/bin/bash

# Check and beautify JSON files
function run_jq {
    echo "${FUNCNAME[0]}():"

    # Drew: In this case JQ will only install in a container build.
    #   When not in a container build, like ./mm install, task will be set
    #   and therefore jq does not get installed. Changed it to run install
    #   when jq is not available
    # Install JQ if not already installed and TRAIN[task] is not set
    #if [[ -z "${TRAIN[task]}" ]] \
    #    && ! command -v jq > /dev/null 2>&1; then
    if ! command -v jq > /dev/null 2>&1; then
        if [[ -z "${TRAIN[task]}" ]]; then
            # todo:: Elrod, Remove this hack (and use run_apt_get)
            apt-get clean
            rm -rf /var/lib/apt/lists/*
            apt-get update
            apt-get install jq -y 2>&1 | while IFS= read -r line; do
                echo "  $line"
            done
        else
            sudo apt-get install jq -y 2>&1 | while IFS= read -r line; do
                echo "  $line"
            done
        fi
    fi
    
    # Run JQ to check the format of JSON files
    if [[ -z "${TRAIN[manage_file]}" || -z "${TRAIN[file_type]:-}" ]]; then
        return 0
    fi

    if [[ "${TRAIN[file_type]}" != "json" ]]; then
        echo "  Skipping [${TRAIN[file_type]}] file"
        return 0
    fi

    set_file_scanned

    # Load and scan the JSON file to see if jq says it is invalid
    if [[ "${TRAIN[manage_file]:?}" == "yes" 
        || "${TRAIN[task]:?}" == "${FUNCNAME[0]}" ]]; then
        # Run the jq command and capture error messages in a variable
        error=$(jq '.' "${TRAIN[file_path]:?}" 2>&1)

        exit_code=$?

        if [[ "${exit_code}" -gt 0 ]]; then
            file_flaws=$(jq -n --arg file_path "${TRAIN[file_path]:?}" \
            --arg error "${error}" \
            '{file_path: $file_path, line: 0, column: 0, flaw: $error}' | jq -c '.')

            insert_file_flaws "${file_flaws}" "jq"
            echo "  JSON validation failed: ${error}"
        fi
    fi

    # Fix the JSON file since it contains valid JSON at this point
    if [[ "${TRAIN[task]:?}" == "fix" ]]; then
        temp_file=$(mktemp)

        if jq --indent 2 "." < "${TRAIN[file_path]:?}" > "${temp_file}"; then
            if ! diff -q "${temp_file}" "${TRAIN[file_path]:?}" > /dev/null; then
                echo "  Reformatting ${TRAIN[file_path]:?}"
                cat "${temp_file}" > "${TRAIN[file_path]:?}"
            fi
        fi

        rm "${temp_file}"
    fi
}
