#!/bin/bash

# fixing, new, failed, skipped, passed, fixed, unapproved, rejected, approved
function set_file_status {
    echo "${FUNCNAME[0]}():"

    if [[ "${TRAIN[manage_file]:?}" != "yes" &&
        "${TRAIN[task]:0:4}" != "run_" ]]; then
        return 0
    fi

    # If the file_path has been scanned by at least one run_ tool, increment file_scans
    if [[ "${TRAIN[file_scanned]}" == true ]]; then
        set_train "file_scans" $((TRAIN[file_scans] + 1))
    fi

    # file_scanned is set to true when task is scan, fix, or run_ (in each run_ function)
    # track_loops counts the number of times the main loop was run to scan the file
    if [[ "${TRAIN[file_scanned]}" == true &&
        "${TRAIN[file_status]}" = "new" &&
        -n "${TRAIN[file_flaws]:-}" &&
        "${TRAIN[file_scans]}" -gt 0 ]]; then
        set_train "file_status" "fixing"
    fi

    # If set_file_scanned was never called when running a tool, mark it skipped
    if [[ "${TRAIN[file_scanned]}" == false &&
        "${TRAIN[file_status]}" = "new" &&
        "${TRAIN[track_loops]}" -ge "${TRAIN[file_loop_max]:?}" ]]; then
        set_train "file_status" "skipped"
    fi

    # If the file has been scanned and there are no file_flaws
    # it means the file "passed" all the tool (syntax) tests
    if [[ "${TRAIN[file_status]}" = "new" &&
        "${TRAIN[file_mods]}" -eq 0 &&
        -z "${TRAIN[file_flaws]:-}" &&
        "${TRAIN[file_scans]}" -gt 0 ]]; then
        set_train "file_status" "passed"
    fi

    # If the file_path was scanned, and it changed,
    # and there are no file_flaws, then it's fixed
    if [[ "${TRAIN[file_mods]}" -gt 0 &&
        -z "${TRAIN[file_flaws]:-}" &&
        "${TRAIN[file_scans]}" -gt 0 ]]; then
        set_train "file_status" "fixed"
    fi

    # If there are still file_flaws related to the file_path and
    # the file_path has been scanned the maximum number of times
    # then stop processing the file_path and record it as failed
    if [[ "${TRAIN[file_scans]}" -ge "${TRAIN[file_scan_max]}" &&
        -n "${TRAIN[file_flaws]:-}" &&
        "${TRAIN[force_flaw_fix]:?}" != "true" ]]; then
        set_train "file_status" "failed"
    fi
}
