#!/bin/bash

# Install MicroManager
function install_micromanager {
    # If command line arguments are passed, do not continue processing
    if [[ $# -gt 0 ]] \
       || ! command -v sudo > /dev/null; then
        return 0
    fi

    echo "${FUNCNAME[0]}():"

    # If the mm file exists, Don't install
    if [[ -f "/opt/micromanager/mm" 
        || -f "/var/lib/dockerized" ]]; then
        echo "  MicroManager is already installed."
        return 0
    fi

    # Set the location of the MicroManager repository
    micromanager_repository="https://AliasAPI@bitbucket.org/AliasAPI/micromanager.git"

    # Get the name of the operating system
    operating_system=$(uname)

    # Check if the operating system is Debian-based
    if [ "$operating_system" != "Linux" ] \
        || ! command -v apt-get > /dev/null; then
        echo "  Error: Please install MicroManager on a Debian-based operating system like MX Linux."
        return 1
    fi

    # Install Git if it is not already installed
    if ! command -v git > /dev/null; then
        echo "  Installing git . . ."

        if ! sudo apt-get install git -y; then
            echo "  Error: Failed to install git."
            return 1
        fi
    fi

    # Change directory to /opt
    if ! cd /opt; then
        echo "  Error: Could not change directory to /opt."
        return 1
    fi

    # Clone the MicroManager repository
    if ! sudo git clone "$micromanager_repository"; then
        echo "  Error: Failed to clone the repository."
        return 1
    fi

    # Configure MicroManager
    # Change ownership of the /opt/micromanager directory to the current user
    if ! sudo chown -R "$USER":"$USER" /opt/micromanager; then
        echo "  Error: Failed to change ownership of /opt/micromanager."
        return 1
    fi

    # Grant the user read, write, and execute permissions on the /opt/micromanager directory
    if ! sudo chmod -R u+rwx /opt/micromanager; then
        echo "  Error: Failed to set permissions on /opt/micromanager."
        return 1
    fi

    # Grant execution permissions to the script
    if [ -f "/opt/micromanager/mm" ]; then
        chmod +x /opt/micromanager/mm
    fi

    # Run MicroManager
    /opt/micromanager/mm install
}

install_micromanager "$@"
