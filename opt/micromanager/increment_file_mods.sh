#!/bin/bash

# Determine if the text in the file_path has been changed
# Run integration and unit tests when file_changed = true
function increment_file_mods {
    echo "${FUNCNAME[0]}():"

    if [[ "${TRAIN[manage_file]:?}" != "yes" &&
        "${TRAIN[task]:0:4}" != "run_" ]]; then
        return 0
    fi

    # Recalculate the md5sum to help determine if the file changed
    file_md5sum=$(md5sum "${TRAIN[file_path]:?}" | awk '{print $1}')

    # The file_md5sum is initially set in insert_files_to_process
    if [[ "${TRAIN[file_md5sum]:?}" != "${file_md5sum}" ]]; then
        set_train "file_md5sum" "${file_md5sum}"

        # Increment file_mods to track how many changes were made
        set_train "file_mods" $((TRAIN[file_mods] + 1))

        # Mark the file_changed (to know when to run unit tests)
        set_train "file_changed" true

        echo "  The file has been modified"
    fi
}
