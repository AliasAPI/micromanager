#!/bin/bash

function create_directories {
    echo "${FUNCNAME[0]}():"

    if [[ "${TRAIN[task]:?}" != "install" &&
        "${TRAIN[task]:?}" != "restart" &&
        "${TRAIN[task]:?}" != "permit" ]]; then
        return 0
    fi

    # todo:: Create a loop for this repeated code
    if [ ! -d "$MM_DIR/data" ]; then
        mkdir "$MM_DIR/data"
    fi

    if [ ! -d "$DATA_FILES_DIR" ]; then
        mkdir "$DATA_FILES_DIR"
    fi

    if [ ! -d "$DATA_FILES_DIR/.file_lists" ]; then
        mkdir "$DATA_FILES_DIR/.file_lists"
    fi

    if [ ! -d "$LOGS_DIR" ]; then
        mkdir "$LOGS_DIR"
    fi

    if [ ! -d "$DB_DUMPS_DIR" ]; then
        mkdir "$DB_DUMPS_DIR"
    fi

    if [ ! -d "$MARIADB_DIR" ]; then
        mkdir "$MARIADB_DIR"
    fi

    if [ ! -d "$MARIADB_DIR/data" ]; then
        mkdir "$MARIADB_DIR/data"
    fi

    if [ ! -d "$MM_DIR/app" ]; then
        mkdir "$MM_DIR/app"
    fi

    if [ ! -d "$MM_DIR/prompts" ]; then
        mkdir "$MM_DIR/prompts"
    fi

    if [ ! -d "$MM_DIR/tools" ]; then
        mkdir "$MM_DIR/tools"
    fi

    if [ ! -d "$MM_DIR/app/public" ]; then
        mkdir "$MM_DIR/app/public"

        # Replace the placeholder application
        index="$MM_DIR/app/public/index.php"
        sudo chmod -R a+rwX "$MM_DIR/app/public"
        echo "<?php" > "$index"
        echo "echo \"Welcome to MicroManager !\" " >> "$index"
    fi
}
