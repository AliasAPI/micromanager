#!/bin/bash

# Set the Micromanager environment variables
function set_environment_variables {
    echo "${FUNCNAME[0]}():"

    # Configure the MM_DIR (which is the root directory of the project on the host machine)
    script_directory="$(cd "$(dirname "${BASH_SOURCE[0]}")" && pwd)/../../"
    MM_DIR=$(realpath -s "$script_directory")
    export MM_DIR=${MM_DIR%/}

    # List the environment files
    environment_files="
        ${MM_DIR}/opt/micromanager/.env.micromanager 
        ${MM_DIR}/.env 
        ${MM_DIR}/opt/ollama/.env.ollama"

    environment_variables=""

    echo "  Setting the required MicroManager environment variables . . ."

    # Use the UID and GID from the host machine
    export UID
    export GID=$(id -g)
    export USER
    export GROUP=$(id -gn)

    # Export environment variables
    set -a

    # Loop through the environment files
    for environment_file in $environment_files; do
        if [[ -f "$environment_file" ]]; then
            source "$environment_file"

            # Extract environment variable names from the sourced file
            while IFS='=' read -r variable value; do
                # echo "  Add $variable"
                if [[ "$variable" =~ ^[A-Z_][A-Z0-9_]*$ ]]; then
                    environment_variables+="$variable "
                fi
            done < <(grep -E '^[A-Z_][A-Z0-9_]*=' "$environment_file")
        fi
    done

    # Loop through each variable name and check if it is set
    for variable in $environment_variables; do
        # Remove leading and trailing whitespace
        variable=$(echo "$variable" | xargs)

        if [[ -z "${!variable+x}" ]]; then
            echo "  Error: The [$variable] variable is not set"
            echo "  Set a default value in the .env file"
            exit 1
        fi
    done

    set +a

    set_train "environment_variables" "$environment_variables"

    echo "  The environment variables required by MicroManager are set"
}
