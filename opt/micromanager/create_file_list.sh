#!/bin/bash

# Create a file listing the files that will be processed
# based on configurations in the .env-micromanager file
function create_file_list {
    echo "${FUNCNAME[0]}():"

    if [[ "${TRAIN[task]:?}" != "install" &&
        "${TRAIN[task]:?}" != "restart" &&
        "${TRAIN[task]:?}" != "reset" ]] \
        || [[ "${TRAIN[dockerized]:?}" == "no" ]]; then
        return 0
    fi

    exclude_filepaths=""

    if [ -f "${TRAIN[files_to_exclude]:?}" ]; then
        while IFS= read -r line || [ -n "$line" ]; do
            exclude_filepaths="${exclude_filepaths}${line}"$'\n'
        done < "${TRAIN[files_to_exclude]:?}"
    fi

    file_list=$(find_files \
        "${TRAIN[scan_base_paths]:?}" \
        "${TRAIN[scan_directory_depth]:-}" \
        "${TRAIN[scan_exclude_directories]:-}" \
        "$exclude_filepaths" \
        "${TRAIN[scan_exclude_strings]:-}" \
        "${TRAIN[scan_filename_patterns]:-}" \
        "${TRAIN[scan_file_strings]:-}" \
        "${TRAIN[scan_file_limit]:-}")

    if [[ -z "$file_list" ]]; then
        echo "  file_list is empty; Check for typos:"
        exit 1
    fi

    echo "$file_list" > "/${TRAIN[files_to_process]:?}" 2>&1

    echo "  Created /${TRAIN[files_to_process]:?}"
}
