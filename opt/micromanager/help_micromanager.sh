#!/bin/bash

function help_micromanager {
    if [[ -z "${TRAIN[task]}" ||
        "${TRAIN[task]}" == "help" ||
        "${TRAIN[task]}" == "--help" ||
        "${TRAIN[task]}" == "-h" ]]; then
        # Get the absolute path of the script's directory
        script_dir="$(cd "$(dirname "${BASH_SOURCE[0]}")" && pwd)"

        # Use the absolute path to the help.txt file
        cat "${script_dir}/help.txt" | more
        exit 1
    fi
}
