#!/bin/bash

# Prompts the user to enter "yes" or "no" and returns 0/1
# returns
# 0 - yes
# 1 - no

# Usage:
# source confirm_yes_or_no
# if confirm_yes_or_no "Is this a yes or no question?" ; then
#     echo "Yes"
# else
#     echo "no"
# fi
function confirm_yes_or_no {
    question="$1"
    answer=''

    # Set default to "no" if no input is provided
    if [[ -z "${answer}" ]]; then
        answer='no'
    fi

    while true; do
        echo "${question}"
        printf "  Type \"yes\" or \"no\" [no]: "
        read -r input
        answer=$(echo "${input}" | tr '[:upper:]' '[:lower:]')

        if [[ "${answer}" == "yes" ]]; then
            return 0
        fi

        if [[ "${answer}" == "no" ||
            -z "${answer}" ]]; then
            return 1
        fi
    done
}
