#!/bin/bash

function delete_applications {
    echo "${FUNCNAME[0]}():"

    # If not deleting (during development), just return
    if [[ "${TRAIN[task]:?}" != "delete" ]]; then
        return 0
    fi

    # Ask the user if they want to delete the original source directory
    if confirm_yes_or_no "  Permanently delete ALL of the downloaded applications?"; then
        if [ -d "${MM_DIR:?}/tools/" ]; then
            echo "  Deleting the libraries in ${MM_DIR}/tools/ . . ."
            sudo rm -Rf "${MM_DIR}"/tools/
        fi

        if [ -d "${MM_DIR}/app" ]; then
            echo "  Deleting the application in ${MM_DIR}/app/ . . ."
            sudo rm -Rf "${MM_DIR}/app"
        fi

        if [ -d "${MM_DIR}/prompts" ]; then
            echo "  Deleting the port code in ${MM_DIR}/prompts/ . . ."
            sudo rm -Rf "${MM_DIR}/prompts"
        fi

        if [ -d "${LOGS_DIR}" ]; then
            echo "  Deleting the logs in ${LOGS_DIR} . . ."
            sudo rm -Rf "${LOGS_DIR}"
        fi

        if [ -d "${MM_DIR}/data/ollama/" ]; then
            echo "  Deleting the Ollama models in ${MM_DIR}/data/ollama/ . . ."
            sudo rm -Rf "${MM_DIR}/data/ollama/"
        fi
    fi

    echo ""

    if confirm_yes_or_no "  Permanently delete the files in /data and databases?"; then
        if [ -d "${DATA_FILES_DIR}" ]; then
            echo "  Deleting directories and flat files in ${DATA_FILES_DIR} . . ."
            sudo rm -Rf "${DATA_FILES_DIR}/.train.json"
            sudo rm -Rf "${DATA_FILES_DIR}"
        fi

        if [ -d "${LOGS_DIR}" ]; then
            echo "  Deleting the logs in ${LOGS_DIR} . . ."
            sudo rm -Rf "${LOGS_DIR}"
        fi

        if [ -d "${MARIADB_DIR}" ]; then
            echo "  Deleting the database in ${MARIADB_DIR} . . ."
            sudo rm -Rf "${MARIADB_DIR}"
        fi

        if [ -d "${DB_DUMPS_DIR}" ]; then
            echo "  Deleting the database dumps in ${MARIADB_DIR} . . ."
            sudo rm -Rf "${DB_DUMPS_DIR}"
        fi
    fi

    echo ""

    # Enable dotglob to include hidden files
    shopt -s dotglob

    # If /data directory exists and is empty, delete it
    if [ -d "${MM_DIR}/data" ] \
        && [ -z "$(ls -A "${MM_DIR}/data")" ]; then
        echo "  Removing the empty ${MM_DIR}/data directory . . ."
        rmdir "${MM_DIR}/data"

        echo ""
    fi

    # Disable dotglob
    shopt -u dotglob
}
