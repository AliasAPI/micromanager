#!/bin/bash

function configure_file_orders {
    echo "${FUNCNAME[0]}():"

    # Select the markup (or data) files first since they may be configuration files
    # Select PHP files first and then other languages to levarage the PHP libraries
    file_type_order=" 'json', 'lock', 'yml', 'yaml', 'xml', 'css',  'html', 'sql', 'tt', 'sh', 'js', 'php', 'py', 'pl', 'go' "

    # Select "fixing" files first in case fixing process was interrupted
    file_status_order=" 'fixing', 'new', 'failed', 'skipped', 'passed', 'ported', 'fixed', 'unapproved', 'rejected', 'approved' "

    set_train "file_status_order" "$file_status_order"
    set_train "file_type_order" "$file_type_order"

    # Display this ordering on bootstrap tasks
    if [[ "${TRAIN[task]:?}" == "install" ||
        "${TRAIN[task]:?}" == "restart" ||
        "${TRAIN[task]:?}" == "config" ]]; then
        echo "  file_type_order:"
        echo " ${file_type_order}"

        echo "  file_status_order:"
        echo " ${file_status_order}"
    fi
}
