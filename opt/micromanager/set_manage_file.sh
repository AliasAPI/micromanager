#!/bin/bash

function set_manage_file {
    echo "${FUNCNAME[0]}():"

    set_train "manage_file" "no"

    if [[ "${TRAIN[task]:?}" == "fix" ||
        "${TRAIN[task]}" == "scan" ]] \
        && [[ -n "${TRAIN[file_path]:-}" ]] \
        && [[ -n "${TRAIN[file_name]:-}" ]] \
        && [[ -n "${TRAIN[file_type]:-}" ]] \
        && [[ -n "${TRAIN[file_md5sum]:-}" ]] \
        && [[ -n "${TRAIN[file_scans]:-}" ]] \
        && [[ -n "${TRAIN[file_mods]:-}" ]] \
        && [[ -n "${TRAIN[file_status]:-}" ]] \
        && [[ -n "${TRAIN[mariadb_connect]:-}" ]] \
        && [[ -n "${TRAIN[file_log_path]:-}" ]] \
        && [[ "${TRAIN[dockerized]:?}" == "yes" ]]; then
        echo "  This file will be managed"
        set_train "manage_file" "yes"
    fi
}
