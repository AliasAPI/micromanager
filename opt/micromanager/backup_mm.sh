#!/bin/bash

function backup_mm {
    echo "${FUNCNAME[0]}():"

    if [[ "${TRAIN[task]:?}" != "backup" ]]; then
        return 0
    fi

    # todo:: Check to make sure the database dump exists

    # todo:: Transmit the database dump to a remote server

    # todo:: Check to make sure the backup was successful

    # todo:: If the backup is NOT successful send an email
}
