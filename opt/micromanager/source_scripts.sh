#!/bin/bash

# Source the Micromanager helper functions
function source_scripts {
    echo "${FUNCNAME[0]}():"

    # Include the find_files command
    echo "  Autoloading the shell scripts that run MicroManager . . ."

    # Shellcheck cannot handle dynamic paths
    # shellcheck disable=SC1090,SC1091
    source "$(cd "$(dirname "${BASH_SOURCE[0]}")" && pwd)/find_files.sh"

    # Configure the location of the Micromanager shell scripts and find_files()
    base_paths="$(cd "$(dirname "${BASH_SOURCE[0]}")" && pwd)/.."
    directory_depth=51
    exclude_directories=""
    exclude_filepaths=""
    exclude_strings=""
    filename_patterns=".sh"
    find_strings=""
    file_limit=10000

    found_scripts=$(find_files \
        "$base_paths" \
        "$directory_depth" \
        "$exclude_directories" \
        "$exclude_filepaths" \
        "$exclude_strings" \
        "$filename_patterns" \
        "$find_strings" \
        "$file_limit")

    # Loop through the files and include their functions
    while IFS= read -r file; do
        # echo "  Sourcing [$file]"
        # Shellcheck cannot handle dynamic paths
        # shellcheck disable=SC1090,SC1091
        source "$file"
    done <<< "$found_scripts"
}
