#!/bin/bash

function create_mm_alias {
    echo "${FUNCNAME[0]}():"

    if [[ "${TRAIN[task]}" != 'install' &&
        "${TRAIN[task]}" != 'config' ]]; then
        return 0
    fi

    local bash_configuration_file="$HOME/.bashrc"
    local mm_directory="/opt/micromanager"
    local mm_script="/opt/micromanager/mm"

    if [ ! -f "$bash_configuration_file" ]; then
        echo "  Error: Create a .bashrc file in $HOME"

        return 1
    fi

    if [ ! -f "$mm_script" ]; then
        echo "  Error: Install MicroManager in $mm_directory"
        return 1
    fi

    # If the mm alias has already been added to the .bashrc, leave this function
    if grep -q "alias mm=" "$bash_configuration_file"; then
        return 0
    fi

    if confirm_yes_or_no "  Do you want to add the mm alias to your .bashrc?"; then
        echo "alias mm='cd $mm_directory && bash \"$mm_script\" \"\$@\"'" >> "$bash_configuration_file"
        echo "  The mm alias has been added to your .bashrc"

        source "$bash_configuration_file"
        echo "  You can now use the mm alias to run MicroManager"
    fi
}
