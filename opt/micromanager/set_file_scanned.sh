#!/bin/bash

function set_file_scanned {
    echo "  ${FUNCNAME[0]}():"

    # Set file_scanned if the task is fix, scan, or is a run_ function name
    if [[ "${TRAIN[manage_file]:?}" == "yes" ||
        "${TRAIN[task]:0:4}" == "run_" ]]; then
        set_train "file_scanned" true

        echo "    The file_path has been scanned"
    fi
}
