#!/usr/bin/env bash

set -euo pipefail

# The find_files function is a Bash script that searches for files in a given directory
# and its subdirectories, based on search patterns and exclusion rules. If no files are
# found that match the search criteria, the function will return an empty string.
# Uncomment the example below this function and call ./find_files.sh to see the results
function find_files {
    local base_paths="$1"
    local directory_depth="$2"
    local exclude_directories="$3"
    local exclude_filepaths="$4"
    local exclude_strings="$5"
    local filename_patterns="$6"
    local find_strings="$7"
    local file_limit="${8:-1000}"

    local exclude_paths=""
    local exclude_files=""
    local include_files=""
    local limit=""
    local find_command=""

    # Validate inputs
    if [[ -z "$base_paths" ]]; then
        echo "Error: base_paths is required" >&2
        return 1
    fi

    # Construct exclude_paths
    if [[ -n "${exclude_directories// /}" ]]; then
        exclude_paths=" -type d \\( "
        for exclude_directory_pattern in $exclude_directories; do
            exclude_paths+=" -path \"*$exclude_directory_pattern*\" -o "
        done
        exclude_paths="${exclude_paths% -o }"
        exclude_paths+=" \\) -prune -false -o "
    fi

    # Construct exclude_files
    if [[ -n "${exclude_filepaths}" ]]; then
        exclude_files=" -type f \\( "
        for exclude_file in $exclude_filepaths; do
            exclude_files+=" ! -path \"*$exclude_file*\" -a "
        done
        exclude_files="${exclude_files% -a }"
        exclude_files+=" \\) -a "
    fi

    # Construct include_files
    if [[ -n "${filename_patterns// /}" ]]; then
        include_files=" \\( "
        for filename_pattern in $filename_patterns; do
            include_files+=" -iname \"*$filename_pattern\" -o "
        done
        include_files="${include_files% -o }"
        include_files+=" \\) "
    fi

    limit=" | head -z -n ${file_limit} "

    find_command="find ${base_paths} -maxdepth ${directory_depth} ${exclude_paths} ${exclude_files} ${include_files} -type f -print0 ${limit}"

    if [[ "${DEBUG:-false}" == "true" ]]; then
        echo "Debug: find command: $find_command" >&2
    fi

    local found_files=()
    while IFS= read -r -d '' file; do
        local exclude_file=false
        local string_found=false

        # Use realpath for better portability
        local file_path
        file_path=$(realpath -s "$file")

        if [[ -n "$exclude_strings" ]]; then
            for exclude_string in $exclude_strings; do
                if grep -q -E -e "$exclude_string" "$file_path"; then
                    exclude_file=true
                    break
                fi
            done
        fi

        # If the file should be excluded, skip this iteration
        if [[ "$exclude_file" == true ]]; then
            continue
        fi

        # If $find_strings is not set, add the file to found_files and continue to next iteration
        if [[ -z "$find_strings" ]]; then
            found_files+=("$file_path")
            continue
        fi

        # Perform grep if $find_strings is set
        for string in $find_strings; do
            if grep -q -E -e "$string" "$file_path"; then
                string_found=true
                break
            fi
        done

        # Add the file to found_files if a match was found
        if [[ "$string_found" == true ]]; then
            found_files+=("$file_path")
        fi
    done < <(eval "$find_command")

    # Sort the results to return more consistent lists
    if [[ ${#found_files[@]} -gt 0 ]]; then
        printf '%s\n' "${found_files[@]}" | sort -u
    fi
}

# Example usage (uncomment to test):
# base_paths="/opt /app"
# directory_depth=5
# exclude_directories="vendor .composer .phan phpmnd"
# exclude_filepaths="../../app/src/create_items.php ../../app/src/delete_items.php"
# exclude_strings=""
# filename_patterns=".sh .html .php"
# find_strings=""
# file_limit=100
# DEBUG=true
# files=$(find_files "$base_paths" "$directory_depth" "$exclude_directories" \
#   "$exclude_filepaths" "$exclude_strings" "$filename_patterns" "$find_strings" "$file_limit")
# echo "${files}"
