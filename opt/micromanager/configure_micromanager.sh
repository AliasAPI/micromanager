#!/bin/bash

# Set the global environment variables
function configure_micromanager {
    echo "${FUNCNAME[0]}():"

    # If $@ is not set or empty, set it to "help"
    if [ $# -eq 0 ] || [ -z "$*" ]; then
        set -- "help"
    fi

    set_train "task" "$1"

    set_train "time_start" "$(date +%s)"

    set_train "mm_user" "${USER}"

    set_manage_file

    set_dockerized

    # Copy the env.micromanager file to .env so the developer can add environment variables
    if [[ "${TRAIN[task]:?}" == "install" 
        || "${TRAIN[task]:?}" == "config" ]] \
        && [ ! -f "/opt/micromanager/.env" ] \
        && [ -f "${TRAIN[opt_dir]}/micromanager/.env.micromanager" ]; then
        cp "${TRAIN[opt_dir]}/micromanager/.env.micromanager" /opt/micromanager/.env
    fi

    if [ -z "${TRAIN[environment_variables]}" ]; then
        echo "  Set the environment_variables in set_environment_variables()"
        return 1
    fi

    # Set the environment_variables on the TRAIN
    for variable in ${TRAIN[environment_variables]}; do
        # Trim leading and trailing whitespace
        variable=$(echo "$variable" | xargs)

        # Convert to lowercase key for TRAIN array
        lowercase_variable=$(echo "$variable" | tr '[:upper:]' '[:lower:]')

        # Retrieve the value using indirect expansion
        value="${!variable}"

        # Check if the value is not empty
        if [ -n "$value" ]; then
            set_train "$lowercase_variable" "$value"
        fi
    done

    unset 'TRAIN[environment_variables]'

    # Location of the flat data files
    export DATA_FILES_DIR="${MM_DIR}/data/files"

    set_train "data_files_dir" "${MM_DIR}/data/files"

    set_train "file_port_path" "/app"

    export APP_DATA_FILES="data/files"
    set_train "app_data_files" "$APP_DATA_FILES"

    export DB_DUMPS_DIR="$MM_DIR/data/dumps"
    set_train "db_dumps_dir" "$MM_DIR/data/dumps"

    set_train "files_to_process" "$APP_DATA_FILES/.file_lists/files_to_process.txt"
    set_train "files_to_exclude" "$APP_DATA_FILES/.file_lists/files_to_exclude.txt"

    # Location of the library logs
    export LOGS_DIR="$MM_DIR/data/logs"
    set_train "logs_dir" "$MM_DIR/data/logs"

    export APP_LOGS_DIR="/data/logs"
    set_train "app_logs_dir" "/data/logs"

    set_train "toolbox" "/tools"

    export OPT_DIR="/opt/micromanager"
    set_train "opt_dir" "$OPT_DIR"

    # Composer
    # todo:: Remove all the Composer related code from configure_micromanager
    export BIN_DIR="${TRAIN[toolbox]:?}/vendor/bin"
    set_train "bin_dir" "${TRAIN[toolbox]:?}/vendor/bin"
    # Add the Composer bin directory to the PATH
    export PATH="$PATH:$BIN_DIR"

    if [[ ${FORCE_FLAW_FIX} == true ]]; then
        set_train "file_scan_max" 100
    fi

    # Stop the scripts if one returns a non-zero response (error)
    set -e

    # Produce a failure return code if any command has an error
    set -o pipefail

    if [[ "${TRAIN[task]:?}" == "install" ||
        "${TRAIN[task]:?}" == "restart" ]]; then
        echo "  MicroManager Settings . . ."
        echo "    MicroManager ID (MM_ID): ${TRAIN[mm_id]}"
        echo "    MicroManager Directory (MM_DIR): ${MM_DIR}"
        echo ""
        echo "  User Settings . . ."
        echo "    User ID: ${UID}"
        echo "    Group ID: ${GID}"
        echo "    Username: ${TRAIN[mm_user]}"
        echo "    Group: ${GROUP}"
        echo ""
        echo "  Data Storage . . ."
        echo "    Data Files Directory: ${TRAIN[data_files_dir]}"
        echo "    Database Dumps Directory: ${TRAIN[db_dumps_dir]}"
        echo ""
        echo "  Application Code . . ."
        echo "    App Directory: ${TRAIN[git_app_path]:-/app}"
        echo "    Port Directory: ${TRAIN[git_prompts_path]:-}"
        echo "    Bin Directory: ${TRAIN[bin_dir]:-}"
        echo ""
        echo "  MicroManage Files . . ."
        echo "    Scan Base Paths: ${TRAIN[scan_base_paths]:?}"
        echo "    Scan Directory Depth: ${TRAIN[scan_directory_depth]:-}"
        echo "    Scan Exclude Directories: ${TRAIN[scan_exclude_directories]:-}"
        echo "    Scan Exclude Filepaths: ${TRAIN[scan_exclude_filepaths]:-}"
        echo "    Scan Exclude Strings: ${TRAIN[scan_exclude_strings]:-}"
        echo "    Scan File Limit: ${TRAIN[scan_file_limit]:-}"
        echo "    Scan Filename Patterns: ${TRAIN[scan_filename_patterns]:-}"
        echo "    Scan File Strings:  ${TRAIN[scan_file_strings]:-}"
        echo ""
        echo "  Passwords may be set in ${MM_DIR}/.env"
    fi
}
