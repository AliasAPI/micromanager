#!/bin/bash

# Run JSLint to lint Javascript files
# https://github.com/jslint-org/jslint
function run_jslint {
    echo "${FUNCNAME[0]}():"

    # Install JSLint
    if [[ "${TRAIN[task]:?}" == "install" ]] \
        && [ ! -f "${TRAIN[toolbox]:?}/jslint.mjs" ]; then
        cd "${TRAIN[toolbox]:?}" || return 1

        curl -L https://www.jslint.com/jslint.mjs > jslint.mjs
    fi

    # Configure JSLint
    configuration=" --experimental-modules "

    # Run JSLint
    if [[ "${TRAIN[file_type]:?}" != "js" ]]; then
        echo "  Skipping [${TRAIN[file_type]:?}] file"
        return 0
    fi

    if [[ "${TRAIN[task]:?}" == "scan" ]]; then
        # todo:: Run jslint to get the errors in file_log
        # todo:: insert_file_flaws
        return 0
    fi

    if [[ "${TRAIN[task]:?}" != "fix" &&
        "${TRAIN[task]:?}" != "jslint" ]]; then
        return 0
    fi

    # set_file_scanned

    # todo:: Implement and test run_jslint
    node "${TRAIN[toolbox]:?}/jslint.mjs" "${configuration}" "${TRAIN[file_path]:?}"
}
