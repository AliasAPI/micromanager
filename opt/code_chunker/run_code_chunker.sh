#!/bin/bash

# https://github.com/CintraAI/code-chunker
# https://huggingface.co/spaces/CintraAI/code-chunker
function run_code_chunker {
    echo "${FUNCNAME[0]}():"

    # Install Code-Chunker 
    if [[ "${TRAIN[task]:?}" == "install" ]] \
        && [[ "${TRAIN[dockerized]:?}" == "no" ]]; then
        # Go to the code-chunker virtual environment
        cd "/opt" || return 1

        # Create a virtual environment if it doesn't exist
        if [[ ! -d "code_chunker_env" ]]; then
            echo "  Creating virtual environment for Tiktoken . . ."
            python3 -m venv code_chunker_env
        fi

        # Activate the virtual environment
        echo "  Activating virtual environment . . ."
        source code_chunker_env/bin/activate

        git clone https://github.com/CintraAI/code-chunker.git /opt/code_chunker_env/lib/python3.11/site-packages/code_chunker
 
        cd /opt/code_chunker_env/lib/python3.11/site-packages/code_chunker
        sed -i 's/from CodeParser/from code_chunker.CodeParser/' Chunker.py
        sed -i 's/from utils/from code_chunker.utils/' Chunker.py
        pip install -r requirements.txt
    fi


    # Configure Code-Chunker 
    export FILE_PATH="${TRAIN[file_path]}"
    export FILE_TYPE_PORT="${TRAIN[file_type_port]}"
    export ENCODING_NAME="gpt-3.5-turbo"
    local chunk_tokens_script="/opt/code_chunker/chunk_code.py"


    if [[ "${TRAIN[manage_file]:?}" != "yes" &&
        "${TRAIN[task]:?}" != "run_code_chunker" ]]; then
        return 0
    fi


    set_file_scanned

    # Run Code Chunker
    # If the virtual environment is not started, start it
    if [ -z "${VIRTUAL_ENV:-}" ]; then
        source "/opt/code_chunker_env/bin/activate"
    fi

    file_chunks=$(python3 "$chunk_tokens_script")

    deactivate

    # Write the chunks to the log directory
    # Unchunk the file
    # calculate the md5sum of the original file
    # calculate the md5sum of the aider_output
    # if not matching md5sum, error

    # Add fences to the chunked files
    # Loop the Aider to prompt the chunks

    echo "  file_chunks: $file_chunks"


    if [[ "${TRAIN[task]}" == "fix" ]]; then
        echo "  run_code_chunker ran"
        exit 1
    fi
}
