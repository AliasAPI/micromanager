import os
import unittest
from unittest.mock import patch
#from Chunker import Chunker, CodeChunker
from code_chunker.Chunker import Chunker, CodeChunker
import tiktoken
from code_chunker.utils import load_json

def chunk_code():
    file_path = os.getenv('FILE_PATH')
    encoding_model_name = os.getenv('ENCODING_NAME')
    file_type_port = os.getenv('FILE_TYPE_PORT')
    chunk_token_limit = 3700

    # Validate inputs
    if not file_path:
        raise ValueError("Set FILE_PATH environment variable for chunk_code()")

    if not encoding_model_name:
        raise ValueError("Set the ENCODING_NAME environment variable for chunk_code()")

    if not file_type_port:
        raise ValueError("Set the FILE_TYPE_PORT environment variable for chunk_code()")

    try:
        chunker = CodeChunker(file_extension=file_type_port, encoding_name=encoding_model_name)

        with open(file_path, 'r') as file:
            file_code = file.read()

        chunks = chunker.chunk(file_code, token_limit=chunk_token_limit)

        CodeChunker.print_chunks(chunks)
    except KeyError:
        raise ValueError("Invalid file extension or encoding model name.")
    except FileNotFoundError:
        raise ValueError(f"File not found at path: {file_path}")
    except Exception as e:
        raise ValueError(f"An unexpected error occurred: {str(e)}")

chunk_code()