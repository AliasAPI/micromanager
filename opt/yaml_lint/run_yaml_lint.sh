#!/bin/bash

# Run YAML-Lint to automatically lint YAML files
# https://github.com/j13k/yaml-lint
function run_yaml_lint {
    echo "${FUNCNAME[0]}():"

    # Install YAML-Lint
    if [[ "${TRAIN[task]:?}" == "install" ]] \
        && [ ! -f "${TRAIN[bin_dir]:?}/yaml-lint" ]; then
        cd "${TRAIN[toolbox]:?}" || return 1

        composer require --dev j13k/yaml-lint
    fi

    # Configure YAML-Lint
    # todo:: Add the most strict configuration file in etc/yaml_lint/run_yaml_lint.sh
    # done:: Copy the configuration file to the correct location
    # https://yamllint.readthedocs.io/en/stable/configuration.html#default-configuration
    if [[ "${TRAIN[task]:?}" == "install" ||
        "${TRAIN[task]:?}" == "config" ]]; then
        if [[ ! -f "${TRAIN[opt_dir]}/yaml_lint/.yamllint.yml" ]]; then
            echo "  The .yamllint.yml file is missing."
        fi

        cp "${TRAIN[opt_dir]}/yaml_lint/.yamllint.yml" "${TRAIN[toolbox]:?}"
    fi

    if [[ "${TRAIN[manage_file]:?}" != "yes" &&
        "${TRAIN[task]:?}" != "${FUNCNAME[0]}" ]]; then
        return 0
    fi

    # Restrict output to syntax errors --quiet

    command="php ${TRAIN[bin_dir]:?}/yaml-lint --format=json --parse-tags "

    # Run YAML-Lint
    if [[ "${TRAIN[file_type]:?}" != "yml" ]]; then
        echo "  Skipping [${TRAIN[file_type]:?}] file"
        return 0
    fi

    # Yaml-lint only reports flaws (so get the errors and add them to the file_flaws table)

    set_file_scanned

    error=$(${command} 2>&1 > "${TRAIN[file_log_path]:?}")

    exit_code=$?

    if [[ "${exit_code}" -gt 1 ]]; then
        file_flaws="{\"file_path\":\"${TRAIN[file_path]:?}\",\"line\":0,\"column\":0,"
        file_flaws+="\"flaw\":\"${error} Exit code: ${exit_code}\"}"
        insert_file_flaws "${file_flaws}" "yaml_lint"
    fi
}
