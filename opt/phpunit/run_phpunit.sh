#!/bin/bash

# Run PHPUnit last, after linting and checking the code
# https://github.com/phpro/grumphp/blob/master/doc/tasks/phpunit.md
function run_phpunit {
    echo "${FUNCNAME[0]}():"

    # Install PHPUnit
    if [[ "${TRAIN[task]:?}" == "install" ]] \
        && [ ! -f "${TRAIN[bin_dir]:?}/phpunit" ]; then
        cd "${TRAIN[toolbox]:?}" || return 1

        composer require --dev phpunit/phpunit --ignore-platform-reqs
    fi

    # Configure PHPUnit
    # If a phpunit configuration file does NOT exist, use the default configuration
    if [[ "${TRAIN[task]:?}" == "install" ||
        "${TRAIN[task]:?}" == "config" ]] \
        && [ ! -f "${TRAIN[git_app_path]:?}/phpunit.xml" ]; then
        # Copy the default phpunit.xml to the app directory inside the php container
        cp "${TRAIN[opt_dir]:?}/phpunit/phpunit.xml.dist" "${TRAIN[git_app_path]:?}/phpunit.xml"

        test_configuration_file="${TRAIN[git_app_path]:?}/phpunit.xml"
    fi

    set_phpunit_options

    # If a custom phpunit.xml does not exist, use the default file from Micromanager
    if [[ "${TRAIN[task]:?}" == "install" ||
        "${TRAIN[task]:?}" == "config" ]] \
        && [ ! -f "${TRAIN[git_app_path]:?}/phpunit.xml" ]; then
        # Copy the default phpunit.xml to the app directory inside the php container
        cp "${TRAIN[opt_dir]:?}/phpunit/phpunit.xml.dist" "${test_configuration_file}"

        cd "${TRAIN[git_app_path]:?}" || return 1

        # Migrate the phpunit configuration file (if needed)
        php "${TRAIN[bin_dir]:?}/phpunit" --migrate-configuration < /dev/null >> "${TRAIN[app_logs_dir]:?}/phpunit.log" || true
        # ./tools/vendor/bin/phpunit -c /app/phpunit.xml --migrate-configuration
    fi

    # Run PHPUnit
    # todo:: Do not run PHPUnit on every scan? Think.  # A file_changed flag has been set.  Use it.
    # How about only running it on files that are fixed, passed, etc? Flawless?
    if [[ "${TRAIN[task]:?}" != "fix" &&
        "${TRAIN[task]:?}" != "scan" &&
        "${TRAIN[task]:?}" != "test" &&
        "${TRAIN[task]:?}" != "run_phpunit" ]]; then
        return 0
    fi

    cd "${TRAIN[git_app_path]:?}" || return 1

    /tools/vendor/bin/phpunit "${TRAIN[phpunit_options]:?}" || true

    # todo:: process phpunit output
}
