#!/bin/bash

# Run JSONlint first, to check and beautify JSON files
# https://github.com/Seldaek/jsonlint
# todo:: Replace this jsonlint with https://github.com/zaach/jsonlint
function run_jsonlint {
    echo "${FUNCNAME[0]}():"

    # Install JSONlint
    if [[ "${TRAIN[task]:?}" == "install" ]] \
        && [ ! -f "${TRAIN[bin_dir]:?}/jsonlint" ]; then
        cd "${TRAIN[toolbox]:?}" || return 1

        composer require --dev seld/jsonlint
    fi

    # Configure JSONlint
    if [[ "${TRAIN[task]:?}" == "install" ||
        "${TRAIN[task]:?}" == "config" ]] \
        && [ ! -f "${TRAIN[toolbox]:?}/jslint.mjs" ]; then
        cd "${TRAIN[toolbox]:?}" || return 1

        curl -L https://www.jslint.com/jslint.mjs > jslint.mjs
    fi

    if [[ "${TRAIN[manage_file]:?}" != "yes" &&
        "${TRAIN[task]:?}" != "${FUNCNAME[0]}" ]]; then
        return 0
    fi

    command="php ${TRAIN[bin_dir]:?}/jsonlint ${TRAIN[file_path]:?} "

    if [[ "${TRAIN[task]:?}" == "fix" ]]; then
        # Overwrite the file
        command+="--in-place "
    fi

    # Run JSONlint
    if [[ "${TRAIN[file_type]:?}" != "json" ]]; then
        echo "  Skipping [${TRAIN[file_type]:?}] file"
        return 0
    fi

    set_file_scanned

    cd "${TRAIN[toolbox]:?}" || return 1

    error=$(${command} 2>&1 > "${TRAIN[file_log_path]:?}")

    exit_code=$?

    if [[ "${exit_code}" -gt 1 ]]; then
        file_flaws="{\"file_path\":\"${TRAIN[file_path]:?}\",\"line\":0,\"column\":0,"
        file_flaws+="\"flaw\":\"${error} Exit code: ${exit_code}\"}"
        insert_file_flaws "${file_flaws}" "jsonlint"
    fi

    file_flaws=$(jq -c '.files | to_entries[] | 
        {file_path: .key} + (.value.messages[] | {line, column, flaw: .message})' "${TRAIN[file_log_path]}")

    # echo $file_flaws exit 1
    insert_file_flaws "${file_flaws}" "jsonlint"
}
