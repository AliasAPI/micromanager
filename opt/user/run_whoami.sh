#!/bin/bash

function run_whoami {
    echo "${FUNCNAME[0]}():"

    if [[ "${TRAIN[task]:?}" != "install" ]]; then
        return 0
    fi

    whoami_name="$(whoami)"

    echo "whoami: $whoami"
}
