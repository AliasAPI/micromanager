#!/bin/bash

# Set the MicroManager user to match the host machine developer account
function permit_user {
    echo "${FUNCNAME[0]}():"

    if [[ "${TRAIN[task]:?}" != "install" &&
        "${TRAIN[task]:?}" != "restart" &&
        "${TRAIN[task]:?}" != "permit" ]]; then
        return 0
    fi

    # MM_USER=$(whoami)
    # export MM_USER
    MM_USER_UID=$(id -u "$USER")
    export MM_USER_UID
    MM_USER_GID=$(id -g "$USER")
    export MM_USER_GID

    sudo usermod -aG docker $USER

    echo "  MicroManager User . . ."
    echo "    MicroManager USER: ${TRAIN[mm_user]:?}"
    echo "    MicroManager USER UID: $MM_USER_UID"
    echo "    MicroManager USER GID: $MM_USER_GID"
    echo ""

    # Add the MM_USER to all the necessary groups
    MM_USER_GROUPS=("${MM_USER_GID}" "daemon" "docker" "mysql" "sudo")

    echo "  Adding the [${TRAIN[mm_user]}] user to following groups . . ."

    for group in "${MM_USER_GROUPS[@]}"; do
        if [[ $(getent group "${group%?}") ]]; then
            echo "   ${group%?}"
            # Add MM_USER to each group
            sudo usermod -a -G "${group%?}" "${TRAIN[mm_user]}"
        fi
    done

    # Set directories as a string not an array
    directories="
        $MM_DIR/app
        $MM_DIR/prompts
        $DATA_FILES_DIR
        $LOGS_DIR
        $MARIADB_DIR/data
        $DB_DUMPS_DIR
        $MM_DIR/tools"

    # Process each directory
    echo "$directories" | while read -r dir; do
        # Trim leading/trailing whitespace
        directory=$(echo "$directories" | xargs)

        # Skip empty lines
        [ -z "$directory" ] && continue

        if [ -d "$directory" ]; then
            sudo chmod -R a+rwX "$directory"
        fi
    done
}
