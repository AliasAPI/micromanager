#!/bin/bash

# Run PHPMD to automatically check PHP files
# https://github.com/phpmd/phpmd
function run_phpmd {
    echo "${FUNCNAME[0]}():"

    # Install PHPMD
    if [[ "${TRAIN[task]:?}" == "install" ]] \
        && [ ! -f "${TRAIN[bin_dir]:?}/phpmd" ]; then
        cd "${TRAIN[toolbox]:?}" || return 1

        composer require --dev phpmd/phpmd
    fi

    # Configure PHPMD
    # todo:: Review https://github.com/phpmd/phpmd
    # todo:: Add the most strict configuration file in etc/phpmd/phpmd.xml
    if [[ "${TRAIN[task]:?}" == "install" ||
        "${TRAIN[task]:?}" == "config" ]] \
        && [ ! -f "${TRAIN[toolbox]:?}/phpmd.xml" ]; then
        cp "${TRAIN[opt_dir]}/phpmd/phpmd.xml" "${TRAIN[toolbox]:?}"
    fi

    if [[ "${TRAIN[manage_file]:?}" != "yes" &&
        "${TRAIN[task]:?}" != "${FUNCNAME[0]}" ]]; then
        return 0
    fi

    command="php ${TRAIN[bin_dir]:?}/phpmd ${TRAIN[file_path]:?} json ${TRAIN[toolbox]:?}/phpmd.xml"

    # Run PHPMD
    if [[ "${TRAIN[file_type]:?}" != "php" ]]; then
        echo "  Skipping [${TRAIN[file_type]:?}] file"
        return 0
    fi

    set_file_scanned

    cd "${TRAIN[toolbox]:?}" || return 1

    set +e
    error=$(${command} 2>&1 > "${TRAIN[file_log_path]:?}")
    exit_code=$?
    set -e

    if [[ "${exit_code}" -gt 1 ]]; then
        file_flaws="{\"file_path\":\"${TRAIN[file_path]:?}\",\"line\":0,\"column\":0,"

        file_flaws+="\"flaw\":\"${error} Exit code: ${exit_code}\"}"

        insert_file_flaws "${file_flaws}" "phpmd"
    fi

    file_flaws=$(jq -c '.files[] | 
        {file_path: .file} + (.violations[] | {line: .beginLine, column: 0, flaw: .description})' "${TRAIN[file_log_path]}")

    insert_file_flaws "${file_flaws}" "phpmd"
}
