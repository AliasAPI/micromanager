#!/bin/bash

# Run LiteLLM to call 100+ LLM APIs in OpenAI format (using Ollama)
# https://github.com/BerriAI/litellm
function run_litellm {
    echo "${FUNCNAME[0]}():"

    # Install LiteLLM if necessary
    if [[ "${TRAIN[task]:?}" == "install" ]] && \
        [[ "${TRAIN[dockerized]:?}" == "no" ]] && \
        command -v pipx &> /dev/null && \
        ! command -v litellm &> /dev/null; then
        # Install LiteLLM and indent output by 2 spaces
        pip install 'litellm[proxy]' 2>&1 | while IFS= read -r line; do
            echo "  $line"
        done

        # Ensure LiteLLM is installed before proceeding
        if ! command -v litellm &> /dev/null; then
            echo "Error: LiteLLM is not installed or not in PATH." >&2
            return 1
        fi
    fi

}
