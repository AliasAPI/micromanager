#!/bin/bash

# https://github.com/mvdan/sh
# https://pkg.go.dev/mvdan.cc/sh/v3
function run_shfmt {
    echo "${FUNCNAME[0]}():"

    # Install shfmt
    if [[ "${TRAIN[task]:?}" == "install" ]] \
        && [[ "${TRAIN[dockerized]:?}" == "no" ]] \
        && ! command -v shfmt > /dev/null 2>&1; then

        export DEBIAN_FRONTEND=noninteractive

        # Go installation required in Dockerfile
        # go install mvdan.cc/sh/v3/cmd/shfmt@latest
        apt-get install shfmt -y | while IFS= read -r line; do
            echo "  $line"
        done

        # Unset to avoid affecting subsequent commands
        unset DEBIAN_FRONTEND
    fi

    # Configure shfmt
    # https://www.howtogeek.com/devops/how-to-use-shfmt-to-format-shell-scripts-better/
    if [[ "${TRAIN[manage_file]:?}" != "yes" &&
        "${TRAIN[task]:?}" != "${FUNCNAME[0]}" ]]; then
        return 0
    fi

    if [[ "${TRAIN[file_type]:?}" != "sh" ]]; then
        echo "  Skipping [${TRAIN[file_type]:?}] file"
        return 0
    fi

    command="shfmt "

    # -w Write the file if fixing flaws
    if [[ "${TRAIN[task]}" == "fix" ||
        "${TRAIN[task]}" == "${FUNCNAME[0]}" ]]; then
        command+="-w "
    fi

    command+="--language-dialect bash "
    # Indent with 4 spaces
    command+="-i 4 "
    # Binary ops like && and | may start a line
    command+="--binary-next-line "
    # Redirect operators will be followed by a space
    command+="--space-redirects "
    # Keep column alignment paddings
    command+="--keep-padding "
    # Switch cases will be indented
    command+="--case-indent "
    # Specify the file to fix
    command+="${TRAIN[file_path]} "

    # Run Shfmt
    set_file_scanned

    set +e
    # Run the command and capture stdout and stderr separately
    error_output=$({ ${command} 1> "${TRAIN[file_log_path]:?}"; } 2>&1)
    exit_code=$?
    set -e

    # Check if the exit code indicates an error
    if [ $exit_code -gt 2 ]; then
        # Clean and format the error message
        flaw=$(clean_flaw "${error_output} Exit code $exit_code")
        file_flaws="[{\"file_path\":\"${TRAIN[file_path]:?}\",\"line\":0,\"column\":0,\"flaw\":\"${flaw}\"}]"

        # Save the error as JSON in the log file
        echo "${file_flaws}" > "${TRAIN[app_logs_dir]}"/shfmt_error.log
        return 1
    fi
}
