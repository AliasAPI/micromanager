#!/bin/bash

# Run ProgPilot to automatically check PHP files
# https://github.com/designsecurity/progpilot
function run_progpilot {
    echo "${FUNCNAME[0]}():"

    # Install ProgPilot
    if [ "${TRAIN[task]:?}" == "install" ] \
        && [ ! -f "${TRAIN[bin_dir]:?}/progpilot" ]; then
        cd "${TRAIN[toolbox]:?}" || return 1

        composer require --dev designsecurity/progpilot
    fi

    # Configure ProgPilot
    # todo:: Review /progpilot/configuration.yml
    # todo:: Add the most strict configuration file in etc/progpilot/run_progpilot.sh
    if [[ "${TRAIN[task]:?}" == "install" ||
        "${TRAIN[task]:?}" == "config" ]] \
        && [ ! -f "${TRAIN[toolbox]:?}/progpilot" ]; then
        cp "${TRAIN[opt_dir]:?}/progpilot/configuration.yml" "${TRAIN[toolbox]:?}"
    fi

    if [[ "${TRAIN[manage_file]:?}" != "yes" &&
        "${TRAIN[task]:?}" != "${FUNCNAME[0]}" ]]; then
        return 0
    fi

    command="php ${TRAIN[bin_dir]:?}/progpilot --configuration configuration.yml ${TRAIN[file_path]:?} "

    # Run ProgPilot
    if [[ "${TRAIN[file_type]:?}" != "php" ]]; then
        echo "  Skipping [${TRAIN[file_type]:?}] file"
        return 0
    fi

    set_file_scanned

    cd "${TRAIN[git_app_path]:?}" || return 1

    # todo:: Get run_progpilot to work
    set +e
    # Run the command and capture stdout and stderr separately
    error_output=$({ ${command} 1> "${TRAIN[file_log_path]:?}"; } 2>&1)
    exit_code=$?
    set -e

    # Check if the exit code indicates an error
    if [ $exit_code -gt 2 ]; then
        # Clean and format the error message
        flaw=$(clean_flaw "${error_output} Exit code $exit_code")
        file_flaws="[{\"file_path\":\"${TRAIN[file_path]:?}\",\"line\":0,\"column\":0,\"flaw\":\"${flaw}\"}]"

        # Save the error as JSON in the log file
        echo "${file_flaws}" > "${TRAIN[app_logs_dir]}"/php_codesniffer.log
        return 1
    fi

    # Installing and running Progpilot ends with this error:
    # PHP Fatal error:  Declaration of progpilot\Console\Application::run(?Symfony\Component\Console\Input\InputInterface $input = null, ?Symfony\Component\Console\Output\OutputInterface $output = null) must be compatible with Symfony\Component\Console\Application::run(?Symfony\Component\Console\Input\InputInterface $input = null, ?Symfony\Component\Console\Output\OutputInterface $output = null): int in /tools/vendor/designsecurity/progpilot/package/src/progpilot/Console/Application.php on line 27
}
