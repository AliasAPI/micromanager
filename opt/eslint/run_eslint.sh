#!/bin/bash

# Run eslint to lint Javascript, React, Vue, Node, and Typescript files
# https://eslint.org
# https://github.com/eslint/eslint
# https://github.com/EvgenyOrekhov/eslint-config-hardcore
function run_eslint {
    echo "${FUNCNAME[0]}():"

    # Install eslint
    if [[ "${TRAIN[task]:?}" == "install" ]]; then
        # todo:: Check to see if eslint is already installed

        cd "${TRAIN[toolbox]:?}" || return 1

        # todo:: Make sure this installs in /opt/eslint/node_modules/.bin
        # https://github.com/eslint/eslint
        # npm init @eslint/config

        # Aims to include as many plugins and rules as possible to make your code extremely consistent and robust.
        # https://github.com/EvgenyOrekhov/eslint-config-hardcore
        # npm install eslint-config-hardcore --save-dev
    fi

    # Configure eslint
    if [[ "${TRAIN[task]:?}" == "install" ||
        "${TRAIN[task]:?}" == "config" ]] \
        && [ ! -f "${TRAIN[toolbox]:?}/.eslintrc.json" ]; then
        configuration_file="react.eslintrc.json"
        configuration_file="typescript.eslintrc.json"
        configuration_file="typescript.nuxt3.eslintrc.json"
        configuration_file="vue3.nuxt3.eslintrc.json"

        cp "${TRAIN[opt_dir]}/eslint/${configuration_file}" "${TRAIN[toolbox]:?}/.eslintrc.json"
    fi

    # todo:: Format eslint output in JSON
    # https://stackoverflow.com/questions/58391647/eslint-output-to-file-and-console-at-the-same-time
    configuration=" --format json --output-file /data/logs/eslint.json "

    # Run eslint
    if [[ "${TRAIN[task]:?}" != "fix" &&
        "${TRAIN[task]:?}" != "scan" &&
        "${TRAIN[task]:?}" != "run_eslint" ]]; then
        echo "  Skipping [${TRAIN[file_type]:?}] file"
        return 0
    fi

    cd "${TRAIN[git_app_path]:?}" || return 1

    if [[ "${TRAIN[file_type]:?}" != "js" ]]; then
        echo "  Skipping [${TRAIN[file_type]:?}] file"
        return 0
    fi

    # todo:: Add scan (dry run) functionality
    if [[ "${TRAIN[task]:?}" == "scan" ]]; then
        # todo:: Run eslint to get the errors in file_log
        # todo:: insert_file_flaws
        echo " ${configuration}" # remove this line
        return 0
    fi

    if [[ "${TRAIN[task]:?}" != "fix" &&
        "${TRAIN[task]:?}" != "html_purifier" ]]; then
        return 0
    fi

    # set_file_scanned

    # todo:: Implement and test run_jslint
    # npx eslint "${TRAIN[file_path]:?}" "${configuration}"
}
