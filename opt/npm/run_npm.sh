function run_npm {
    echo "${FUNCNAME[0]}():"

    # Install npm
    if [[ "${TRAIN[task]:?}" == "install" ]] \
        && [[ "${TRAIN[dockerized]:?}" == "no" ]] \
        && ! command -v npm &> /dev/null; then
        echo "  Installing npm . . ."

        # Install npm while indenting the output by 2 spaces
        apt-get install -y npm 2>&1 | while IFS= read -r line; do
            echo "  $line"
        done

        if [ $? -ne 0 ]; then
            echo "  Error: Failed to install npm"
            exit 1
        fi
    fi

    # Configure npm

    # Run npm

    # Process npm output
}
