#!/bin/bash

# Run PHP Code Sniffer to automatically check PHP files
# PHP_CodeSniffer is a set of two PHP scripts;
# The phpcs script tokenizes PHP, JavaScript and CSS files to detect violations
# The phpcbf (PHP Code Beautifier and Fixer) script to automatically corrects the code
# Run PHP-Parallel-Lint first and do not run PHP_CodeSniffer if it fails
# https://github.com/PHPCSStandards/PHP_CodeSniffer/
# https://packagist.org/packages/slevomat/coding-standard
function run_php_codesniffer {
    echo "${FUNCNAME[0]}():"

    # Install PHP CodeSniffer
    if [[ "${TRAIN[task]:?}" == "install" ]] \
        && [ ! -f "${TRAIN[bin_dir]:?}/phpcs" ]; then
        cd "${TRAIN[toolbox]:?}" || return 1

        composer config --no-plugins allow-plugins.dealerdirect/phpcodesniffer-composer-installer true
        composer require --dev PHPCSStandards/PHP_CodeSniffer
        composer require --dev slevomat/coding-standard
    fi

    # Configure PHP CodeSniffer and PHP Code Beautifier
    if [[ "${TRAIN[task]:?}" == "install" ||
        "${TRAIN[task]:?}" == "config" ]] \
        && [ ! -f "${TRAIN[toolbox]:?}/phpcs.xml" ]; then
        cp "${TRAIN[opt_dir]:?}/php_codesniffer/phpcs.xml" "${TRAIN[toolbox]:?}/phpcs.xml"
    fi

    if [[ "${TRAIN[manage_file]:?}" != "yes" &&
        "${TRAIN[task]:?}" != "run_phpcbf" ]]; then
        return 0
    fi

    if [[ "${TRAIN[file_type]:?}" != "php" ]]; then
        echo "  Skipping [${TRAIN[file_type]:?}] file"
        return 0
    fi

    # Use this command to list all the rules (and make it easier to produce the configuration file):
    # /tools/vendor/bin/phpcs -e --standard=Squiz
    # "I developed PHP_CodeSniffer, so it might not be surprising that the standard I use is very strict.
    # This one also contains checks for JS and CSS files because it is used on our large web applications."
    # https://stackoverflow.com/questions/21288459/whats-a-good-standard-to-use-with-php-codesniffer
    # Strict settings: https://github.com/orisai/coding-standard-php
    # todo:: Fix the command and phpcs.xml config file to stop outputting "No fixable errors were found Time: 18ms; Memory: 10MB"
    # -q Quiet mode; disables progress and verbose output. Use -vvv to get very verbose output
    command="php ${TRAIN[bin_dir]:?}/phpcs "
    command+="--standard=${TRAIN[toolbox]}/phpcs.xml "
    command+="--report=json -w -q --encoding=UTF-8 ${TRAIN[file_path]}"

    # todo:: https://github.com/squizlabs/PHP_CodeSniffer/wiki/Reporting#printing-a-json-report
    # todo:: Configure Javascript linting
    # todo:: Configure CSS linting

    # -w Fix both warnings and errors (on by default)
    fix_command="php ${TRAIN[bin_dir]:?}/phpcbf "
    fix_command+="--config-set php_path ${TRAIN[opt_dir]:?}/bitnami/php/bin/php "
    fix_command+="-w -q ${TRAIN[file_path]:?}"

    # Run PHP CodeSniffer
    cd "${TRAIN[toolbox]:?}" || return 1

    set_file_scanned

    set +e
    # Run the command and capture stdout and stderr separately
    error_output=$({ ${command} 1> "${TRAIN[file_log_path]:?}"; } 2>&1)
    exit_code=$?
    set -e

    # Check if the exit code indicates an error
    if [ $exit_code -gt 2 ]; then
        # Clean and format the error message
        flaw=$(clean_flaw "${error_output} Exit code $exit_code")
        file_flaws="[{\"file_path\":\"${TRAIN[file_path]:?}\",\"line\":0,\"column\":0,\"flaw\":\"${flaw}\"}]"

        # Save the error as JSON in the log file
        echo "${file_flaws}" > "${TRAIN[app_logs_dir]}"/php_codesniffer.log
        return 1
    fi

    # Process the output if there is no error
    file_flaws=$(jq -c '.files | to_entries[] | 
        {file_path: .key} + (.value.messages[] | {line, column, flaw: .message})' "${TRAIN[file_log_path]}")

    insert_file_flaws "${file_flaws}" "php_codesniffer"

    if [[ "${TRAIN[task]:?}" != "fix" &&
        "${TRAIN[task]:?}" != "run_phpcbf" ]]; then
        return 0
    fi

    set +e
    error_output=$({ ${fix_command} 1> "${TRAIN[app_logs_dir]}/phpcbf.log"; } 2>&1)
    exit_code=$?
    set -e

    if [ $exit_code -gt 2 ]; then
        flaw=$(clean_flaw "${error_output} Exit code ${exit_code}")
        file_flaws="{\"file_path\":\"${TRAIN[file_path]:?}\",\"line\":0,\"column\":0,\"flaw\":\"${flaw}\"}"

        # Save the error as JSON in the log file
        echo "${file_flaws}" > "${TRAIN[app_logs_dir]}"/phpcbf.log

        insert_file_flaws "${file_flaws}" "phpcbf"
        return 0
    fi
}
